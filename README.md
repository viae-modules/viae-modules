# VIAE Modules
Frequent used modules of VIAE. For questions/contact, go to https://viae.solutions.

## Context and content description
VIAE modules contains core classes focused on kotlin and micronaut, but with clean architecture 
and library independence in mind. Although the focus is towards micronaut, most of the modules are 
micronaut independent.

Why did we start on our own module code? We wanted to hook up keycloak (OAuth 2.0 server: [official website](https://www.keycloak.org/)) 
to our application, which was micronaut. There was no official micronaut support by then. As this
tool is often used in the Java - Kotlin world, we thought that a library/framework independent 
implementation was missing to connect to keycloak and to do user validation and permission checks 
via "clean"/plain Java/Kotlin code. We created an interface based connection that can be used in 
SpringBoot, Micronaut, ... . 

Next to the user authentication/authorization checks, we had quite some other projects/classes that 
were used over multiple projects. We decided to extract these into their own module as well.

## How to use
As these modules are created with library independence in mind, it would be strange to force you 
to include these as a library. You can just copy the classes into your own project and adapt/use them
like you prefer.

If you want to drag in the latest changes as well, you can include our components via maven or gradle 
as they are published to maven central.

If you like these modules, feel free to extend them with functionality that you think is missing.
The only thing to keep in mind is **avoid unnecessary use of libraries**.

## Module listing

| Module  | Description | 
| ------------- |-------------| 
| <pre>viae-oauth2.0-validator</pre> | Kotlin based connection with keycloak. User authentication and permission validation without having to use Micronaut, SpringBoot, ... | 
| <pre>viae-requestlog</pre>  | Kotlin based request log framework. Add logging about who is using your endpoints, how long these actions take, what the status was of that request and what happened during the processing of it without having to use Micronaut, SpringBoot, ... | 
