package solutions.viae.coreutils.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.ZoneId
import java.time.ZonedDateTime

class DateUtilsTest {

    @Test
    fun testToString() {
        //Given
        val date =
            ZonedDateTime.of(1989, 4, 17, 20, 10, 35, 0, ZoneId.of("Europe/Brussels"))

        //When
        val result = DateUtils.toString(date)

        //Then
        assertThat(result).isEqualTo("1989-04-17 20:10:35.000000")
    }

    @Test
    fun testNowToString() {
        //When
        val now = DateUtils.nowAsString()
        val nowTs = DateUtils.now()

        //Then
        assertThat(now).startsWith(
            "" +
                    "${nowTs.year.toString().padStart(2, '0')}-" +
                    "${nowTs.monthValue.toString().padStart(2, '0')}-" +
                    "${nowTs.dayOfMonth.toString().padStart(2, '0')} " +
                    "${nowTs.hour.toString().padStart(2, '0')}:" +
                    "${nowTs.minute.toString().padStart(2, '0')}:" +
                    nowTs.second.toString().padStart(2, '0')
        )
    }
}
