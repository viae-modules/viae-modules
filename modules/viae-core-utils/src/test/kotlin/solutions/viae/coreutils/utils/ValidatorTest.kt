package solutions.viae.coreutils.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.ErrorMessage.Companion.error
import solutions.viae.coreutils.domain.ErrorMessage.Companion.permissionError
import solutions.viae.coreutils.domain.ErrorType.FUNCTIONAL
import solutions.viae.coreutils.domain.ErrorType.PERMISSION_FAILURE
import java.util.*

class ValidatorTest {
    @Test
    fun testWithErrorOnFirstAndSecondLevel() {
        //When
        val result = runValid {
            with(it) {
                isNotBlank("", error(ValidatorTest::class, "should not be blank", "input-validation"))

                and("input-validation") {
                    isTrue(false, permissionError(ValidatorTest::class, "should be true", "data-validation"))
                }

                respond {
                    "success"
                }
            }
        }

        //Then
        assertThat(result.errors).isEqualTo(
            listOf(
                ErrorMessage(
                    classSource = ValidatorTest::class,
                    message = "should not be blank",
                    errorGroup = "input-validation",
                    errorType = FUNCTIONAL
                )
            )
        )
    }

    @Test
    fun testWithErrorOnSecondLevel() {
        //When
        val result = runValid {
            with(it) {
                isNotBlank("non-blank value", error(ValidatorTest::class, "should not be blank", "input-validation"))

                and("input-validation") {
                    isTrue(false, permissionError(ValidatorTest::class, "should be true", "data-validation"))
                }

                respond {
                    "success"
                }
            }
        }

        //Then
        assertThat(result.errors).isEqualTo(
            listOf(
                ErrorMessage(
                    classSource = ValidatorTest::class,
                    message = "should be true",
                    errorGroup = "data-validation",
                    errorType = PERMISSION_FAILURE
                )
            )
        )
    }

    @Test
    fun testWithValidScenario() {
        //When
        val result = runValid {
            with(it) {
                isNotBlank("non-blank value", error(ValidatorTest::class, "should not be blank", "input-validation"))

                and("input-validation") {
                    isTrue(true, permissionError(ValidatorTest::class, "should be true", "data-validation"))
                }

                respond {
                    "success"
                }
            }
        }

        //Then
        assertThat(result.errors).isEqualTo(emptyList<ErrorMessage>())
        assertThat(result.data).isEqualTo("success")
    }

    @Test
    fun testWithValidTransformation() {
        //When
        val result = runValid {
            with(it) {
                isNotBlank("non-blank value", error(ValidatorTest::class, "should not be blank", "input-validation"))

                val result = and("input-validation") {
                    tryTransform(
                        { UUID.fromString(UUID.randomUUID().toString()) },
                        error(ValidatorTest::class, "could not transform", "data-transformation")
                    )
                }

                respond {
                    "success, ${result!!.toString()}" //it is safe to call "!!", with errors, this code block is not executed
                }
            }
        }

        //Then
        assertThat(result.errors).isEqualTo(emptyList<ErrorMessage>())
        assertThat(result.data).startsWith("success, ")
    }

    @Test
    fun testWithInvalidTransformation() {
        //When
        val result = runValid {
            with(it) {
                isNotBlank("non-blank value", error(ValidatorTest::class, "should not be blank", "input-validation"))

                val result = and("input-validation") {
                    tryTransform(
                        { UUID.fromString("not a UUID") },
                        error(ValidatorTest::class, "could not transform", "data-transformation")
                    )
                }

                respond {
                    "success, ${result!!.toString()}" //it is safe to call "!!", with errors, this code block is not executed
                }
            }
        }

        //Then
        assertThat(result.errors).isEqualTo(
            listOf(
                ErrorMessage(
                    classSource = ValidatorTest::class,
                    message = "could not transform, cause: Invalid UUID string: not a UUID",
                    errorGroup = "data-transformation",
                    errorType = FUNCTIONAL
                )
            )
        )
    }
}
