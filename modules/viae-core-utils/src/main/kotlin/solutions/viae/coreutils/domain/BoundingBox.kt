package solutions.viae.coreutils.domain

import solutions.viae.coreutils.service.Logger

data class BoundingBox(
    val center: LatLng,
    val east: LatLng,
    val west: LatLng,
    val north: LatLng,
    val south: LatLng,
    val radiusInMeter: Long
) {

    fun print(logger: Logger) {
        logger.info(
            """
    bounding box:
    center: ${center.print()}
    radiusInMeter: radiusInMeter
    north: ${north.print()}
    east: ${east.print()}
    south: ${south.print()}
    west: ${west.print()}
        """.trimIndent()
        )
    }

    fun printForTesting(logger: Logger, reference: LatLng = center, referenceName: String = "reference name not set") {
        logger.info(
            """
site: http://www.hamstermap.com/custommap.html
bounding box:
${center.lat}	${center.lng}	cross5	red	1	center.
${north.lat}	${north.lng}	circle4	blue	2	north.
${east.lat}	${east.lng}	circle4	blue	6	east.
${south.lat}	${south.lng}	circle4	blue	4	south.
${west.lat}	${west.lng}	circle4	blue	5	west.
${reference.lat}	${reference.lng}	star2	yellow	6	reference: ${referenceName}.
"""
        )
    }
}
