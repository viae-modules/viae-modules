package solutions.viae.coreutils.domain

import java.time.DayOfWeek
import java.time.ZonedDateTime
import java.util.*
import java.util.Optional.empty

class Timing {
    class TimeSlot(
        val isStrict: Boolean,
        val strictTimeSlot: Optional<StrictTimeSlot>,
        val periodicTimeSlot: Optional<PeriodicTimeSlot>
    ) {

        companion object {
            fun of(ts: StrictTimeSlot): TimeSlot {
                return TimeSlot(
                    isStrict = true,
                    strictTimeSlot = Optional.of(ts),
                    periodicTimeSlot = empty()
                )
            }

            fun of(ts: PeriodicTimeSlot): TimeSlot {
                return TimeSlot(
                    isStrict = true,
                    strictTimeSlot = empty(),
                    periodicTimeSlot = Optional.of(ts)
                )
            }
        }
    }

    class StrictTimeSlot(
        val nthOfMonth: Int,
        val day: Day,
        val hour: Int,
        val minute: Int = 0,
        val seconds: Int = 0
    )

    class PeriodicTimeSlot(val everyMilliseconds: Long)

    enum class Day(val order: Int) {
        MONDAY(0),
        TUESDAY(1),
        WEDNESDAY(2),
        THURSDAY(3),
        FRIDAY(4),
        SATURDAY(5),
        SUNDAY(6);

        val dayOfWeek: DayOfWeek
            get() = DayOfWeek.of(order + 1)

        companion object {

            @Suppress("MagicNumber")
            fun from(timeStamp: ZonedDateTime): Day {
                return when (timeStamp.dayOfWeek.value) {
                    1 -> MONDAY
                    2 -> TUESDAY
                    3 -> WEDNESDAY
                    4 -> THURSDAY
                    5 -> FRIDAY
                    6 -> SATURDAY
                    7 -> SUNDAY
                    else -> throw IllegalStateException("day " + timeStamp.dayOfWeek.value + " is not supported")
                }
            }
        }
    }
}
