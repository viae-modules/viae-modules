package solutions.viae.coreutils.domain

data class LatLng (val lat: Double, val lng: Double) {
    fun print(): String {
        return "lat: ${lat}; lng: ${lng}"
    }
}
