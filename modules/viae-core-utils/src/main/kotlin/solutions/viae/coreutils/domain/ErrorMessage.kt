package solutions.viae.coreutils.domain

import solutions.viae.coreutils.domain.ErrorType.FUNCTIONAL
import solutions.viae.coreutils.domain.ErrorType.PERMISSION_FAILURE
import kotlin.reflect.KClass

data class ErrorMessage(
    val classSource: KClass<*>,
    val message: String,
    val errorGroup: String = "default",
    val errorType: ErrorType = FUNCTIONAL
) {
    val source = classSource.qualifiedName!!

    companion object {
        fun error(classSource: KClass<*>, message: String, errorGroup: String = "default"): ErrorMessage {
            return ErrorMessage(classSource, message, errorGroup, FUNCTIONAL)
        }

        fun permissionError(classSource: KClass<*>, message: String, errorGroup: String = "default"): ErrorMessage {
            return ErrorMessage(classSource, message, errorGroup, PERMISSION_FAILURE)
        }
    }
}

enum class ErrorType {
    FUNCTIONAL, PERMISSION_FAILURE, NOT_FOUND
}
