package solutions.viae.coreutils.utils

import solutions.viae.coreutils.domain.Response
import java.lang.IllegalStateException
import kotlin.reflect.KClass

object ObjectUtils {
    fun whenNull(value: Any?, callback: () -> Unit) {
        if (value == null) {
            callback()
        }
    }

    fun ifBlank(value: String?, onBlank: () -> Unit): Unit {
        if (value == null || value == "") {
            onBlank()
        }
    }

    fun ifNull(value: Any?, onNull: () -> Unit): Unit {
        if (value == null) {
            onNull()
        }
    }

    fun <T> tryTransform(transform: () -> T, onError: (e: Exception) -> Unit): T? {
        return try {
            transform()
        } catch (e: Exception) {
            onError(e)
            null
        }
    }

    fun <T> tryTransformFromResponse(transform: () -> Response<T>, onError: (e: Exception) -> Unit): T? {
        return try {
            val result = transform()
            if(result.hasNoErrors()){
                if(result.isWithData()) result.data else null
            } else {
                onError(IllegalStateException(result.errors.joinToString(";")))
                null
            }
        } catch (e: Exception) {
            System.err.println(e.localizedMessage)
            onError(e)
            null
        }
    }
}

fun String.toUtf8Bytes() = this.toByteArray(Charsets.UTF_8)
fun stringFromUtf8Bytes(bytes: ByteArray) = String(bytes, Charsets.UTF_8)

fun <E: Enum<E>> enumFromName(enumClass: KClass<E>, value: String): E = java.lang.Enum.valueOf(enumClass.java, value)
fun <E: Enum<E>> enumFromOrdinal(enumClass: KClass<E>, ordinal: Int): E = enumClass.java.enumConstants[ordinal]

fun <E: Enum<E>> KClass<E>.enumClassName(): String = this.java.canonicalName ?: ""
fun <E : Enum<E>> KClass<E>.enumMembers(): Array<E> = this.java.enumConstants
