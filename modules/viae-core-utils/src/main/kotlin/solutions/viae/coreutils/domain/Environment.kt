package solutions.viae.coreutils.domain

enum class Environment {
    IN_MEMORY, LOCAL_TESTING, DEBUG, LOCAL, DEV, INT, TEST, QA, PROD,
    LOCAL_TESTING_1, LOCAL_TESTING_2, LOCAL_TESTING_3,
    REMOTE_TESTING_1, REMOTE_TESTING_2, REMOTE_TESTING_3;

    companion object {
        fun fromString(value: String): Response<Environment> {
            return when (value.toUpperCase()) {
                IN_MEMORY.name -> Response.success(IN_MEMORY)
                DEBUG.name -> Response.success(DEBUG)
                LOCAL_TESTING.name -> Response.success(LOCAL_TESTING)
                LOCAL_TESTING_1.name -> Response.success(LOCAL_TESTING_1)
                LOCAL_TESTING_2.name -> Response.success(LOCAL_TESTING_2)
                LOCAL_TESTING_3.name -> Response.success(LOCAL_TESTING_3)
                REMOTE_TESTING_1.name -> Response.success(REMOTE_TESTING_1)
                REMOTE_TESTING_2.name -> Response.success(REMOTE_TESTING_2)
                REMOTE_TESTING_3.name -> Response.success(REMOTE_TESTING_3)
                LOCAL.name -> Response.success(LOCAL)
                DEV.name -> Response.success(DEV)
                INT.name -> Response.success(INT)
                TEST.name -> Response.success(TEST)
                QA.name -> Response.success(QA)
                PROD.name -> Response.success(PROD)
                else -> Response.failure(
                    ErrorMessage(
                        Environment::class,
                        "Environment '$value' is not supported, possible values are: ${values().map { v -> v.name }}"
                    )
                )
            }
        }
    }
}
