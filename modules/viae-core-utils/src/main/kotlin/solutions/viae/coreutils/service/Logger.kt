package solutions.viae.coreutils.service

interface Logger {

    fun trace(message: String, vararg args: String)

    fun debug(message: String, vararg args: String)

    fun info(message: String, vararg args: String)

    fun warn(message: String, vararg args: String)

    fun warn(cause: Throwable, message: String, vararg args: String)

    fun error(message: String, vararg args: String)

    fun error(cause: Throwable, message: String, vararg args: String)
}
