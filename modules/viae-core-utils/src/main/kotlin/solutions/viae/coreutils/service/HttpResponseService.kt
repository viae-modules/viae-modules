package solutions.viae.coreutils.service

import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import java.net.URI

enum class ResponseType {
    UNAUTHORIZED, BAD_REQUEST, OK, OK_NO_CONTENT
}

interface ViaeHttpRequest {
    val headers: Map<String, String>
    val uri: URI
    val method: String
    val hostName: String
    val port: Int
    val environment: Environment
}

/**
 * Wrapper interface around the infrastructure specific response class.
 *
 * @param RDT<RDT> (response) data type of the response. When using micronaut, this would be the class you pass to the HttpResponse class.
 * E.g. HttpResponse<RDT> ~ ResponseType<RDT>
 */
interface ViaeHttpResponse<RDT>{
    val type: ResponseType
}

data class UnAuthorizedResponseType<RDT>(
    val errorMessageHeader: Pair<String, String>
): ViaeHttpResponse<RDT>{
    override val type = ResponseType.UNAUTHORIZED
}

data class BadRequestResponseType<RDT>(
    val errors: List<ErrorMessage>
): ViaeHttpResponse<RDT>{
    override val type = ResponseType.BAD_REQUEST
}

data class OkResponseType<RDT>(
    val data: RDT
): ViaeHttpResponse<RDT>{
    override val type = ResponseType.OK
}

class OkNoContentResponseType: ViaeHttpResponse<Void>{
    override val type = ResponseType.OK
}

interface HttpResponseService {
    /**
     * @param DT<DT> data type of the response. When using micronaut, this would be a wrapper around the HttpResponse class.
     * @param RDT<RDT> (response) data type of the response. You can see this as a wrapper around the HttpResponse class if you would be using micronaut.
     */
    fun <DT> httpify(response: Response<DT>): ViaeHttpResponse<DT> {
        return try{
            httpify(response) { it }
        } catch (e: Exception){
            System.err.println("uncaught exception: ${e.localizedMessage}")
            e.printStackTrace()
            BadRequestResponseType(listOf(
                ErrorMessage(
                    HttpResponseService::class,
                    "uncaught exception: ${e.localizedMessage}"
                )
            ))
        }
    }
    /**
     * @param DT<DT> data type of the response. When using micronaut, this would be a wrapper around the HttpResponse class.
     * @param MT<MT> the mapped data type of the response. Here you can e.g. transform a data representation to a json one.
     * @param RDT<RDT> (response) data type of the response. You can see this as a wrapper around the HttpResponse class if you would be using micronaut.
     */
    fun <DT, RDT> httpify(response: Response<DT>, resultMapper: (result: DT) -> RDT): ViaeHttpResponse<RDT> {
        return if (response.hasPermissionViolation()) {
            UnAuthorizedResponseType("error-message" to "Insufficient Permissions")
        } else if (response.hasErrors()) {
            BadRequestResponseType(response.errors)
        } else {
            OkResponseType(resultMapper(response.data))
        }
    }
}
