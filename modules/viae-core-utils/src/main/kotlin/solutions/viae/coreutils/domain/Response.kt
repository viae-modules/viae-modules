package solutions.viae.coreutils.domain

import solutions.viae.coreutils.domain.ErrorType.NOT_FOUND
import solutions.viae.coreutils.domain.ErrorType.PERMISSION_FAILURE
import solutions.viae.coreutils.service.Logger
import java.util.stream.Collectors
import kotlin.reflect.KClass

interface Response<T> {
    val data: T
        get() {
            if (this.hasErrors()) {
                throw IllegalStateException("trying to get data while there are errors: ${errors.joinToString(";")}")
            }
            return data
        }
    val errors: List<ErrorMessage>

    fun isWithData(): Boolean

    fun <R> map(transform: (data: T?) -> R): Response<R> {
        return if (this.hasNoErrors()) {
            success(transform(if (this is ResponseWithData) data else null))
        } else {
            failure(errors)
        }
    }

    fun orIllegalState(): T? {
        return orElse { IllegalStateException(it) }
    }

    fun orElse(onError: (errors: String) -> Throwable): T? {
        return if (this.hasNoErrors() && this is ResponseWithData) {
            data
        } else if (this.hasNoErrors()) {
            null
        } else {
            throw onError(errors.joinToString(";"))
        }
    }

    fun hasErrors(): Boolean {
        return errors.isNotEmpty()
    }

    fun hasNoErrors(): Boolean {
        return !hasErrors()
    }

    fun writeErrors(logger: Logger, description: String) {
        logger.error(
            """${"\n"}
$description
${errors.stream().map { e -> "\t\t- $e" }.collect(Collectors.joining("\n"))}${"\n"}
            """.trimIndent()
        )
    }

    fun hasPermissionViolation(): Boolean {
        return errors.any { PERMISSION_FAILURE == it.errorType }
    }

    fun hasNotFoundViolation(): Boolean {
        return errors.any { NOT_FOUND == it.errorType }
    }

    companion object {
        fun success(): Response<Unit> {
            return ResponseWithoutData(errors = emptyList())
        }

        fun <T> success(data: T): Response<T> {
            return ResponseWithData(data = data, errors = emptyList())
        }

        fun <T> failure(source: KClass<*>, message: String): Response<T> {
            return ResponseWithoutData(
                errors = listOf(
                    ErrorMessage(
                        source,
                        message,
                        "default"
                    )
                )
            )
        }

        fun <T> failure(message: ErrorMessage): Response<T> {
            return ResponseWithoutData(errors = listOf(message))
        }

        fun <T> failure(messages: List<ErrorMessage>): Response<T> {
            return ResponseWithoutData(errors = messages)
        }
    }
}

class ResponseWithData<T>(
    data: T,
    override val errors: List<ErrorMessage>
) : Response<T> {

    override val data: T = data
        get() {
            check(errors.isEmpty()) { "can't get data: there are exceptions: [${errors.joinToString(";")}]" }
            return field
        }

    override fun isWithData(): Boolean {
        return true
    }
}

class ResponseWithoutData<T>(
    override val errors: List<ErrorMessage>
) : Response<T> {

    override val data: T
        get() {
            check(errors.isEmpty()) { "can't get data: there are exceptions: ${errors.joinToString(";")}" }
            System.err.println("the used response doesn't contain a data object")
            System.err.println(errors.joinToString(";"))
            System.err.println(Thread.currentThread().stackTrace.joinToString("\n"))
            throw UnsupportedOperationException("the used response doesn't contain a data object")
        }

    override fun isWithData(): Boolean {
        return false
    }

}
