package solutions.viae.coreutils.domain

import java.util.*

data class DatabaseId(val value: UUID = UUID.randomUUID()) {
    fun asString(): String {
        return value.toString()
    }

    companion object {
        fun fromString(value: String): DatabaseId {
            return DatabaseId(UUID.fromString(value))
        }
    }
}
