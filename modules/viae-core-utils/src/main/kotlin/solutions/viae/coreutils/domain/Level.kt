package solutions.viae.coreutils.domain

enum class Level {
    ERROR, INFO, WARN, DEBUG, TRACE
}
