package solutions.viae.coreutils.domain

import solutions.viae.coreutils.service.Logger
import java.util.concurrent.Flow

class DefaultSubscriber<T>(
    private val logger: Logger,
    private val actionName: String,
    private val onNextMapper: (t: T) -> Any
) : Flow.Subscriber<T> {
    override fun onSubscribe(subscription: Flow.Subscription) {
        logger.debug("subscribed for $actionName")
        subscription.request(1)
    }

    override fun onNext(item: T) {
        logger.debug("got next item for $actionName: $item")
        this.onNextMapper(item)
    }

    override fun onError(throwable: Throwable) {
        logger.error(throwable, "error on subscription for $actionName")
    }

    override fun onComplete() {
        logger.debug("completed subscription for $actionName")
    }
}
