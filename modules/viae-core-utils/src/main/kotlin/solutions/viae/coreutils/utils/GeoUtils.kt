package solutions.viae.coreutils.utils

import solutions.viae.coreutils.domain.BoundingBox
import solutions.viae.coreutils.domain.LatLng
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt

object GeoUtils {
    private const val rEarthInKm = 6378
    private const val metersInAKm = 1000.0
    private const val earthCircleRadius = 6371.0
    private const val degreesOfHalfACircle = 180

    /**
     * haversine method
     * Returns distance in KM
     */
    fun distanceInKm(
        location1: LatLng,
        location2: LatLng
    ): Double {

        val dLat = Math.toRadians(location2.lat - location1.lat)
        val dLon = Math.toRadians(location2.lng - location1.lng)
        // convert to radians
        val lat1: Double = Math.toRadians(location1.lat)
        val lat2: Double = Math.toRadians(location2.lat)

        // apply formulae
        val a = sin(dLat / 2).pow(2.0) +
                sin(dLon / 2).pow(2.0) *
                cos(lat1) *
                cos(lat2)
        val rad = earthCircleRadius
        val c = 2 * asin(sqrt(a))
        return rad * c
    }

    fun getBoundingBoxFor(center: LatLng, radiusInMeter: Long, correctionFactor: Double = 1.0): BoundingBox {
        return BoundingBox(
            center = center,
            radiusInMeter = radiusInMeter,
            north = getNewLatLng(center, radiusInMeter * 0, radiusInMeter * 1, correctionFactor),
            south = getNewLatLng(center, radiusInMeter * 0, radiusInMeter * -1, correctionFactor),
            east = getNewLatLng(center, radiusInMeter * 1, radiusInMeter * 0, correctionFactor),
            west = getNewLatLng(center, radiusInMeter * -1, radiusInMeter * 0, correctionFactor)
        )
    }

    fun toRadians(distanceInMeter: Long): Double {
        val distanceInKm:Double = distanceInMeter / metersInAKm
        return distanceInKm / rEarthInKm
    }

    private fun getNewLatLng(center: LatLng, dxInMeter: Long, dyInMeter: Long, correctionFactor: Double): LatLng {
        val dyInKm:Double = dxInMeter / metersInAKm
        val dxInKm:Double = dyInMeter / metersInAKm
        val newLatitude = center.lat + (dyInKm / rEarthInKm) * (degreesOfHalfACircle / Math.PI)
        val newLongitude = center.lng + (dxInKm / rEarthInKm) * (degreesOfHalfACircle / Math.PI) / Math.cos(center.lat * Math.PI / degreesOfHalfACircle)
        return LatLng(
            newLatitude * correctionFactor,
            newLongitude * correctionFactor
        )
    }
}
