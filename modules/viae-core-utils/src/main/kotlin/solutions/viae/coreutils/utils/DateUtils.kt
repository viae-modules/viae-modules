package solutions.viae.coreutils.utils

import solutions.viae.coreutils.domain.Timing
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Optional
import java.util.Optional.empty

class DateUtils private constructor() {

    companion object {
        private val config = try {
            val path =
                DateUtils::class.java.classLoader.getResource("viae-configuration.properties")
                    ?: throw IllegalStateException("Could not read properties file. Is 'viae-configuration.properties' existing?")
            Files.readAllLines(Paths.get(path.toURI()))
        } catch (e: Exception) {
            System.err.println("Could not read properties file. Is 'viae-configuration.properties' existing?")
            throw IllegalStateException("Could not read properties file. Is 'viae-configuration.properties' existing?")
        }

        private val TS_FORMATTER =
            DateTimeFormatter.ofPattern(config.first { it.startsWith("pattern_ts_formatter") }
                .replace("pattern_ts_formatter=", ""))
        private val TS_WITHOUT_NANO_DOTS =
            DateTimeFormatter.ofPattern(config.first { it.startsWith("pattern_ts_without_nano_dots") }
                .replace("pattern_ts_without_nano_dots=", ""))
        private val DATE_FORMATTER =
            DateTimeFormatter.ofPattern(config.first { it.startsWith("pattern_date_formatter") }
                .replace("pattern_date_formatter=", ""))
        private val ZONE =
            ZoneId.of(config.first { it.startsWith("default_zone_id") }
                .replace("default_zone_id=", ""))

        fun toString(ts: ZonedDateTime): String {
            return ts.format(TS_FORMATTER)
        }

        fun toTimeStamp(ts: String, zone: ZoneId = ZONE): ZonedDateTime {
            return ZonedDateTime.of(LocalDateTime.parse(ts, TS_FORMATTER), zone).withZoneSameInstant(ZONE)
        }

        fun toDefaultZone(ts: ZonedDateTime): ZonedDateTime {
            return ts.withZoneSameInstant(ZONE)
        }

        fun nowAsString(): String {
            return ZonedDateTime.now(ZONE).format(TS_FORMATTER)
        }

        fun toStringAndRemoveSpecialCharactersAndDotsFromDate(value: ZonedDateTime = ZonedDateTime.now(ZONE)): String {
            return value.format(TS_WITHOUT_NANO_DOTS)
                .replace(" ".toRegex(), "_")
                .replace(":".toRegex(), "")
                .replace("-".toRegex(), "")
                .replace("\\.".toRegex(), "_")
        }

        fun nowDateAsEncodedString(): String {
            return ZonedDateTime.now(ZONE).format(DATE_FORMATTER)
                .replace(" ".toRegex(), "_")
                .replace(":".toRegex(), "")
                .replace("-".toRegex(), "")
        }

        fun now(): ZonedDateTime {
            return ZonedDateTime.now(ZONE)
        }

        fun toDate(date: String?): LocalDate? {
            return if (date != null) LocalDate.parse(date, DATE_FORMATTER) else null
        }

        fun of(year: Int, monthValue: Int, dayOfMonth: Int, hour: Int = 0, minute: Int = 0, seconds: Int = 0): ZonedDateTime {
            return ZonedDateTime.of(LocalDateTime.of(year, monthValue, dayOfMonth, hour, minute, seconds), ZONE)
        }

        fun getDate(day: Timing.Day, nthDayOfMonth: Int, year: Int, month: Int): Optional<LocalDate> {
            var result = empty<LocalDate>()

            if (nthDayOfMonth > 0) {
                var index = LocalDate.of(year, month, 1)

                var nthOfMonthCounter = 0
                while (index.isBefore(LocalDate.of(year, month + 1, 1)) && !result.isPresent()) {
                    val dayOfWeek = index.dayOfWeek
                    if (dayOfWeek.value == day.dayOfWeek.value) {
                        nthOfMonthCounter++
                    }
                    if (nthOfMonthCounter == nthDayOfMonth) {
                        result = Optional.of(index)
                    }
                    index = index.plusDays(1)
                }
            }

            return result
        }
    }
}
