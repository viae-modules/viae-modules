package solutions.viae.coreutils.utils

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.domain.Response.Companion.success
import kotlin.reflect.KClass

fun <T> runValid(c: (v: Validator) -> Response<T>): Response<T> {
    val v = Validator()
    return try {
        c(v)
    } catch (e: IllegalArgumentException) {
        failure(v.getErrors())
    }
}

class Validator {
    private val errors = mutableListOf<ErrorMessage>()
    private var failFast: Boolean = false

    fun isNotBlank(value: String?, message: ErrorMessage) {
        ObjectUtils.ifBlank(value) { addError(message) }
    }

    fun isNotNull(value: Any?, message: ErrorMessage) {
        ObjectUtils.ifNull(value) { addError(message) }
    }

    fun isNotTrue(value: Boolean, message: ErrorMessage) {
        if (value) {
            addError(message)
        }
    }

    fun isTrue(value: Boolean, message: ErrorMessage) {
        if (!value) {
            addError(message)
        }
    }

    fun <T> tryTransform(transform: () -> T, message: ErrorMessage): T? {
        return ObjectUtils.tryTransform(
            transform,
            {
                addError(message.copy(message = "${message.message}, cause: ${it.localizedMessage}"))
            })
    }

    fun <T> tryTransformFromResponse(transform: () -> Response<T>, message: ErrorMessage): T? {
        return ObjectUtils.tryTransformFromResponse(
            transform,
            {
                addError(message.copy(message = "${message.message}, cause: ${it.localizedMessage}"))
            })
    }

    fun <T : Enum<T>> toEnum(
        value: String?,
        enumClass: KClass<T>,
        message: ErrorMessage,
        errorGroup: String = "default"
    ): T? {
        return if (value != null) {
            try {
                enumFromName(enumClass, value)
            } catch (e: java.lang.IllegalArgumentException) {
                addError(message.copy(errorGroup = errorGroup))
                null
            }
        } else {
            addError(message.copy(errorGroup = errorGroup))
            null
        }
    }

    fun <T> and(vararg prerequisiteIds: String, callback: () -> T): T? {
        val set = prerequisiteIds.toSet()
        return if (errors.none { set.contains(it.errorGroup) }) {
            callback()
        } else {
            null
        }
    }

    fun <T> respond(callback: () -> T): Response<T> {
        return if (errors.isEmpty()) {
            success(callback())
        } else {
            failure(errors)
        }
    }

    private fun addError(message: ErrorMessage) {
        errors.add(message)
        if (failFast) {
            throw IllegalArgumentException()
        }
    }

    fun failFast() {
        this.failFast = true;
    }

    fun continueOnError() {
        this.failFast = false;
    }

    fun hasErrors(): Boolean {
        return errors.isNotEmpty()
    }

    fun hasNoErrors(): Boolean {
        return !hasErrors()
    }

    fun getErrors(): MutableList<ErrorMessage> {
        return this.errors
    }

    fun <T> result(value: T): Response<T> {
        return if (errors.isEmpty()) Response.success(value) else Response.failure(errors)
    }
}

