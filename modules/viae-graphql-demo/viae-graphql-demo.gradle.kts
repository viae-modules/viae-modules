dependencies {
    api(Libraries.Kotlinx.serializationRuntime)

    kapt(Libraries.Micronaut.injectJava)

    implementation(Libraries.Micronaut.core)
    implementation(Libraries.Micronaut.runtime)
    implementation(Libraries.Micronaut.httpClient)
    implementation(Libraries.Micronaut.httpServerNetty)
    implementation(Libraries.Micronaut.graphql)

}

plugins {
    application
}

val viaeMainClassName = "solutions.viae.graphqldemo.Application"
val projectSources = project.property("sourceSets") as SourceSetContainer
val sourceSets = projectSources.getByName("main").output.asPath
tasks {
    val runLocal by registering(JavaExec::class) {
        classpath = projectSources.getByName("main").runtimeClasspath
        main = viaeMainClassName
        jvmArgs("-Dmicronaut.environments=local")
    }

    val jar by getting(Jar::class) {
        manifest {
            attributes["Main-Class"] = viaeMainClassName
            attributes["Multi-Release"] = true
        }
    }
}

application {
    mainClassName = viaeMainClassName
}
