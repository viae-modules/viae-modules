package solutions.viae.graphqldemo.graphql.datafetcher

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import java.util.*
import java.util.concurrent.TimeUnit

import javax.inject.Singleton

@Singleton
class HelloDataFetcher : DataFetcher<Person> {

    override fun get(env: DataFetchingEnvironment): Person {
        var name = env.getArgument<String>("name")
        if (name == null || name.trim().isEmpty()) {
            name = "World"
        }

        val address = if(env.selectionSet.fields.any { it.name.toLowerCase().equals("address") }){
            TimeUnit.SECONDS.sleep(sleepInterval)
            Address(
                ref = UUID.randomUUID().toString(),
                street = "street",
                houseNumber = "houseNumber",
                city = "city",
                postalCode = "postalCode",
                country = "country"
            )
        } else {
            null
        }
        return Person(
            ref = UUID.randomUUID().toString(),
            firstName = "Hello $name!",
            lastName = "Last name of $name",
            emailAddress = "$name@test.com",
            gender = Gender.MALE,
            address = address
        )
    }

    companion object {
        private const val sleepInterval: Long = 3
    }
}

@Singleton
class FuckOffDataFetcher : DataFetcher<String> {

    override fun get(env: DataFetchingEnvironment): String {
        var name = env.getArgument<String>("name")
        if (name == null || name.trim().isEmpty()) {
            name = "World"
        }
        return "Fuck off $name!"
    }
}

data class Address (
    val ref: String,
    val street: String,
    val houseNumber: String,
    val city: String,
    val postalCode: String,
    val country: String,
)

data class Person(
    val ref: String,
    val firstName: String,
    val lastName: String,
    val emailAddress: String?,
    val gender: Gender,
    val address: Address?
)

enum class Gender {
    MALE, FEMALE
}
