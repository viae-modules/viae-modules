package solutions.viae.graphqldemo

import io.micronaut.runtime.Micronaut
import solutions.viae.graphqldemo.config.Config

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages(Config::class.java.packageName)
            .mainClass(Application.javaClass)
            .start()
    }
}
