package solutions.viae.oauth2validation.demo

import io.micronaut.context.annotation.Value
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import solutions.viae.oauth2validation.demo.components.Application
import solutions.viae.oauth2validation.demo.components.api.SecuredControllerClient
import solutions.viae.oauth2validation.demo.components.config.Configuration
import solutions.viae.oauth2validation.domain.OAuth2ServerConfig
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createClientIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRealmIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRoleIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createUser
import solutions.viae.oauth2validation.helpers.KeycloakHelper.getAccessToken
import javax.inject.Inject

/**
 * global (i.e. keycloak realm level) security test
 */
@MicronautTest(
    application = Application::class,
    environments = ["ci"]
)
class OAuthValidationDemoTest {

    @Inject
    private lateinit var securedControllerClient: SecuredControllerClient

    @Value("\${viae.sso.server-url}")
    lateinit var serverUrl: String

    @Test
    fun validatePermissionForValidLogonOnHasAll() {
        //Given
        val realm = "test"
        val userName = "test-all-for-application"
        val password = "test"
        val clientId = "test-client"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createRoleIfNotExists(serverUrl, realm, "READ")
        createRoleIfNotExists(serverUrl, realm, "READ_SPECIAL")
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret)
        val userData = createUser(serverUrl, realm, userName, password, listOf("READ", "READ_SPECIAL"), emptyMap(), mapOf("test-user-attribute" to "some-value"))
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = securedControllerClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = securedControllerClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = securedControllerClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnAll.body())).isEqualTo("this is the secured content for this is the secured content for some argument with attributes {test-user-attribute=\"some-value\"}")
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
        Assertions.assertThat((responseOnNone.body())).isEqualTo(null)
    }

    @Test
    fun validatePermissionForValidLogonOnHasOneOfAndHavingOne() {
        //Given
        val realm = "test"
        val userName = "test-one-of-having-one-for-application"
        val password = "test"
        val clientId = "test-client"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createRoleIfNotExists(serverUrl, realm, "READ")
        createRoleIfNotExists(serverUrl, realm, "READ_SPECIAL")
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret)
        val userData = createUser(serverUrl, realm, userName, password, listOf("READ"))
        val keycloakUserName = userData.second
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret
        val token = getAccessToken(serverUrl, realm, "test-client", keycloakUserName, password)

        //When
        val responseOnAll = securedControllerClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = securedControllerClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = securedControllerClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
        Assertions.assertThat((responseOnNone.body())).isEqualTo(null)
    }

    @Test
    fun validatePermissionForValidLogonOnHasOneOfAnNotHavingOne() {
        //Given
        val realm = "test"
        val userName = "test-one-of-not-having-one-for-application"
        val password = "test"
        val clientId = "test-client"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createRoleIfNotExists(serverUrl, realm, "READ")
        createRoleIfNotExists(serverUrl, realm, "READ_SPECIAL")
        createRoleIfNotExists(serverUrl, realm, "NOT_CHECKED")
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret)
        val userData = createUser(serverUrl, realm, userName, password, listOf("NOT_CHECKED"))
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = securedControllerClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = securedControllerClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = securedControllerClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo(null)
        Assertions.assertThat((responseOnNone.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
    }

    @Test
    fun validatePermissionForValidLogonOnHasNone() {
        //Given
        val realm = "test"
        val userName = "test-none-for-application"
        val password = "test"
        val clientId = "test-client"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createRoleIfNotExists(serverUrl, realm, "READ")
        createRoleIfNotExists(serverUrl, realm, "READ_SPECIAL")
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret)
        val userData = createUser(serverUrl, realm, userName, password, emptyList())
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = securedControllerClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = securedControllerClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = securedControllerClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo(null)
        Assertions.assertThat((responseOnNone.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
    }
}
