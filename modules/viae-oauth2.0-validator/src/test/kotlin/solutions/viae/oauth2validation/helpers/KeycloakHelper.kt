package solutions.viae.oauth2validation.helpers

import kotlinx.serialization.json.Json
import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.ProtocolMapperRepresentation
import org.keycloak.representations.idm.RealmRepresentation
import org.keycloak.representations.idm.RoleRepresentation
import org.keycloak.representations.idm.UserRepresentation
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.stream.Collectors
import javax.ws.rs.NotFoundException
import javax.ws.rs.ProcessingException


object KeycloakHelper {

    private val keycloak = KeycloakBuilder.builder()
        .realm("master")
        .username("admin")
        .password("admin")
        .clientId("admin-cli")


    fun createUser(
        serverUrl: String,
        realm: String,
        userName: String,
        password: String,
        roles: List<String>,
        clientRoles: Map<String, List<String>> = emptyMap(),
        userAttributes: Map<String, String> = emptyMap()
    ): Pair<String, String> {
        synchronized(this) {
            val userResource = keycloak.serverUrl("${serverUrl}/auth").build().realm(realm).users()
            val rolesResource = keycloak.serverUrl("${serverUrl}/auth").build().realm(realm).roles()
            val clientsResource = keycloak.serverUrl("${serverUrl}/auth").build().realm(realm).clients()

            val user = UserRepresentation()
            user.isEnabled = true
            user.username = "${LocalDateTime.now()}-$userName"
            user.firstName = userName
            user.lastName = LocalDateTime.now().toString()
            user.realmRoles = roles
            user.attributes = userAttributes.mapValues { listOf(it.value) }
            val response = userResource.create(user)
            val userId = response.location.path.replace(".*/([^/]+)$".toRegex(), "$1")

            val passwordCred = CredentialRepresentation()
            passwordCred.isTemporary = false
            passwordCred.type = CredentialRepresentation.PASSWORD
            passwordCred.value = password

            userResource.get(userId).resetPassword(passwordCred)
            userResource.get(userId).roles().realmLevel().add(roles.map { rolesResource.get(it).toRepresentation() })
            clientRoles.entries.map { entry ->
                val clientId = clientsResource.findByClientId(entry.component1()).get(0).id
                val keycloakClientRoles = entry.component2().map { clientsResource.get(clientId).roles().get(it).toRepresentation() }
                userResource.get(userId).roles().clientLevel(clientId).add(keycloakClientRoles)
            }

            return Pair(userId, user.username)
        }
    }

    fun createRoleIfNotExists(serverUrl: String, realm: String, role: String) {
        synchronized(this) {
            val roleResource = keycloak.serverUrl("${serverUrl}/auth").build().realm(realm).roles()
            try {
                roleResource.get(role).toRepresentation()
            } catch (exception: Exception) {
                when (exception) {
                    is NotFoundException,
                    is ProcessingException -> {
                        val keycloakRole = RoleRepresentation()
                        keycloakRole.name = role
                        roleResource.create(keycloakRole)
                    }
                    else -> throw exception
                }
            }
        }
    }

    fun createClientIfNotExists(serverUrl: String, realm: String, clientId: String, clientSecret: String, clientRoles: List<String> = emptyList()) {
        synchronized(this) {
            val resource = keycloak.serverUrl("${serverUrl}/auth").build().realm(realm).clients()
            if (resource.findByClientId(clientId).size == 0) {
                val mapper = ProtocolMapperRepresentation()
                mapper.name = "client-mapper-id"
                mapper.protocol = "openid-connect"
                mapper.protocolMapper = "oidc-usermodel-attribute-mapper"
                mapper.config.put("user.attribute", "test-user-attribute")
                mapper.config.put("id.token.claim", "true")
                mapper.config.put("access.token.claim", "true")
                mapper.config.put("jsonType.label", "String")
                mapper.config.put("userinfo.token.claim", "true")
                mapper.config.put("claim.name", "attributes.test-user-attribute") //how it will be displayed in the token => dot notation results in an attributes object with the field test-user-attribute

                val keycloakClient = ClientRepresentation()
                keycloakClient.clientId = clientId
                keycloakClient.secret = clientSecret
                keycloakClient.adminUrl = serverUrl
                keycloakClient.webOrigins = listOf(serverUrl)
                keycloakClient.redirectUris = listOf("${serverUrl}/*")
                keycloakClient.isDirectAccessGrantsEnabled = true
                keycloakClient.protocolMappers = listOf(mapper)
                resource.create(keycloakClient)

                clientRoles.forEach {
                    val role = RoleRepresentation()
                    role.name = it
                    role.clientRole = true
                    val keycloakRoles = resource.get(resource.findByClientId(clientId)[0].id).roles()
                    keycloakRoles.create(role)
                }
            }
        }
    }

    fun createRealmIfNotExists(serverUrl: String, realm: String) {
        synchronized(this) {
            println("create realm on server url: '${serverUrl}/auth'")
            val resource = keycloak.serverUrl("${serverUrl}/auth").build().realms()

            val stream = URL("$serverUrl/auth/realms/master/protocol/openid-connect/certs").openStream()
            val json = BufferedReader(InputStreamReader(stream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining())
            val certInfos = Json.Default.parseJson(json).jsonObject
            println("available realms: ${certInfos}")
            println("available realms: ${resource.findAll().joinToString(",")}")

            try {
                resource.realm(realm).toRepresentation()
            } catch (exception: Exception) {
                when (exception) {
                    is NotFoundException,
                    is ProcessingException -> {
                        val keycloakRealm = RealmRepresentation()
                        keycloakRealm.displayName = realm
                        keycloakRealm.realm = realm
                        keycloakRealm.isEnabled = true
                        resource.create(keycloakRealm)
                    }
                    else -> throw exception
                }
            }
        }
    }

    fun getAccessToken(serverUrl: String, realm: String, clientId: String, userName: String, password: String): String {
        synchronized(this) {
            val keycloak = KeycloakBuilder.builder()
                .serverUrl("${serverUrl}/auth")
                .realm(realm)
                .username(userName)
                .password(password)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret("7df1f2eb-56bc-4c98-94ed-90c68decb928") //TODO should be parameter as well
                .build()
            return keycloak.tokenManager().accessToken.token
        }
    }
}
