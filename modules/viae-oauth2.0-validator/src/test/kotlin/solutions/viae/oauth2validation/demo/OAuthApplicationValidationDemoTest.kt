package solutions.viae.oauth2validation.demo

import io.micronaut.context.annotation.Value
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import solutions.viae.oauth2validation.demo.components.Application
import solutions.viae.oauth2validation.demo.components.api.ApplicationSecuredControllerClient
import solutions.viae.oauth2validation.demo.components.config.Configuration
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createClientIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRealmIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRoleIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createUser
import solutions.viae.oauth2validation.helpers.KeycloakHelper.getAccessToken
import javax.inject.Inject

/**
 * application level (i.e. keycloak client level) security test
 */
@MicronautTest(
    application = Application::class,
    environments = ["ci"]
)
class OAuthApplicationValidationDemoTest {

    private val availableClientRoles = listOf("READ", "READ_SPECIAL", "NOT_CHECKED")
    @Inject
    private lateinit var applicationSecuredClient: ApplicationSecuredControllerClient

    @Value("\${viae.sso.server-url}")
    lateinit var serverUrl: String

    @Test
    fun validatePermissionForValidLogonOnHasAll() {
        //Given
        val realm = "test"
        val userName = "test-all"
        val password = "test"
        val clientId = "dummy-application"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        val clientRoles = mapOf(
            "dummy-application" to listOf("READ", "READ_SPECIAL")
        )
        //And
        createRealmIfNotExists(serverUrl, realm)
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret, availableClientRoles)
        val userData = createUser(serverUrl, realm, userName, password, emptyList(), clientRoles)
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = applicationSecuredClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = applicationSecuredClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = applicationSecuredClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnAll.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
        Assertions.assertThat((responseOnNone.body())).isEqualTo(null)
    }

    @Test
    fun validatePermissionForValidLogonOnHasOneOfAndHavingOne() {
        //Given
        val realm = "test"
        val userName = "test-one-of-having-one"
        val password = "test"
        val clientId = "dummy-application"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret, availableClientRoles)
        val userData = createUser(serverUrl, realm, userName, password, emptyList(), mapOf("dummy-application" to listOf("READ")))
        val keycloakUserName = userData.second
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret
        val token = getAccessToken(serverUrl, realm, "dummy-application", keycloakUserName, password)

        //When
        val responseOnAll = applicationSecuredClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = applicationSecuredClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = applicationSecuredClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
        Assertions.assertThat((responseOnNone.body())).isEqualTo(null)
    }

    @Test
    fun validatePermissionForValidLogonOnHasOneOfAnNotHavingOne() {
        //Given
        val realm = "test"
        val userName = "test-one-of-not-having-one"
        val password = "test"
        val clientId = "dummy-application"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret, availableClientRoles)
        val userData = createUser(serverUrl, realm, userName, password,  emptyList(), mapOf("dummy-application" to listOf("NOT_CHECKED")))
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = applicationSecuredClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = applicationSecuredClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = applicationSecuredClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo(null)
        Assertions.assertThat((responseOnNone.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
    }

    @Test
    fun validatePermissionForValidLogonOnHasNone() {
        //Given
        val realm = "test"
        val userName = "test-none"
        val password = "test"
        val clientId = "dummy-application"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createClientIfNotExists(serverUrl, realm, clientId, clientSecret, availableClientRoles)
        val userData = createUser(serverUrl, realm, userName, password, emptyList())
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientId, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientId
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = applicationSecuredClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = applicationSecuredClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = applicationSecuredClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(401)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnAll.body())).isEqualTo(null)
        Assertions.assertThat((responseOnOneOf.body())).isEqualTo(null)
        Assertions.assertThat((responseOnNone.body())).isEqualTo("this is the secured content for this is the secured content for some argument")
    }
}
