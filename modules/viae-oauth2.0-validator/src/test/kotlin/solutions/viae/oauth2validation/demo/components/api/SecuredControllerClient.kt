package solutions.viae.oauth2validation.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.client.annotation.Client

@Client(
    value = "/api/secured",
    errorType = String::class
)
interface SecuredControllerClient {

    @Get("/on-all", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnAll(@Header("Authorization") authorizationHeader: String): HttpResponse<String>

    @Get("/on-one-of", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnOneOf(@Header("Authorization") authorizationHeader: String): HttpResponse<String>

    @Get("/on-none", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnNone(@Header("Authorization") authorizationHeader: String): HttpResponse<String>
}
