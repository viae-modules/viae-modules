package solutions.viae.oauth2validation.e2e

import io.micronaut.context.annotation.Value
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import solutions.viae.oauth2validation.demo.components.Application
import solutions.viae.oauth2validation.demo.components.api.SecuredControllerClient
import solutions.viae.oauth2validation.demo.components.config.Configuration
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createClientIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRealmIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createRoleIfNotExists
import solutions.viae.oauth2validation.helpers.KeycloakHelper.createUser
import solutions.viae.oauth2validation.helpers.KeycloakHelper.getAccessToken
import javax.inject.Inject

@MicronautTest(
    application = Application::class,
    environments = ["ci"]
)
class ClientValidationIT {

    @Inject
    private lateinit var securedControllerClient: SecuredControllerClient

    @Value("\${viae.sso.server-url}")
    lateinit var serverUrl: String

    @Test
    fun `when issuer client is not the same as the client, validation should not fail`(){
        //Given
        val realm = "test"
        val userName = "client-test-all"
        val password = "test"
        val clientIdFromServerValidation = "test-client-validation-1"
        val clientIdForTokenRequest = "test-client-validation-2"
        val clientSecret = "7df1f2eb-56bc-4c98-94ed-90c68decb928"
        //And
        createRealmIfNotExists(serverUrl, realm)
        createRoleIfNotExists(serverUrl, realm, "READ")
        createRoleIfNotExists(serverUrl, realm, "READ_SPECIAL")
        createClientIfNotExists(serverUrl, realm, clientIdFromServerValidation, clientSecret)
        createClientIfNotExists(serverUrl, realm, clientIdForTokenRequest, clientSecret)
        val userData = createUser(serverUrl, realm, userName, password, listOf("READ", "READ_SPECIAL"))
        val keycloakUserName = userData.second
        val token = getAccessToken(serverUrl, realm, clientIdForTokenRequest, keycloakUserName, password)
        //And
        Configuration.configurationCache["client-id"] = clientIdFromServerValidation
        Configuration.configurationCache["client-secret"] = clientSecret

        //When
        val responseOnAll = securedControllerClient.getSecuredDataOnAll("Bearer $token")
        val responseOnNone = securedControllerClient.getSecuredDataOnNone("Bearer $token")
        val responseOnOneOf = securedControllerClient.getSecuredDataOnOneOf("Bearer $token")

        //Then
        Assertions.assertThat((responseOnAll.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnOneOf.status().code)).isEqualTo(200)
        Assertions.assertThat((responseOnNone.status().code)).isEqualTo(401)
    }
}
