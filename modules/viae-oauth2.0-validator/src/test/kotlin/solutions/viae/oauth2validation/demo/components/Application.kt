package solutions.viae.oauth2validation.demo.components

import io.micronaut.runtime.Micronaut
import solutions.viae.oauth2validation.demo.components.api.SecuredController

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages(SecuredController::class.java.packageName)
            .mainClass(Application.javaClass)
            .start()
    }
}
