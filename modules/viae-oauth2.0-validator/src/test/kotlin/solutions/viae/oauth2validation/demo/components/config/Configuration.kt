package solutions.viae.oauth2validation.demo.components.config

import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Value
import io.micronaut.http.context.ServerRequestContext
import solutions.viae.oauth2validation.demo.components.usecases.*
import solutions.viae.oauth2validation.domain.OAuth2ServerConfig
import solutions.viae.oauth2validation.domain.UserAccessToken
import solutions.viae.oauth2validation.services.UserContextHolder
import javax.inject.Singleton

@Factory
class Configuration {

    @Value("\${viae.sso.server-url}")
    lateinit var serverUrl: String

    @Singleton
    fun useCaseForAll(userContextHolder: UserContextHolder): UseCaseWithSecurityForAllPermissions {
        return UseCaseWithSecurityForAllPermissions(userContextHolder)
    }

    @Singleton
    fun useCaseForNone(userContextHolder: UserContextHolder): UseCaseWithSecurityForNoneOfPermissions {
        return UseCaseWithSecurityForNoneOfPermissions(userContextHolder)
    }

    @Singleton
    fun useCaseForOneOf(userContextHolder: UserContextHolder): UseCaseWithSecurityForOneOfPermissions {
        return UseCaseWithSecurityForOneOfPermissions(userContextHolder)
    }


    @Singleton
    fun useCaseForAllOnApplicationLevel(userContextHolder: UserContextHolder): UseCaseWithDummyApplicationSecurityForAllPermissions {
        return UseCaseWithDummyApplicationSecurityForAllPermissions(userContextHolder)
    }

    @Singleton
    fun useCaseForNoneOnApplicationLevel(userContextHolder: UserContextHolder): UseCaseWithDummyApplicationSecurityForNoneOfPermissions {
        return UseCaseWithDummyApplicationSecurityForNoneOfPermissions(userContextHolder)
    }

    @Singleton
    fun useCaseForOneOfOnApplicationLevel(userContextHolder: UserContextHolder): UseCaseWithDummyApplicationSecurityForOneOfPermissions {
        return UseCaseWithDummyApplicationSecurityForOneOfPermissions(userContextHolder)
    }

    @Singleton
    fun userContextHolder(): UserContextHolder {
        return UserContextHolder(OAuth2ServerConfig(
            "${serverUrl}/auth",
            "test",
            configurationCache["client-id"] ?: throw IllegalStateException("'client-id' is not set"),
            configurationCache["client-secret"] ?: throw IllegalStateException("'client-secret' is not set")
        )) {
            try {
                UserAccessToken(
                    ServerRequestContext.currentRequest<Any>().orElseThrow { IllegalStateException("no request context present") }
                        .headers
                        .authorization.orElseThrow { IllegalStateException("no authorization header present") }
                        .split(" ")[1]
                )
            } catch (exception: Exception){
                System.err.println(exception.localizedMessage)
                throw IllegalStateException(exception)
            }
        }
    }

    companion object {
        val configurationCache = mutableMapOf<String, String>()
    }
}
