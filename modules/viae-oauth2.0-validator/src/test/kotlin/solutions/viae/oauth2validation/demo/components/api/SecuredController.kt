package solutions.viae.oauth2validation.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponse.ok
import io.micronaut.http.HttpResponse.unauthorized
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import solutions.viae.oauth2validation.demo.components.usecases.UseCaseWithSecurityForAllPermissions
import solutions.viae.oauth2validation.demo.components.usecases.UseCaseWithSecurityForNoneOfPermissions
import solutions.viae.oauth2validation.demo.components.usecases.UseCaseWithSecurityForOneOfPermissions
import java.lang.Exception
import javax.inject.Inject

@Controller("/api/secured")
class SecuredController {
    @Inject
    private lateinit var useCaseWithSecurityForAllPermissions: UseCaseWithSecurityForAllPermissions
    @Inject
    private lateinit var useCaseWithSecurityForOneOfPermissions: UseCaseWithSecurityForOneOfPermissions
    @Inject
    private lateinit var useCaseWithSecurityForNoneOfPermissions: UseCaseWithSecurityForNoneOfPermissions

    @Get("/on-all", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnAll(): HttpResponse<String> {
        return try{
            ok(useCaseWithSecurityForAllPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }

    @Get("/on-none", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnNone(): HttpResponse<String> {
        return try{
            ok(useCaseWithSecurityForNoneOfPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }

    @Get("/on-one-of", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnOneOff(): HttpResponse<String> {
        return try{
            ok(useCaseWithSecurityForOneOfPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }
}
