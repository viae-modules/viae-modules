package solutions.viae.oauth2validation.demo.components.usecases

import solutions.viae.oauth2validation.services.UserContextHolder

class UseCaseWithSecurityForAllPermissions(
    private val userContextHolder: UserContextHolder
) {

    fun execute(request: String): String {
        val context = userContextHolder.userContext.orIllegalState()!!
        if (context.hasAllThePermissions("READ", "READ_SPECIAL")) {
            return "this is the secured content for $request with attributes ${context.attributes}"
        } else {
            throw IllegalStateException("not enough permissions")
        }
    }
}
