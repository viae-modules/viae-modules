package solutions.viae.oauth2validation.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponse.ok
import io.micronaut.http.HttpResponse.unauthorized
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import solutions.viae.oauth2validation.demo.components.usecases.*
import java.lang.Exception
import javax.inject.Inject

@Controller("/api/application-secured")
class ApplicationSecuredController {
    @Inject
    private lateinit var useCaseWithDummyApplicationSecurityForAllPermissions: UseCaseWithDummyApplicationSecurityForAllPermissions
    @Inject
    private lateinit var useCaseWithDummyApplicationSecurityForOneOfPermissions: UseCaseWithDummyApplicationSecurityForOneOfPermissions
    @Inject
    private lateinit var useCaseWithDummyApplicationSecurityForNoneOfPermissions: UseCaseWithDummyApplicationSecurityForNoneOfPermissions

    @Get("/on-all", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnAll(): HttpResponse<String> {
        return try{
            ok(useCaseWithDummyApplicationSecurityForAllPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }

    @Get("/on-none", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnNone(): HttpResponse<String> {
        return try{
            ok(useCaseWithDummyApplicationSecurityForNoneOfPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }

    @Get("/on-one-of", produces = [MediaType.APPLICATION_JSON])
    fun getSecuredDataOnOneOff(): HttpResponse<String> {
        return try{
            ok(useCaseWithDummyApplicationSecurityForOneOfPermissions.execute("this is the secured content for some argument"))
        } catch (exception: Exception){
            if(exception.localizedMessage.contains("not enough permissions")){
                unauthorized()
            } else {
                throw exception
            }
        }
    }
}
