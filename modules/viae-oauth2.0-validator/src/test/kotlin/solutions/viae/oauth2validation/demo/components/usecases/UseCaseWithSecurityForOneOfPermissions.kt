package solutions.viae.oauth2validation.demo.components.usecases

import solutions.viae.oauth2validation.services.UserContextHolder

class UseCaseWithSecurityForOneOfPermissions(
    private val userContextHolder: UserContextHolder
) {

    fun execute(request: String): String {
        val context = userContextHolder.userContext.orIllegalState()!!
        if (context.hasOneOfThePermissions("READ", "READ_SPECIAL")) {
            return "this is the secured content for $request"
        } else {
            throw IllegalStateException("not enough permissions")
        }
    }
}
