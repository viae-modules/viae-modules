package solutions.viae.oauth2validation.domain

/**
 * This is to support the confidential flow (i.e. the flow with client id and client secret)
 */
data class OAuth2ServerConfig (
    val serverUrl: String,
    val realm: String,
    val clientId: String,
    val clientSecret: String,
    val proxySettings: ProxySettings? = null
)

data class ProxySettings(
    val host: String,
    val port: Int
)
