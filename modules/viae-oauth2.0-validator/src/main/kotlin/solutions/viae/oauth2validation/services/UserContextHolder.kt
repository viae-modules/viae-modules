package solutions.viae.oauth2validation.services

import kotlinx.serialization.json.Json
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.domain.Response.Companion.success
import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.oauth2validation.domain.MediaType.applicationWwwFormUrlEncoded
import solutions.viae.oauth2validation.domain.OAuth2ServerConfig
import solutions.viae.oauth2validation.domain.Oauth2Permission
import solutions.viae.oauth2validation.domain.StatusCode
import solutions.viae.oauth2validation.domain.UserAccessToken
import solutions.viae.oauth2validation.domain.UserContext
import solutions.viae.oauth2validation.utils.ViaeBodyPublishers
import java.net.InetSocketAddress
import java.net.ProxySelector
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse.BodyHandlers
import java.time.Duration

class UserContextHolder(
    private val oAuth2ServerConfig: OAuth2ServerConfig,
    private val getCurrentUserAccessToken: () -> UserAccessToken
) {
    private val httpClient: HttpClient

    init {
        var builder = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .followRedirects(HttpClient.Redirect.NORMAL)
            .connectTimeout(Duration.ofSeconds(1))
        if (oAuth2ServerConfig.proxySettings != null) {
            builder = builder.proxy(ProxySelector.of(InetSocketAddress(oAuth2ServerConfig.proxySettings.host, oAuth2ServerConfig.proxySettings.port)))
        }
        this.httpClient = builder.build()
    }

    val userContext: Response<UserContext>
        @Throws(UserContextBuildingException::class)
        get() {
            val token = getCurrentUserAccessToken()
            return verifyAccessToken(token.token)
        }

    val isAuthenticated: Boolean
        get() {
            return try {
                userContext.data.userName //you can get the user context ==> user has a valid JWT token in the header
                true
            } catch (exception: Exception) {
                false
            }
        }

    private fun verifyAccessToken(token: String): Response<UserContext> {
        return try {
            val request = HttpRequest.newBuilder()
                .POST(
                    ViaeBodyPublishers.ofFormData(
                        "grant_type" to "client_credentials",
                        "client_id" to oAuth2ServerConfig.clientId,
                        "client_secret" to oAuth2ServerConfig.clientSecret,
                        "token" to token
                    )
                )
                .header("Content-Type", applicationWwwFormUrlEncoded)
                .uri(URI.create("${oAuth2ServerConfig.serverUrl}/realms/${oAuth2ServerConfig.realm}/protocol/openid-connect/token/introspect"))
                .build()
            val response = httpClient.send(request, BodyHandlers.ofString())
            if (response.statusCode() != StatusCode.ok) {
                throw UserContextBuildingException("could not validate token => ${response.statusCode()}: ${response.body()}")
            }
            val json = Json.parseJson(response.body()).jsonObject
            if (!json.getPrimitive("active").boolean) {
                throw UserContextBuildingException("token is expired/not active")
            }
            val applicationPermissions: Map<ApplicationName, Set<Oauth2Permission>> =
                json["resource_access"]?.jsonObject?.content
                    ?.map { it.key to it.value.jsonObject.getArray("roles").map { it.primitive.content }.toSet() }
                    ?.toMap() ?: emptyMap()
            val firstName = json.getPrimitiveOrNull("given_name")?.content
            val lastName = json.getPrimitiveOrNull("family_name")?.content
            success(UserContext(
                firstName = firstName,
                lastName = lastName,
                attributes = if(json.containsKey("attributes")){
                    json.getObject("attributes").content.map { it.key to it.value.primitive.toString() }.toMap()
                } else emptyMap(),
                email = json.getPrimitiveOrNull("email")?.content,
                userName = json.getPrimitive("username").content,
                globalPermissions = json.getObject("realm_access").getArray("roles").map { it.primitive.content }.toSet(),
                applicationPermissions = applicationPermissions
            ))
        } catch (exception: Exception) {
            failure(UserContextHolder::class, "could not build user context: ${exception.localizedMessage}")
        }
    }

}

class UserContextBuildingException(errorMessage: String, cause: Exception? = null) : Exception(errorMessage, cause)
