package solutions.viae.oauth2validation.domain

import solutions.viae.coreutils.domain.ApplicationName

data class UserContext(
    val userName: String,
    val email: String?,
    val firstName: String?,
    val lastName: String?,
    val globalPermissions: Set<Oauth2Permission>,
    val applicationPermissions: Map<ApplicationName, Set<Oauth2Permission>>,
    val attributes: Map<String, Any>
){
    fun hasAllThePermissions(vararg permissions: Oauth2Permission): Boolean {
        return permissions.isEmpty() || permissions.all { this.globalPermissions.contains(it) }
    }

    fun hasNoneOfThePermissions(vararg permissions: Oauth2Permission): Boolean {
        return permissions.isEmpty() || permissions.none { this.globalPermissions.contains(it) }
    }

    fun hasOneOfThePermissions(vararg permissions: Oauth2Permission): Boolean {
        return permissions.isEmpty() || permissions.any { this.globalPermissions.contains(it) }
    }
    fun hasAllTheApplicationPermissions(application: ApplicationName, vararg permissions: Oauth2Permission): Boolean {
        val tempPermissions = applicationPermissions[application] ?: emptySet()
        return permissions.isEmpty() || permissions.all { tempPermissions.contains(it) }
    }

    fun hasNoneOfTheApplicationPermissions(application: ApplicationName, vararg permissions: Oauth2Permission): Boolean {
        val tempPermissions = applicationPermissions[application] ?: emptySet()
        return permissions.isEmpty() || permissions.none { tempPermissions.contains(it) }
    }

    fun hasOneOfTheApplicationPermissions(application: ApplicationName, vararg permissions: Oauth2Permission): Boolean {
        val tempPermissions = applicationPermissions[application] ?: emptySet()
        return permissions.isEmpty() || permissions.any { tempPermissions.contains(it) }
    }
}
