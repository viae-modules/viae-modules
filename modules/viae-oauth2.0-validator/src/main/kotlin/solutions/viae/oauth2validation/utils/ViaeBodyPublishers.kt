package solutions.viae.oauth2validation.utils

import java.net.URLEncoder
import java.net.http.HttpRequest
import java.nio.charset.StandardCharsets


object ViaeBodyPublishers {
    fun ofFormData(vararg elements: Pair<Any, Any>): HttpRequest.BodyPublisher {
        return ofFormData(elements.toMap())
    }

    fun ofFormData(data: Map<Any, Any>): HttpRequest.BodyPublisher {
        val builder = StringBuilder()
        for ((key, value) in data) {
            if (builder.isNotEmpty()) {
                builder.append("&")
            }
            builder.append(URLEncoder.encode(key.toString(), StandardCharsets.UTF_8))
            builder.append("=")
            builder.append(URLEncoder.encode(value.toString(), StandardCharsets.UTF_8))
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString())
    }
}
