package solutions.viae.oauth2validation.domain

data class UserAccessToken(val token: String)
