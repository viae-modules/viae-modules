package solutions.viae.oauth2validation.domain

object MediaType {
    const val applicationWwwFormUrlEncoded = "application/x-www-form-urlencoded"
}
