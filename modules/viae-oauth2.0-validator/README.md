# VIAE OAth2.0 Validator

Kotlin and interface based connection to keycloak (OAuth 2.0 server: [official website](https://www.keycloak.org/)) 
without being forced to use Micronaut, SpringBoot, ... .

## Example
Micronaut example can be found in [OAuthValidationDemoTest](./src/test/kotlin/solutions/viae/oauth2validation/demo/OAuthValidationDemoTest.kt).
Required controller - use case configuration can be found in the folder "test / demo / components".

**Important:** 

make sure that docker-compose is running: This will start up an in-memory keycloak 
instance. Command (from within the viae-oauth2.0-validator root folder): `docker-compose up`.
A keycloak instance will be runnen on port 8081. You can browse to http://localhost:8081/auth 
to connect to the admin screen. User name - password: admin - admin (please make this more secure 
when using this in a production environment).

**Important regarding keycloak:**

The token should be generated against the same DNS name of the keycloak instance as it is defined in the 
oauth 2.0 validator config. E.g. validating a bearer token generated against http://keycloak:81/auth for 
the given server url (i.e. within viae-oauth2.0-validator) https://keycloak:8443/auth will always result in an 
inactive token (i.e. token expired, token not active).

**Usage: bean configuration**
```kotlin
    @Singleton
    fun userContextHolder(): UserContextHolder {
        return UserContextHolder(OAuth2ServerConfig(
            "http://localhost:8081/auth",
            "test"
        )) {
            //... extract JWT token from header (i.e. authorization header without bearer prefix)
        }
    }
```

**Usage: usecase**
```kotlin
class UseCaseWithSecurityForAllPermissions(
    private val userContextHolder: UserContextHolder
) {

    fun execute(request: String): String {
        if (userContextHolder.userContext.hasAllThePermissions("READ", "READ_SPECIAL")) {
            return "this is the secured content for $request"
        } else {
            throw IllegalStateException("not enough permissions")
        }
    }
}
```
