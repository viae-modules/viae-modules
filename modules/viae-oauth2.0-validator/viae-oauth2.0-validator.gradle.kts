dependencies {
    api(project(":modules:viae-core-utils"))
    api(Libraries.Kotlinx.serializationRuntime)

    testImplementation(Libraries.Keycloak.admin)
    testImplementation(Libraries.Logging.slf4jSimple)
}
