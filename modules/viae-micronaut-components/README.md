# VIAE core utils
Kotlin classes that are often used in the core layer of the VIAE projects. 
Although we strive for having no dependencies at all in our core layer (i.e. clean architecture), 
we make a single exception for this package. 

## Highlights
### Functional validation
Examples: [ValidatorTest](src/test/kotlin/solutions/viae/coreutils/utils/ValidatorTest.kt)
```kotlin
    val result = runValid {
        with(it) {
            isNotBlank("non-blank value", ErrorMessage.error(ValidatorTest::class, "should not be blank", "input-validation"))

            and("input-validation") {
                isTrue(false, ErrorMessage.permissionError(ValidatorTest::class, "should be true", "data-validation"))
            }

            respond {
                "success"
            }
        }
    }
```

### Date utils
Utility class arround ZonedDateTime.

Examples: [DateUtilsTest](src/test/kotlin/solutions/viae/coreutils/utils/DateUtilsTest.kt)

Configuration: needs a "viae-configuration.properties" file on the root of your classpath.
Required properties are:
* pattern_ts_formatter
* pattern_ts_without_nano_dots
* pattern_date_formatter
* default_zone_id

Example: [viae-configuration.properties](src/test/resources/viae-configuration.properties)

### Geo utils

### Response class
