package solutions.viae.requestlog.demo

import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint
import solutions.viae.requestlog.demo.components.Application
import solutions.viae.requestlog.demo.components.api.DemoClient
import solutions.viae.requestlog.demo.components.api.RequestLogsClient
import javax.inject.Inject

@MicronautTest(
    application = Application::class
)
class RequestLogsDemoTest {

    @Inject
    private lateinit var demoClient: DemoClient

    @Inject
    private lateinit var requestLogsClient: RequestLogsClient

    @Test
    fun doTestRequestLogCreation() {
        //Given
        val start = DateUtils.now()

        //When
        val initialLogsCount = requestLogsClient.getRequestLogs(DateUtils.toString(start), null).body()!!.size

        //Given
        demoClient.getHelloWorld(null, "pieter", "vandeperre")
        demoClient.getForAReasonMappedHelloWorld("maarten", "vandeperre")
        requestLogsClient.doCreate(
            null,
            RequestLogEntrypoint.DoCreateBody(
                message = "this user action in my javascript code was executed",
                tookInMilliSeconds = 250
            )
        )
        val userId = "the user id from the logged in user - ${DateUtils.toString(start)}"
        requestLogsClient.doCreate(
            userId,
            RequestLogEntrypoint.DoCreateBody(
                message = "this second user action in my javascript code was executed",
                tookInMilliSeconds = 2500,
                payloadSize = "an array of 1000 items"
            )
        )
        demoClient.getHelloWorld(userId, "user", "for test")

        //When
        val newLogs = requestLogsClient.getRequestLogs(DateUtils.toString(start), null).body()!!

        //Then
        assertThat(newLogs).isNotEmpty()
        assertThat(newLogs.map { it.payloadSize }).containsAnyElementsOf(
            listOf(
                "#firstName characters: 6; #lastName characters: 10; #resulting characters: 23",
                "#firstName characters: 7; #lastName characters: 10; #resulting parts: 24",
                "not-set",
                "this user action in my javascript code was executed",
                "this second user action in my javascript code was executed"
            )
        )
        assertThat(newLogs.size).isGreaterThanOrEqualTo(initialLogsCount + 4)

        //When
        val userLogs = requestLogsClient.getRequestLogs(DateUtils.toString(start), userId).body()!!

        //Then
        assertThat(userLogs).isNotEmpty()
        assertThat(userLogs.map { it.freeMessage }).isEqualTo(listOf("this second user action in my javascript code was executed", null))
        assertThat(userLogs.map { it.payloadSize }).isEqualTo(
            listOf(
                "an array of 1000 items",
                "#firstName characters: 4; #lastName characters: 8; #resulting characters: 19"
            )
        )

        //Given
        val logEntryWithLoggingRef = newLogs.filter { it.url == "/api/demo/v0.1/raw?firstName=pieter&lastName=vandeperre" }.first().ref

        //When
        val entry = requestLogsClient.getRequestLog(logEntryWithLoggingRef).body()!!

        //Then
        assertThat(entry.logLines.map { "${it.level.name};${it.message}" }).isEqualTo(
            listOf(
                "INFO;a first action happened",
                "WARN;a second action happened: demo action"
            )
        )
    }
}
