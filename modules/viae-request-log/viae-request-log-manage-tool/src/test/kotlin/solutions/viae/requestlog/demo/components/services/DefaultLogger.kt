package solutions.viae.requestlog.demo.components.services

import solutions.viae.coreutils.service.Logger
import java.lang.String.format
import java.util.stream.Collectors

internal class DefaultLogger() : Logger {
    override fun trace(message: String, vararg args: String) {
        println("trace: " + format(message, *args))
    }

    override fun debug(message: String, vararg args: String) {
        try {
            var msg = message
            if (args.isEmpty()) {
                msg = message.replace("%", "%%")
            }
            println("debug: " + format(msg, *args))
        } catch (e: Exception) {
            error(e, "")
        }

    }

    override fun info(message: String, vararg args: String) {
        var msg = message
        if (args.isEmpty()) {
            msg = message.replace("%", "%%")
        }
        println("info: " + format(msg, *args))
    }

    override fun warn(message: String, vararg args: String) {
        var msg = message
        if (args.isEmpty()) {
            msg = message.replace("%", "%%")
        }
        println("warn: " + format(msg, *args))
    }

    override fun warn(cause: Throwable, message: String, vararg args: String) {
        var msg = message
        if (args.isEmpty()) {
            msg = message.replace("%", "%%")
        }
        println("warn: " + format(msg, *args))
    }

    override fun error(message: String, vararg args: String) {
        var msg = message
        if (args.isEmpty()) {
            msg = message.replace("%", "%%")
        }
        println("error: " + format(msg, *args))
    }

    override fun error(cause: Throwable, message: String, vararg args: String) {
        var msg = message
        if (args.isEmpty()) {
            msg = message.replace("%", "%%")
        }
        println("error: " + format(msg, *args))
        println(cause.localizedMessage)
        val stackTrace =
            listOf(cause.stackTrace).stream().map { st -> st.toString() }.collect(Collectors.joining("\t\t\t\n"))
        println(stackTrace)
    }

}
