package solutions.viae.requestlog.scenarios

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.micronaut.http.client.annotation.Client
import solutions.viae.requestlog.core.entrypoints.RequestLogResource
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint

@Client("/api/requestlogs/v0.1")
interface RequestLogControllerClient {

    @Post(consumes = [MediaType.APPLICATION_JSON], produces = [MediaType.APPLICATION_JSON])
    @Header(name = "command", value = "GENERATE_ID")
    fun generateNewRequestLogId(): HttpResponse<String>

    @Post(consumes = [MediaType.APPLICATION_JSON], produces = [MediaType.APPLICATION_JSON])
    @Header(name = "command", value = "CREATE_REQUEST_LOG")
    fun createNewRequestLog(
        @Body body: RequestLogEntrypoint.DoCreateBody,
        @Header(name = "User-Id ") userId: String,
        @Header(name = "Request-Id ") requestId: String,
        @Header(name = "Request-Group-Id ") requestGroupId: String? = null,
        @Header(name = "User-Agent ") userAgent: String = "User-Agent",
        @Header(name = "X-Forwarded-For ") xForwardedFor: String = "X-Forwarded-For"
    ): HttpResponse<String>

    @Get
    fun getRequestLogs(@QueryValue after: String): HttpResponse<List<RequestLogResource>>

    @Get
    fun getRequestLogsFor(@QueryValue after: String, @QueryValue userId: String): HttpResponse<List<RequestLogResource>>

}
