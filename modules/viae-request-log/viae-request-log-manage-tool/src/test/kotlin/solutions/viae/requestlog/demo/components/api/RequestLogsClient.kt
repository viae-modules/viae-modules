package solutions.viae.requestlog.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.annotation.Client
import solutions.viae.requestlog.core.entrypoints.RequestLogResource
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint
import javax.inject.Singleton

@Client(value = "/api/requestlogs/v0.1", configuration = RequestLogsClient.Config::class)
interface RequestLogsClient {

    @Get
    fun getRequestLogs(@QueryValue after: String?, @QueryValue userId: String?): HttpResponse<List<RequestLogResource>>

    @Get(value = "/{ref}")
    fun getRequestLog(@QueryValue ref: String?): HttpResponse<RequestLogResource>

    @Post(produces = [MediaType.APPLICATION_JSON], consumes = [MediaType.APPLICATION_JSON])
    fun doCreate(@Header("User-Id") userId: String?, @Body body: RequestLogEntrypoint.DoCreateBody?): HttpResponse<*>

    @Singleton
    open class Config : HttpClientConfiguration() {
        init {
            isExceptionOnErrorStatus = false
        }

        override fun getConnectionPoolConfiguration(): ConnectionPoolConfiguration {
            return ConnectionPoolConfiguration()
        }

    }

}
