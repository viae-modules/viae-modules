package solutions.viae.requestlog.demo.components.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.requestlog.core.services.RequestLogService
import solutions.viae.requestlog.demo.components.config.present
import solutions.viae.requestlog.demo.components.config.toViaeHttpRequest
import javax.inject.Inject

@Controller(
    value = "/api/demo/v0.1",
    consumes = [MediaType.APPLICATION_JSON],
    produces = [MediaType.APPLICATION_JSON]
)
class DemoController @Inject constructor(
    private val requestLogService: RequestLogService,
    private val environment: Environment,
    private val logger: Logger
) {

    @Get(value="/raw")
    fun getHelloWorld(@QueryValue firstName: String?, @QueryValue lastName: String?, httpRequest: HttpRequest<*>): HttpResponse<String> {
        return requestLogService.logExecutionMetaData(
            httpRequest.toViaeHttpRequest(environment),
            { logger ->
                logger.info("a first action happened")
                logger.warn("a second action happened: %s", "demo action")
                Response.success("hello $firstName $lastName")
            },
            { data -> "#firstName characters: ${firstName?.length ?: 0}; #lastName characters: ${lastName?.length ?: 0}; #resulting characters: ${data.length}" }
        ).present(logger)
    }

    @Get(value="/mapped")
    fun getForAReasonMappedHelloWorld(@QueryValue firstName: String?, @QueryValue lastName: String?, httpRequest: HttpRequest<*>): HttpResponse<List<String>> {
        return requestLogService.logExecutionMetaData(
            httpRequest.toViaeHttpRequest(environment),
            { Response.success("hello $firstName $lastName") },
            { data -> data.split(" ") },
            { data -> "#firstName characters: ${firstName?.length ?: 0}; #lastName characters: ${lastName?.length ?: 0}; #resulting parts: ${data.length}" }
        ).present(logger)
    }
}
