package solutions.viae.requestlog.manage.tool

import io.micronaut.runtime.Micronaut
import solutions.viae.requestlog.manage.tool.api.RequestLogsController
import solutions.viae.requestlog.manage.tool.config.Configuration

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages(
                RequestLogsController::class.java.packageName,
                Configuration::class.java.packageName
            )
            .mainClass(Application.javaClass)
            .start()
    }
}
