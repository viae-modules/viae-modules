package solutions.viae.requestlog.manage.tool.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.service.Logger
import solutions.viae.requestlog.manage.tool.config.present
import solutions.viae.requestlog.manage.tool.config.toViaeHttpRequest
import solutions.viae.requestlog.core.entrypoints.RequestLogResource
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint
import javax.inject.Inject

@Controller(
    value = "/api/requestlogs/v0.1",
    consumes = [MediaType.APPLICATION_JSON],
    produces = [MediaType.APPLICATION_JSON]
)
class RequestLogsController @Inject constructor(
    private val environment: Environment,
    private val requestLogEntrypoint: RequestLogEntrypoint,
    private val logger: Logger
) {
    @Post
    fun doCreate(@Body body: RequestLogEntrypoint.DoCreateBody?, httpRequest: HttpRequest<*>): HttpResponse<*> {
        return requestLogEntrypoint.doCreate(body, httpRequest.toViaeHttpRequest(environment)).present(logger)
    }

    @Get
    fun getRequestLogs(@QueryValue after: String?, @QueryValue userId: String?): HttpResponse<List<RequestLogResource>> {
        return requestLogEntrypoint.getRequestLogs(after, userId).present(logger)
    }

    @Get(value = "/{ref}")
    fun getRequestLog(@QueryValue ref: String?): HttpResponse<RequestLogResource> {
        return requestLogEntrypoint.getRequestLog(ref).present(logger)
    }
}
