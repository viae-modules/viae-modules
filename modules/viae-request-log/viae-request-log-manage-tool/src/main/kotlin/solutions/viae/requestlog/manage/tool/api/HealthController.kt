package solutions.viae.requestlog.manage.tool.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/api/health")
class HealthController {
    @Get(consumes = [MediaType.APPLICATION_JSON], produces = [MediaType.APPLICATION_JSON])
    fun isUp(): HttpResponse<String> {
        return HttpResponse.ok("up and running")
    }
}
