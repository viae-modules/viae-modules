package solutions.viae.requestlog.manage.tool.repositories
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment

import javax.inject.Singleton

@Singleton
class HelloDataFetcher : DataFetcher<String> {

    override fun get(env: DataFetchingEnvironment): String {
        var name = env.getArgument<String>("name")
        if (name == null || name.trim().isEmpty()) {
            name = "World"
        }
        return "Hello $name!"
    }
}
