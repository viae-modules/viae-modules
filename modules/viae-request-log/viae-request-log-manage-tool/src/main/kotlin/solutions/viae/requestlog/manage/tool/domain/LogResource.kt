package solutions.viae.requestlog.manage.tool.domain

import solutions.viae.requestlog.core.domain.RequestLogLogLine
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel
import java.time.ZonedDateTime
import java.util.*

/**
 * A log entry.
 * This class contains a field with log lines as well:
 * The root log entry can e.g. be a REST endpoint call.
 * In this case there will be one entry to cover the REST call and
 * it will have log lines for logging what happened within the processing
 * of the REST request
 */
data class LogResource(
    val id: String,
    val applicationName: String,
    val groupId: String?,
    val url: String,
    val path: String,
    val freeMessage: String,
    val method: String,
    val xForwardedFor: String,
    val tookInMilliSeconds: Long,
    val remoteAddress: String,
    val userAgent: String,
    val userId: String,
    val userName: String,
    val created: String,
    val responseCode: String,
    val errorMessage: String,
    val isFreeMessageLog: Boolean,
    val payloadSize: String,
    val environment: String,
    val logLines: List<RequestLogLogLineResource>,
    val ref: String
)

/**
 * Individual log lines
 */
data class RequestLogLogLineResource(
    val level: RequestLogLogLineLevel,
    val ts: ZonedDateTime,
    val message: String
)
