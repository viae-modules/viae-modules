dependencies {
    implementation(project(":modules:viae-core-utils"))
    implementation(project(":modules:viae-request-log:viae-request-log-core"))
    implementation(project(":modules:viae-request-log:viae-request-log-mongodb"))

    kapt(Libraries.Micronaut.injectJava)

    implementation(Libraries.Kotlinx.serializationRuntime)
    implementation(Libraries.Database.mongodbDriverCore)
    implementation(Libraries.Database.mongodbDriverSync)

    implementation(Libraries.Logging.slf4jSimple)
    implementation(Libraries.Micronaut.core)
    implementation(Libraries.Micronaut.runtime)
    implementation(Libraries.Micronaut.httpClient)
    implementation(Libraries.Micronaut.httpServerNetty)
    implementation(Libraries.Micronaut.graphQl)
}
