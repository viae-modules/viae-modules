package solutions.viae.requestlog.mongodb.config

import com.mongodb.client.MongoCollection
import org.bson.Document
import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.Environment
import solutions.viae.requestlog.core.factories.CrudRequestLogRepositoryFactory
import solutions.viae.requestlog.core.services.DefaultMongoDbRequestLogService
import solutions.viae.requestlog.core.services.RequestLogService
import solutions.viae.requestlog.mongodb.factories.MongoDbRequestLogRepositoryFactory

class MongoDbRequestLogConfig(
    environment: Environment,
    applicationName: ApplicationName,
    mongoCollectionSupplier: () -> MongoCollection<Document>
) {
    private val requestLogRepositoryFactory: CrudRequestLogRepositoryFactory = MongoDbRequestLogRepositoryFactory(environment, mongoCollectionSupplier)
    val requestLogService: RequestLogService = DefaultMongoDbRequestLogService(requestLogRepositoryFactory, applicationName)

    companion object {
        fun init(
            environment: Environment,
            applicationName: ApplicationName,
            mongoCollectionSupplier: () -> MongoCollection<Document>
        ): MongoDbRequestLogConfig {
            return MongoDbRequestLogConfig(
                environment = environment,
                applicationName = applicationName,
                mongoCollectionSupplier = mongoCollectionSupplier
            )
        }
    }
}
