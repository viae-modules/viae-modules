package solutions.viae.requestlog.mongodb.factories

import com.mongodb.client.MongoCollection
import org.bson.Document
import solutions.viae.requestlog.mongodb.repositories.MongoDbRequestLogsRepository
import solutions.viae.coreutils.domain.Environment
import solutions.viae.requestlog.core.factories.CrudRequestLogRepositoryFactory
import solutions.viae.requestlog.core.repositories.RequestLogsRepository
import solutions.viae.requestlog.mongodb.repositories.InMemoryRequestLogsRepository

internal class MongoDbRequestLogRepositoryFactory(
    private val environment: Environment,
    private val mongoCollectionSupplier: () -> MongoCollection<Document>
) : CrudRequestLogRepositoryFactory {
    override val requestLogsRepository: RequestLogsRepository

    init {
        if (Environment.IN_MEMORY == environment) {
            requestLogsRepository = InMemoryRequestLogsRepository()
        } else {
            requestLogsRepository = MongoDbRequestLogsRepository(mongoCollectionSupplier)
        }
    }

}
