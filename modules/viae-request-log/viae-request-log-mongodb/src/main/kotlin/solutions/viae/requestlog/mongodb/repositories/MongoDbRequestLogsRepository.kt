package solutions.viae.requestlog.mongodb.repositories

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.and
import com.mongodb.client.model.Filters.gte
import com.mongodb.client.model.Filters.eq
import org.bson.Document
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.utils.DateUtils.Companion.toDefaultZone
import solutions.viae.coreutils.utils.runValid
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.domain.RequestLogLogLine
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel
import solutions.viae.requestlog.core.domain.RequestLogRef
import solutions.viae.requestlog.core.domain.UserId
import solutions.viae.requestlog.core.repositories.RequestLogsRepository
import java.time.ZonedDateTime
import java.util.*

internal class MongoDbRequestLogsRepository(
    private val mongoCollectionSupplier: () -> MongoCollection<Document>
) : RequestLogsRepository {

    override fun saveRequestLog(requestLog: RequestLog): Response<Long> {
        return runValid {
            with(it) {
                val document = tryTransform(
                    { toDocument(requestLog).data }, error("could not transform to mongodb documents", "input-validation")
                )
                and("input-validation") {
                    tryTransform(
                        { mongoCollectionSupplier().insertOne(document!!) },
                        error("could not insert request logs on database ${mongoCollectionSupplier().namespace.fullName}", "do-action")
                    )
                }
                respond { 1L }
            }
        }
    }

    override fun getRequestLog(ref: RequestLogRef): Response<RequestLog> {
        return runValid {
            with(it) {
                val document = tryTransform(
                    { getRequestLog(mongoCollectionSupplier(), ref) },
                    error("could not find request logs from database based upon ref $ref", "input-validation")
                )
                val result = and("input-validation") {
                    tryTransform(
                        { mapToRequestLog(document!!, true) },
                        error("could not transform request log document to domain object for search based upon ref", "do-action")
                    )
                }
                respond { result!! }
            }
        }
    }

    override fun getRequestLogs(after: ZonedDateTime): Response<List<RequestLog>> {
        return runValid {
            with(it) {
                val documents = tryTransform(
                    { getRequestLogs(mongoCollectionSupplier(), after, null) },
                    error("could not read/filter request logs from database based upon timestamp", "transform")
                )
                val result = and("input-validation", "transform") {
                    tryTransform(
                        { mapToRequestLogs(documents!!) },
                        error("could not transform request log documents to domain objects for search based upon timestamp", "do-action")
                    )
                }
                respond { result!! }
            }
        }
    }

    override fun getRequestLogsFor(after: ZonedDateTime, userId: UserId): Response<List<RequestLog>> {
        return runValid {
            with(it) {
                val documents = tryTransform(
                    { getRequestLogs(mongoCollectionSupplier(), after, userId) },
                    error("could not read/filter request logs from database based upon timestamp and user", "transform")
                )
                val result = and("input-validation", "transform") {
                    tryTransform(
                        { mapToRequestLogs(documents!!) },
                        error("could not transform request log documents to domain objects for search based upon timestamp and user", "do-action")
                    )
                }
                respond { result!! }
            }
        }
    }

    companion object {
        private fun toDocument(requestLog: RequestLog): Response<Document> {
            return Response.success(
                Document()
                    .append("id", requestLog.id)
                    .append("ref", requestLog.ref.toString())
                    .append("applicationName", requestLog.applicationName)
                    .append("groupId", requestLog.groupId)
                    .append("url", requestLog.url)
                    .append("path", requestLog.path)
                    .append("method", requestLog.method)
                    .append("remoteAddress", requestLog.remoteAddress)
                    .append("tookInMilliSeconds", requestLog.tookInMilliSeconds)
                    .append("userAgent", requestLog.userAgent)
                    .append("user", requestLog.user)
                    .append("xForwardedFor", requestLog.xForwardedFor)
                    .append("created", DateUtils.toString(requestLog.created))
                    .append("errorMessage", requestLog.errorMessage)
                    .append("responseCode", requestLog.responseCode)
                    .append("freeMessage", requestLog.freeMessage)
                    .append("isFreeMessageLog", requestLog.isFreeMessageLog)
                    .append("payloadSize", requestLog.payloadSize)
                    .append("environment", requestLog.environment)
                    .append("logs", requestLog.logLines.map { log ->
                        Document()
                            .append("level", log.level.name)
                            .append("ts", DateUtils.toString(toDefaultZone(log.ts)))
                            .append("message", log.message)
                    })
            )
        }

        private fun mapToRequestLogs(documents: List<Document>): List<RequestLog> {
            return documents.map { d ->
                mapToRequestLog(d, false)
            }
        }

        private fun mapToRequestLog(d: Document, includeLogLines: Boolean) = RequestLog(
            ref = UUID.fromString(d.getString("ref")),
            id = d.getString("id"),
            applicationName = d.getString("applicationName"),
            groupId = d.getString("groupId"),
            url = d.getString("url"),
            path = d.getString("path"),
            method = d.getString("method"),
            remoteAddress = d.getString("remoteAddress"),
            tookInMilliSeconds = d.getLong("tookInMilliSeconds"),
            userAgent = d.getString("userAgent"),
            user = d.getString("user"),
            xForwardedFor = d.getString("xForwardedFor"),
            created = DateUtils.toTimeStamp(d.getString("created")),
            errorMessage = d.getString("errorMessage"),
            responseCode = d.getString("responseCode"),
            freeMessage = d.getString("freeMessage"),
            isFreeMessageLog = d.getBoolean("isFreeMessageLog"),
            payloadSize = d.getString("payloadSize"),
            environment = d.getString("environment"),
            logLines = if(includeLogLines){
                d.getList("logs", Document::class.java).map { log ->
                    RequestLogLogLine(
                        level = RequestLogLogLineLevel.valueOf(log.getString("level")),
                        ts = DateUtils.toTimeStamp(log.getString("ts")),
                        message = log.getString("message")
                    )
                }
            } else {
                emptyList()
            }
        )

        private fun getRequestLogs(mongoCollection: MongoCollection<Document>, after: ZonedDateTime, userId: UserId?): List<Document> {
            val filter = if (userId == null) {
                gte("created", DateUtils.toString(after))
            } else {
                and(
                    gte("created", DateUtils.toString(after)),
                    eq("user", userId)
                )
            }
            return mongoCollection
                .find(filter)
                .toList()
        }

        private fun getRequestLog(mongoCollection: MongoCollection<Document>, ref: RequestLogRef): Document? {
            val filter = eq("ref", ref.toString())
            return mongoCollection
                .find(filter)
                .firstOrNull()
        }

        private fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(MongoDbRequestLogsRepository::class, message, errorGroup)
        }
    }

}
