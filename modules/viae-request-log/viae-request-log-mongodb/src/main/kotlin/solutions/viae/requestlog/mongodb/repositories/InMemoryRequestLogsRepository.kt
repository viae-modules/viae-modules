package solutions.viae.requestlog.mongodb.repositories

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.domain.Response.Companion.success
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.domain.RequestLogRef
import solutions.viae.requestlog.core.domain.UserId
import solutions.viae.requestlog.core.repositories.RequestLogsRepository
import java.time.ZonedDateTime

internal class InMemoryRequestLogsRepository : RequestLogsRepository {
    private val logs = mutableListOf<RequestLog>()

    override fun saveRequestLog(requestLog: RequestLog): Response<Long> {
        logs.add(requestLog)
        return success(1)
    }

    override fun getRequestLog(ref: RequestLogRef): Response<RequestLog> {println("WARN: in in memory database for request logs, database id filtering is not supported")
        return logs.find { it.ref == ref }
            ?.let { success(it) }
            ?: failure(ErrorMessage.error(InMemoryRequestLogsRepository::class, "log with id $ref not found"))
    }

    override fun getRequestLogs(after: ZonedDateTime): Response<List<RequestLog>> {
        return success(
            logs.filter { l -> l.created.isAfter(after) }
        )
    }

    override fun getRequestLogsFor(after: ZonedDateTime, userId: UserId): Response<List<RequestLog>> {
        return success(
            logs.filter { l -> l.created.isAfter(after) }.filter { l -> l.user == userId }
        )
    }
}
