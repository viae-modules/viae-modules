package solutions.viae.requestlog.scenarios

import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Test

@MicronautTest(
    environments = ["test"],
    packages = ["com.viae.propertyanalysis.configuration.config"]
)
class RequestLogsMongoDbIT : RequestLogsParent() {

    @Test
    override fun testGettingNewRequestId() {
        super.testGettingNewRequestId()
    }

    @Test
    override fun testCreatingAndGettingACustomLog() {
        super.testCreatingAndGettingACustomLog()
    }

    @Test
    override fun testCreatingAndGettingACustomLogBasedUponUser() {
        super.testCreatingAndGettingACustomLogBasedUponUser()
    }
}
