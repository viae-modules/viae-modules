package solutions.viae.requestlog.demo.components.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.annotation.Client
import javax.inject.Singleton

@Client(value = "/api/demo/v0.1", configuration = DemoClient.Config::class)
interface DemoClient {

    @Get(value="/raw")
    fun getHelloWorld(@Header("User-Id") userId: String?, @QueryValue firstName: String?, @QueryValue lastName: String?): HttpResponse<String>

    @Get(value="/mapped")
    fun getForAReasonMappedHelloWorld(@QueryValue firstName: String?, @QueryValue lastName: String?): HttpResponse<List<String>>

    @Singleton
    open class Config : HttpClientConfiguration() {
        init {
            isExceptionOnErrorStatus = false
        }

        override fun getConnectionPoolConfiguration(): ConnectionPoolConfiguration {
            return ConnectionPoolConfiguration()
        }

    }
}

