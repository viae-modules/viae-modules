package solutions.viae.requestlog.scenarios

import org.assertj.core.api.Assertions.assertThat
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint
import solutions.viae.requestlog.core.services.RequestLogService
import java.time.ZonedDateTime.now
import java.util.*
import javax.inject.Inject

abstract class RequestLogsParent {
    @Inject
    private lateinit var requestLogControllerClient: RequestLogControllerClient
    @Inject
    private lateinit var requestLogService: RequestLogService

    open fun testGettingNewRequestId() {
        //When
        val response = requestLogControllerClient.generateNewRequestLogId().body()!!

        //Then
        assertThat(UUID.fromString(response)).isNotNull()
    }

    open fun testCreatingAndGettingACustomLog() {
        //Given
        val log1 = RequestLogEntrypoint.DoCreateBody("custom log message 1")
        val log2 = RequestLogEntrypoint.DoCreateBody("custom log message 2", 150)
        //And
        val id1 = UUID.randomUUID().toString()
        val id2 = UUID.randomUUID().toString()
        println("id1: $id1")
        println("id2: $id2")
        requestLogControllerClient.createNewRequestLog(log1, "user-1", id1)
        requestLogControllerClient.createNewRequestLog(log2, "user-1", id2)

        //When
        val result =
            requestLogControllerClient.getRequestLogs(DateUtils.toString(now().minusSeconds(5))).body()!!

        //Then
        assertThat(result.map { rl -> rl.id }).contains(id1)
        assertThat(result.map { rl -> rl.id }).contains(id2)
    }

    open fun testCreatingAndGettingACustomLogBasedUponUser() {
        //Given
        val log1 = RequestLogEntrypoint.DoCreateBody("custom log message 1")
        val log2 = RequestLogEntrypoint.DoCreateBody("custom log message 2", 150)
        val log3 = RequestLogEntrypoint.DoCreateBody("custom log message 3", 150)
        //And
        val id1 = UUID.randomUUID().toString()
        val id2 = UUID.randomUUID().toString()
        val id3 = UUID.randomUUID().toString()
        println("id1: $id1")
        println("id2: $id2")
        println("id3: $id3")
        requestLogControllerClient.createNewRequestLog(log1, "user-1", id1)
        requestLogControllerClient.createNewRequestLog(log2, "user-2", id2)
        requestLogControllerClient.createNewRequestLog(log3, "user-1", id3)

        //When
        val result =
            requestLogControllerClient.getRequestLogsFor(DateUtils.toString(now().minusSeconds(5)), "user-1").body()!!

        //Then
        assertThat(result.map { rl -> rl.id }).contains(id1)
        assertThat(result.map { rl -> rl.id }).doesNotContain(id2)
        assertThat(result.map { rl -> rl.id }).contains(id3)
    }
}
