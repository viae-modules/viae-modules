package solutions.viae.requestlog.demo.components

import io.micronaut.runtime.Micronaut
import solutions.viae.requestlog.demo.components.api.RequestLogsController

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages(RequestLogsController::class.java.packageName)
            .mainClass(Application.javaClass)
            .start()
    }
}
