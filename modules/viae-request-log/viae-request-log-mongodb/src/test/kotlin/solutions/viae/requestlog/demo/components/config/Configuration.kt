package solutions.viae.requestlog.demo.components.config

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import io.micronaut.context.annotation.Factory
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.service.BadRequestResponseType
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.service.OkResponseType
import solutions.viae.coreutils.service.ResponseType
import solutions.viae.coreutils.service.UnAuthorizedResponseType
import solutions.viae.coreutils.service.ViaeHttpRequest
import solutions.viae.coreutils.service.ViaeHttpResponse
import solutions.viae.requestlog.core.entrypoints.RequestLogEntrypoint
import solutions.viae.requestlog.core.services.RequestLogService
import solutions.viae.requestlog.demo.components.services.DefaultLogger
import solutions.viae.requestlog.mongodb.config.MongoDbRequestLogConfig
import java.net.URI
import javax.inject.Singleton

fun <RDT> ViaeHttpResponse<RDT>.present(logger: Logger): HttpResponse<RDT> {
    return when (type) {
        ResponseType.UNAUTHORIZED -> {
            val response = this as UnAuthorizedResponseType<RDT>
            HttpResponse.unauthorized<RDT>().header(response.errorMessageHeader.first, response.errorMessageHeader.second)
        }
        ResponseType.BAD_REQUEST -> {
            val response = this as BadRequestResponseType<RDT>
            logger.error("Bad Request: ${response.errors.joinToString(", ")}")
            val result = HttpResponse.badRequest<RDT>()
            result.header("error-message", response.errors.joinToString(";") { "${it.source}-${it.message}" })
            result
        }
        ResponseType.OK -> {
            if (this is OkResponseType) {
                HttpResponse.ok(this.data)
            } else {
                HttpResponse.noContent()
            }
        }
        ResponseType.OK_NO_CONTENT -> {
            HttpResponse.noContent()
        }
    }
}

fun HttpRequest<*>.toViaeHttpRequest(environment: Environment): ViaeHttpRequest {
    val methodName = method.name
    return object : ViaeHttpRequest {
        override val environment
            get() = environment
        override val headers: Map<String, String>
            get() = getHeaders().asMap(String::class.java, String::class.java)
        override val hostName: String
            get() = remoteAddress.hostName
        override val port: Int
            get() = remoteAddress.port
        override val method: String
            get() = methodName
        override val uri: URI
            get() = getUri()
    }
}

@Factory
class Configuration(
) {
    private val environment = Environment.LOCAL
    private val applicationName = "Demo Request Log Application"
    private val requestLogServiceConfig = MongoDbRequestLogConfig.init(
        environment = environment,
        mongoCollectionSupplier = { mongoClient().getDatabase("demo-requestlog").getCollection("requestlogs") },
        applicationName = applicationName
    )

    @Singleton
    fun requestLogService(): RequestLogService {
        return requestLogServiceConfig.requestLogService
    }

    @Singleton
    fun requestLogEntrypoint(): RequestLogEntrypoint {
        return RequestLogEntrypoint(requestLogService = requestLogService(), applicationName = applicationName)
    }

    @Singleton
    fun mongoClient(): MongoClient {
        return MongoClients.create(System.getenv("MONGODB_HOST") ?: "mongodb://localhost:27017")
    }

    @Singleton
    fun environment(): Environment {
        return environment
    }

    @Singleton
    fun logger(): Logger {
        return DefaultLogger()
    }
}
