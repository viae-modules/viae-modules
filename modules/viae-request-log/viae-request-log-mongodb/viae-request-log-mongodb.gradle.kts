dependencies {
    api(project(":modules:viae-core-utils"))
    api(project(":modules:viae-request-log:viae-request-log-core"))
    api(Libraries.Kotlinx.serializationRuntime)
    api(Libraries.Database.mongodbDriverCore)
    api(Libraries.Database.mongodbDriverSync)
}
