package solutions.viae.requestlog.core.entrypoints

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.service.BadRequestResponseType
import solutions.viae.coreutils.service.OkNoContentResponseType
import solutions.viae.coreutils.service.OkResponseType
import solutions.viae.coreutils.service.ViaeHttpRequest
import solutions.viae.coreutils.service.ViaeHttpResponse
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.utils.runValid
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.services.RequestLogService
import java.time.ZonedDateTime
import java.util.*

open class RequestLogEntrypoint(
    private val requestLogService: RequestLogService,
    private val applicationName: ApplicationName
) {
    fun doCreate(body: DoCreateBody?, httpRequest: ViaeHttpRequest): ViaeHttpResponse<*> {
        return try {
            when (httpRequest.headers.get("command")?.toUpperCase()) {
                Commands.GENERATE_ID.name -> OkResponseType(UUID.randomUUID().toString())
                Commands.CREATE_REQUEST_LOG.name -> this.createRequestLog(body, httpRequest)
                else -> this.createRequestLog(body, httpRequest)
            }
        } catch (e: Exception) {
            BadRequestResponseType<Unit>(listOf(ErrorMessage(RequestLogEntrypoint::class, "could not create request log: ${e.localizedMessage}")))
        }
    }

    private fun createRequestLog(body: DoCreateBody?, httpRequest: ViaeHttpRequest): ViaeHttpResponse<*> {
        checkNotNull(body, { "CreateRequestLogBody should not be null" })
        requestLogService.saveRequestLog(
            RequestLog.from(httpRequest, applicationName, body.tookInMilliSeconds, body.payloadSize ?: "not-set", body.message)
        )
        return OkNoContentResponseType()
    }

    fun getRequestLogs(after: String?, userId: String?): ViaeHttpResponse<List<RequestLogResource>> {
        val response = runValid {
            with(it) {
                val ts =
                    tryTransform({ DateUtils.toTimeStamp(after!!) }, error("after '${after}' is not a timestamp", "input-validation"))
                val logs = and("input-validation") {
                    tryTransform({ getRequestLogs(ts!!, userId).data }, error("could not get logs", "transform"))
                }
                val result = and("input-validation", "transform") {
                    tryTransform({ logs!!.map { RequestLogResource.from(it) } }, error("could not map request logs", "do-action"))
                }
                respond { result }
            }
        }

        return if (response.hasNoErrors()) {
            OkResponseType(response.data!!)
        } else {
            BadRequestResponseType(response.errors)
        }
    }

    fun getRequestLog(ref: String?): ViaeHttpResponse<RequestLogResource> {
        val response = runValid {
            with(it) {
                val typedRef = tryTransform({ UUID.fromString(ref) }, error("ref '${ref}' is null or is not a UUID", "input-validation"))
                val log = and("input-validation") {
                    tryTransform({ requestLogService.getRequestLog(typedRef!!).data }, error("could not get log with ref $ref", "transform"))
                }
                val result = and("input-validation", "transform") {
                    tryTransform({ RequestLogResource.from(log!!) }, error("could not map request log", "do-action"))
                }
                respond { result }
            }
        }

        return if (response.hasNoErrors()) {
            OkResponseType(response.data!!)
        } else {
            BadRequestResponseType(response.errors)
        }
    }

    private fun getRequestLogs(after: ZonedDateTime, userId: String?): Response<List<RequestLog>> {
        return try {
            if (userId == null) {
                requestLogService.getRequestLogs(after)
            } else {
                requestLogService.getRequestLogsFor(after, userId)
            }
        } catch (e: Exception) {
            failure(RequestLogEntrypoint::class, "could not get the request logs: ${e.localizedMessage}")
        }
    }

    enum class Commands {
        GENERATE_ID, CREATE_REQUEST_LOG
    }

    data class DoCreateBody(
        val message: String,
        val tookInMilliSeconds: Long? = -1,
        val payloadSize: String? = "not-set"
    )

    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(RequestLogEntrypoint::class, message, errorGroup)
        }
    }
}
