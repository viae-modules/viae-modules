package solutions.viae.requestlog.core.services

import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.requestlog.core.domain.RequestLogLogLine
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel.DEBUG
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel.ERROR
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel.INFO
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel.TRACE
import solutions.viae.requestlog.core.domain.RequestLogLogLineLevel.WARN
import java.lang.String.format
import java.util.stream.Collectors

internal class RequestLogLogger : Logger {
    val logLines = mutableListOf<RequestLogLogLine>()

    override fun trace(message: String, vararg args: String) {
        logLines.add(RequestLogLogLine(TRACE, DateUtils.now(), format(message, *args)))
    }

    override fun debug(message: String, vararg args: String) {
        try {
            logLines.add(RequestLogLogLine(DEBUG, DateUtils.now(), format(message, *args)))
        } catch (e: Exception) {
            error(e, "")
        }

    }

    override fun info(message: String, vararg args: String) {
        logLines.add(RequestLogLogLine(INFO, DateUtils.now(), format(message, *args)))
    }

    override fun warn(message: String, vararg args: String) {
        logLines.add(RequestLogLogLine(WARN, DateUtils.now(), format(message, *args)))
    }

    override fun warn(cause: Throwable, message: String, vararg args: String) {
        var msg = message
        msg += "\ncaused by:\n${cause.localizedMessage}"
        val stackTrace =
            listOf(cause.stackTrace).stream().map { st -> st.toString() }.collect(Collectors.joining("\t\t\t\n"))
        msg += "\nstack trace:\n${stackTrace}"
        logLines.add(RequestLogLogLine(WARN, DateUtils.now(), format(message, *args)))
    }

    override fun error(message: String, vararg args: String) {
        logLines.add(RequestLogLogLine(ERROR, DateUtils.now(), format(message, *args)))
    }

    override fun error(cause: Throwable, message: String, vararg args: String) {
        var msg = message
        msg += "\ncaused by:\n${cause.localizedMessage}"
        val stackTrace =
            listOf(cause.stackTrace).stream().map { st -> st.toString() }.collect(Collectors.joining("\t\t\t\n"))
        msg += "\nstack trace:\n${stackTrace}"
        logLines.add(RequestLogLogLine(ERROR, DateUtils.now(), format(message, *args)))
    }

}
