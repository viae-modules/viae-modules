package solutions.viae.requestlog.core.factories

import solutions.viae.requestlog.core.repositories.RequestLogsRepository

interface CrudRequestLogRepositoryFactory {
    val requestLogsRepository: RequestLogsRepository
}
