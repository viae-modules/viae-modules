package solutions.viae.requestlog.core.domain

import java.time.ZonedDateTime

data class RequestLogLogLine(
    val level: RequestLogLogLineLevel,
    val ts: ZonedDateTime,
    val message: String
)

enum class RequestLogLogLineLevel {
    TRACE, DEBUG, INFO, WARN, ERROR
}
