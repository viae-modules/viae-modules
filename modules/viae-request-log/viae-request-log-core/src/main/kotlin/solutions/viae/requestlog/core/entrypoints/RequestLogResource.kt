package solutions.viae.requestlog.core.entrypoints

import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.domain.RequestLogLogLine

data class RequestLogResource(
    val ref: String,
    val id: String,
    val url: String?,
    val path: String?,
    val freeMessage: String?,
    val method: String?,
    val xForwardedFor: String?,
    val tookInMilliSeconds: Long?,
    val remoteAddress: String?,
    val userAgent: String?,
    val user: String?,
    val created: String?,
    val responseCode: String?,
    val errorMessage: String?,
    val isFreeMessageLog: Boolean,
    val payloadSize: String,
    val parts: List<RequestLogResource> = emptyList(),
    val logLines: List<RequestLogLogLine> = emptyList(),
) {
    val message = if (this.isFreeMessageLog) this.freeMessage else this.errorMessage

    companion object {
        fun from(requestLog: RequestLog): RequestLogResource {
            return RequestLogResource(
                ref = requestLog.ref.toString(),
                url = requestLog.url,
                path = requestLog.path,
                method = requestLog.method,
                remoteAddress = requestLog.remoteAddress,
                user = requestLog.user,
                userAgent = requestLog.userAgent,
                xForwardedFor = requestLog.xForwardedFor,
                tookInMilliSeconds = requestLog.tookInMilliSeconds,
                responseCode = requestLog.responseCode,
                errorMessage = requestLog.errorMessage,
                created = DateUtils.toString(requestLog.created),
                id = requestLog.id,
                freeMessage = requestLog.freeMessage,
                isFreeMessageLog = requestLog.isFreeMessageLog,
                payloadSize = requestLog.payloadSize,
                logLines = requestLog.logLines
            )
        }
    }
}
