package solutions.viae.requestlog.core.services

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.service.BadRequestResponseType
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.service.OkResponseType
import solutions.viae.coreutils.service.ViaeHttpRequest
import solutions.viae.coreutils.service.ViaeHttpResponse
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.domain.RequestLogRef
import solutions.viae.requestlog.core.domain.UserId
import solutions.viae.requestlog.core.factories.CrudRequestLogRepositoryFactory
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

data class DoCreateRequestLogData(
    val message: String,
    val tookInMilliSeconds: Long? = -1,
    val payloadSize: String? = "not-set"
)

interface RequestLogService {
    val applicationName: ApplicationName

    fun getRequestLogs(after: ZonedDateTime): Response<List<RequestLog>>

    fun getRequestLog(ref: RequestLogRef): Response<RequestLog>

    fun getRequestLogsFor(after: ZonedDateTime, userId: UserId): Response<List<RequestLog>>

    fun getRequestLogs(after: ZonedDateTime, userId: String?): Response<List<RequestLog>> {
        return try {
            if (userId == null) {
                getRequestLogs(after)
            } else {
                getRequestLogsFor(after, userId)
            }
        } catch (e: Exception) {
            failure(RequestLogService::class, "could not get the request logs: ${e.localizedMessage}")
        }
    }

    fun createRequestLog(body: DoCreateRequestLogData?, httpRequest: ViaeHttpRequest): ViaeHttpResponse<Nothing?>

    fun saveRequestLog(requestLog: RequestLog)

    /**
     * @param payloadSizeResolver: how big is the resulting data set (i.e. the response). Regarding performance logs, this can make a difference
     */
    fun <T> logExecutionMetaData(
        httpRequest: ViaeHttpRequest,
        callback: (logger: Logger) -> Response<T>,
        payloadSizeResolver: (data: T) -> String
    ): ViaeHttpResponse<T> {
        return logExecutionMetaData(httpRequest, callback, { d -> d }, payloadSizeResolver)
    }

    fun <T, R> logExecutionMetaData(
        httpRequest: ViaeHttpRequest,
        callback: (logger: Logger) -> Response<T>,
        successResponseResolver: (data: T) -> R,
        payloadSizeResolver: (data: T) -> String
    ): ViaeHttpResponse<R> {
        val logger = RequestLogLogger()
        val start = DateUtils.now()
        val response = callback(logger)
        val payloadSize: String
        val result: ViaeHttpResponse<R> = if (response.hasNoErrors()) {
            payloadSize = payloadSizeResolver(response.data)
            OkResponseType(successResponseResolver(response.data))
        } else {
            payloadSize = "0: in error"
            BadRequestResponseType(response.errors)
        }
        val end = DateUtils.now()
        val tookInMilliSeconds = start.until(end, ChronoUnit.MILLIS)
        val requestLog = RequestLog.from(httpRequest, applicationName, response, tookInMilliSeconds, payloadSize, logger.logLines)
        saveRequestLog(requestLog)
        return result
    }
}

class DefaultMongoDbRequestLogService constructor(
private val repositoryFactory: CrudRequestLogRepositoryFactory,
override val applicationName: ApplicationName
) : RequestLogService {
    override fun getRequestLogs(after: ZonedDateTime): Response<List<RequestLog>> {
        return repositoryFactory.requestLogsRepository.getRequestLogs(after)
    }

    override fun getRequestLogsFor(after: ZonedDateTime, userId: UserId): Response<List<RequestLog>> {
        return repositoryFactory.requestLogsRepository.getRequestLogsFor(after, userId)
    }

    override fun getRequestLog(ref: RequestLogRef): Response<RequestLog> {
        return repositoryFactory.requestLogsRepository.getRequestLog(ref)
    }

    override fun createRequestLog(body: DoCreateRequestLogData?, httpRequest: ViaeHttpRequest): ViaeHttpResponse<Nothing?> {
        return if (body == null) {
            BadRequestResponseType(listOf(ErrorMessage(DefaultMongoDbRequestLogService::class, "do create body should not be null")))
        } else {
            saveRequestLog(
                RequestLog.from(httpRequest, applicationName, body.tookInMilliSeconds, body.message, body.payloadSize ?: "not-set")
            )
            OkResponseType(null)
        }
    }

    override fun saveRequestLog(requestLog: RequestLog) {
        repositoryFactory.requestLogsRepository.saveRequestLog(
            RequestLog(
                applicationName = requestLog.applicationName,
                url = requestLog.url,
                path = requestLog.path,
                method = requestLog.method,
                remoteAddress = requestLog.remoteAddress,
                user = requestLog.user,
                userAgent = requestLog.userAgent,
                xForwardedFor = requestLog.xForwardedFor,
                tookInMilliSeconds = requestLog.tookInMilliSeconds,
                responseCode = requestLog.responseCode,
                errorMessage = requestLog.errorMessage,
                created = requestLog.created,
                id = requestLog.id,
                groupId = requestLog.groupId,
                freeMessage = requestLog.freeMessage,
                isFreeMessageLog = requestLog.isFreeMessageLog,
                payloadSize = requestLog.payloadSize,
                environment = requestLog.environment,
                logLines = requestLog.logLines
            )
        ).orElse { IllegalStateException("Could not save request log: ${it}") }
    }
}
