package solutions.viae.requestlog.core.repositories

import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.requestlog.core.domain.RequestLog
import solutions.viae.requestlog.core.domain.RequestLogRef
import solutions.viae.requestlog.core.domain.UserId
import java.time.ZonedDateTime

interface RequestLogsRepository {

    fun saveRequestLog(requestLog: RequestLog): Response<Long>

    fun getRequestLog(ref: RequestLogRef): Response<RequestLog>

    fun getRequestLogs(after: ZonedDateTime): Response<List<RequestLog>>

    fun getRequestLogsFor(after: ZonedDateTime, userId: UserId): Response<List<RequestLog>>
}
