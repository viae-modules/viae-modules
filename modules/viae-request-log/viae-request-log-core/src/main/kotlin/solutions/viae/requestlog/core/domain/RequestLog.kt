package solutions.viae.requestlog.core.domain

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.ViaeHttpRequest
import solutions.viae.coreutils.utils.DateUtils
import java.time.ZonedDateTime
import java.util.*

typealias UserId = String
typealias RequestLogRef = UUID

/**
 * A log entry.
 * This class contains a field with log lines as well:
 * The root log entry can e.g. be a REST endpoint call.
 * In this case there will be one entry to cover the REST call and
 * it will have log lines for logging what happened within the processing
 * of the REST request
 */
data class RequestLog(
    val id: String,
    val applicationName: ApplicationName,
    val groupId: String?,
    val url: String,
    val path: String,
    val freeMessage: String,
    val method: String,
    val xForwardedFor: String,
    val tookInMilliSeconds: Long,
    val remoteAddress: String,
    val userAgent: String,
    val user: UserId,
    val created: ZonedDateTime,
    val responseCode: String,
    val errorMessage: String = "",
    val isFreeMessageLog: Boolean,
    val payloadSize: String,
    val environment: String,
    val logLines: List<RequestLogLogLine>,
    val ref: RequestLogRef = UUID.randomUUID()
) {
    companion object {
        fun from(
            httpRequest: ViaeHttpRequest,
            applicationName: ApplicationName,
            response: Response<*>,
            tookInMilliSeconds: Long,
            payloadSize: String,
            logLines: List<RequestLogLogLine>
        ): RequestLog {
            return from(httpRequest, applicationName, response, tookInMilliSeconds, payloadSize, null, logLines)
        }

        fun from(httpRequest: ViaeHttpRequest, applicationName: ApplicationName,tookInMilliSeconds: Long?, payloadSize: String, freeMessage: String): RequestLog {
            return from(httpRequest, applicationName, null, tookInMilliSeconds ?: -1, payloadSize, freeMessage, emptyList())
        }

        private fun from(
            httpRequest: ViaeHttpRequest,
            applicationName: ApplicationName,
            response: Response<*>?,
            tookInMilliSeconds: Long,
            payloadSize: String,
            freeMessage: String?,
            logLines: List<RequestLogLogLine>
        ): RequestLog {
            val userId: String
            val xForwardedFor: String
            val userAgent: String
            if (!listOf(Environment.LOCAL).contains(httpRequest.environment)) {
                xForwardedFor = httpRequest.headers.getValue(checkNotNull("X-Forwarded-For") { "no X-Forwarded-For header" })
                userAgent = httpRequest.headers.getValue(checkNotNull("User-Agent") { "no User-Agent header" })
                userId = httpRequest.headers.getValue(checkNotNull("User-Id") { "no User-Id header" })
            } else {
                userId = httpRequest.headers.get("User-Id") ?: "no User-Id header"
                xForwardedFor = httpRequest.headers.get("X-Forwarded-For") ?: "no X-Forwarded-For header"
                userAgent = httpRequest.headers.get("User-Agent") ?: "no User-Agent header"
            }
            return RequestLog(
                applicationName = applicationName,
                url = httpRequest.uri.toString(),
                path = httpRequest.uri.path,
                method = httpRequest.method,
                xForwardedFor = xForwardedFor,
                userAgent = userAgent,
                remoteAddress = "${httpRequest.hostName}:${httpRequest.port}",
                user = userId,
                tookInMilliSeconds = tookInMilliSeconds,
                created = DateUtils.now(),
                errorMessage = response?.errors?.joinToString(";") ?: "",
                responseCode = if (response?.hasNoErrors() != false) "200" else "422",
                id = httpRequest.headers.get("Request-Id") ?: UUID.randomUUID().toString(),
                groupId = httpRequest.headers.get("Request-Group-Id"),
                freeMessage = freeMessage ?: "",
                isFreeMessageLog = freeMessage != null,
                payloadSize = payloadSize,
                environment = httpRequest.environment.name,
                logLines = logLines
            )
        }
    }
}
