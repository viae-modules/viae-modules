#VIAE Docker Base Images

## connect to an image (e.g. to test)
* docker build -t test -f java-node-dockercompse-dockerfile .
* docker run test:latest tail -f /dev/null (tail command is there to keep the container running)
* other shell (or run previous command in the background): docker ps (and identify the container ID of the docker process)
* in the second shell: docker exec -it <CONTAiNER_ID> sh
