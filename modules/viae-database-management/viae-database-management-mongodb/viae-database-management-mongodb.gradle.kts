dependencies {
    api(project(":modules:viae-core-utils"))
    api(project(":modules:viae-database-management:viae-database-management-core"))
    api(Libraries.Database.mongodbDriverCore)
    api(Libraries.Database.mongodbDriverSync)
    api(Libraries.Kotlinx.serializationRuntime)
    api(Libraries.Jackson.kotlin)
}
