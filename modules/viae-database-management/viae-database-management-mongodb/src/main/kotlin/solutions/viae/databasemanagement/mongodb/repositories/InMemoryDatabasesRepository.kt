package solutions.viae.databasemanagement.mongodb.repositories

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.success
import solutions.viae.databasemanagement.core.api.domain.Database
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.impl.api.CreateDatabaseData
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository

class InMemoryDatabasesRepository : CrudDatabasesRepository {

    private val databaseIds = mutableListOf<DatabaseId>()
    private val databases = mutableMapOf<DatabaseId, Database>()

    override fun cleanUpAllNoneActiveDatabases(): Response<Unit> {
        val toBeDeleted: List<DatabaseId> = databases.entries
            .map { e ->
                if (!e.value.isActive.values.contains(true)) {
                    e.key
                } else {
                    null
                }
            }.filterNotNull()
        toBeDeleted.forEach { id ->
            databaseIds.remove(id)
            databases.remove(id)
        }
        return success()
    }

    override fun ensureGeoLocationIndex(
        databaseId: DatabaseId,
        collectionName: String,
        indexName: String,
        vararg fieldNames: String
    ): Response<Boolean> {
        return success(false)
    }

    override fun ensureAscendingIndex(
        databaseId: DatabaseId,
        collectionName: String,
        indexName: String,
        vararg fieldNames: String
    ): Response<Boolean> {
        return success(false)
    }

    override fun ensureDescendingIndex(
        databaseId: DatabaseId,
        collectionName: String,
        indexName: String,
        vararg fieldNames: String
    ): Response<Boolean> {
        return success(false)
    }

    override fun markDatabaseActive(databaseId: DatabaseId, environment: Environment): Response<Unit> {
        val isActive = databases[databaseId]!!.isActive.toMutableMap()
        isActive[environment] = true
        databases[databaseId] = databases[databaseId]!!.copy(isActive = isActive)
        return success()
    }

    override fun markDatabaseInactive(databaseId: DatabaseId, environment: Environment): Response<Unit> {
        val isActive = databases[databaseId]!!.isActive.toMutableMap()
        isActive[environment] = false
        databases[databaseId] = databases[databaseId]!!.copy(isActive = isActive)
        return success()
    }

    override fun get(databaseId: DatabaseId): Response<Database> {
        return success(databases[databaseId]!!)
    }

    override fun getAvailableDatabases(environment: Environment, active: Boolean, applicationName: ApplicationName?): Response<List<Database>> {
        return success(
            databaseIds
                .mapNotNull { n -> databases[n] }
                .filter { d -> filterOnActive(d, active, environment) }
                .filter { d -> d.application == applicationName || applicationName == null }
        )
    }

    override fun createDatabase(database: CreateDatabaseData): Response<Unit> {
        databaseIds.add(database.id)
        databases[database.id] = Database(database.id, database.name, database.application, database.createdTs, database.isActive)
        return success()
    }

    override fun removeDatabase(databaseId: DatabaseId): Response<Unit> {
        databaseIds.remove(databaseId)
        databases.remove(databaseId)
        return success()
    }

    private fun filterOnActive(database: Database, active: Boolean, environment: Environment): Boolean {
        return if (active) {
            database.isActive.getValue(environment)
        } else {
            !database.isActive.getValue(environment)
        }
    }

}
