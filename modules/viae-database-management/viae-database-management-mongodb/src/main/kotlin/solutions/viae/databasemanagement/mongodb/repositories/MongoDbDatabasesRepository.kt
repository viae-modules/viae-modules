package solutions.viae.databasemanagement.mongodb.repositories

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.and
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.Updates.set
import org.bson.Document
import org.bson.conversions.Bson
import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.Response.Companion.failure
import solutions.viae.coreutils.domain.Response.Companion.success
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.api.domain.Database
import solutions.viae.databasemanagement.core.impl.api.CreateDatabaseData
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository

class MongoDbDatabasesRepository(private val mongoClient: MongoClient) : CrudDatabasesRepository {
    private val databaseCache = mutableMapOf<DatabaseId, Database>()

    override fun ensureGeoLocationIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean> {
        return try {
            val index = Indexes.geo2dsphere(fieldNames.asList())
            createIndex(databaseId, collectionName, indexName, index)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in ensureGeoLocationIndex: ${e.localizedMessage}")
        }
    }

    fun ensureGeoLocationIndex(databaseName: String, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean> {
        return try {
            val index = Indexes.geo2dsphere(fieldNames.asList())
            createIndex(databaseName, collectionName, indexName, index)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in ensureGeoLocationIndex: ${e.localizedMessage}")
        }
    }

    override fun ensureAscendingIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean> {
        return try {
            val index = Indexes.ascending(fieldNames.asList())
            createIndex(databaseId, collectionName, indexName, index)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in ensureAscendingIndex: ${e.localizedMessage}")
        }
    }

    override fun ensureDescendingIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean> {
        return try {
            val index = Indexes.descending(fieldNames.asList())
            createIndex(databaseId, collectionName, indexName, index)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in ensureDescendingIndex: ${e.localizedMessage}")
        }
    }

    override fun cleanUpAllNoneActiveDatabases(): Response<Unit> {
        return try {
            val result = getInfoCollection()
                .find()
                .map { d -> toDatabase(d) }
                .forEach { e ->
                    if (!e.isActive.values.contains(true) && !e.name.startsWith("do-not-remove-on-")) {
                        removeDatabase(e.id)
                        databaseCache.remove(e.id)
                    }
                }
            success(result)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in cleanUpAllNoneActiveDatabases: ${e.localizedMessage}")
        }
    }

    override fun markDatabaseActive(databaseId: DatabaseId, environment: Environment): Response<Unit> {
        return try {
            getInfoCollection().findOneAndUpdate(
                eq("id", databaseId.asString()),
                set("activeEnvironments.${environment.name}", true)
            )
            refreshCachedDatabase(databaseId)
            success()
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in markDatabaseActive: ${e.localizedMessage}")
        }
    }

    override fun markDatabaseInactive(databaseId: DatabaseId, environment: Environment): Response<Unit> {
        return try {
            getInfoCollection().findOneAndUpdate(
                eq("id", databaseId.asString()),
                set("activeEnvironments.${environment.name}", false)
            )
            refreshCachedDatabase(databaseId)
            success()
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in markDatabaseInactive: ${e.localizedMessage}")
        }
    }

    override fun get(databaseId: DatabaseId): Response<Database> {
        return try {
            if (databaseCache.containsKey(databaseId)){
                success(databaseCache.get(databaseId)!!)
            } else {
                val result = getInfoCollection().find(eq("id", databaseId.asString()))
                return result.first()?.let {
                    val db = toDatabase(it)
                    databaseCache[databaseId] = db
                    success(db)
                }?: failure(MongoDbDatabasesRepository::class, "database '${databaseId.asString()}' not found")
            }
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in get: ${e.localizedMessage}")
        }
    }

    override fun removeDatabase(databaseId: DatabaseId): Response<Unit> {
        return try {
            val response = get(databaseId).map { db ->
                mongoClient.getDatabase(db!!.name).drop()
                getInfoCollection().findOneAndDelete(eq("id", databaseId.asString()))
            }
            refreshCachedDatabase(databaseId)
            if (response.hasNoErrors()) success() else failure(response.errors)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in removeDatabase: ${e.localizedMessage}")
        }
    }

    override fun createDatabase(database: CreateDatabaseData): Response<Unit> {
        return try {
            val databaseName = database.name
//            mongoClient.getDatabase(databaseName).createCollection("meta_data")
            getInfoCollection().insertOne(
                Document()
                    .append("id", database.id.asString())
                    .append("created", DateUtils.toString(database.createdTs))
                    .append("name", databaseName)
                    .append("application", database.application)
                    .append("activeEnvironments", Document(database.isActive.mapKeys { e -> e.key.name }))
            )
            success()
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in createDatabase: ${e.localizedMessage}")
        }
    }

    override fun getAvailableDatabases(environment: Environment, active: Boolean, applicationName: ApplicationName?): Response<List<Database>> {
        return try {
            val result = getInfoCollection()
                .find(
                    and(listOfNotNull(
                        eq("activeEnvironments.${environment.name}", active),
                        if(applicationName != null) eq("application", applicationName) else null
                    ))
                )
                .map { d -> toDatabase(d) }
                .toList()
            success(result)
        } catch (e: Exception) {
            failure(MongoDbDatabasesRepository::class, "error in getAvailableDatabases: ${e.localizedMessage}")
        }
    }

    private fun toDatabase(document: Document): Database {
        return Database(
            id = DatabaseId.fromString(document.getString("id")),
            name = document.getString("name"),
            application = document.getString("application"),
            createdTs = DateUtils.toTimeStamp(document.getString("created")),
            isActive = document.get("activeEnvironments", Document::class.java).toMap()
                .mapKeys { e -> Environment.valueOf(e.key) }
                .mapValues { e -> e.value as Boolean }
        )
    }

    private fun refreshCachedDatabase(databaseId: DatabaseId) {
        databaseCache.remove(databaseId)
        get(databaseId)
    }

    private fun getInfoCollection(): MongoCollection<Document> {
        return mongoClient
            .getDatabase("ps_databases_meta_data")
            .getCollection("info")
    }

    private fun createIndex(
        databaseId: DatabaseId,
        collectionName: String,
        indexName: String,
        index: Bson
    ): Response<Boolean> {
        val dbName = get(databaseId).data.name
        return createIndex(dbName, collectionName, indexName, index)
    }

    private fun createIndex(
        dbName: String,
        collectionName: String,
        indexName: String,
        index: Bson
    ): Response<Boolean> {
        val collection = mongoClient.getDatabase(dbName).getCollection(collectionName)
        val indexNames = collection
            .listIndexes()
            .toList()
            .map { d -> d.getString("name") }
        return if (!indexNames.contains(indexName)) {
            collection.createIndex(index, IndexOptions().name(indexName))
            success(true)
        } else {
            success(false)
        }
    }

}
