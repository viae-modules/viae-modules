package solutions.viae.databasemanagement.mongodb.factories

import com.mongodb.client.MongoClient
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.Environment.IN_MEMORY
import solutions.viae.coreutils.service.Logger
import solutions.viae.databasemanagement.core.api.factories.DatabaseManagementRepositoryFactory
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import solutions.viae.databasemanagement.core.impl.factories.DatabaseManagementCrudRepositoryFactory
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import solutions.viae.databasemanagement.mongodb.repositories.InMemoryDatabasesRepository
import solutions.viae.databasemanagement.mongodb.repositories.MongoDbDatabasesRepository

class DbManagementRepositoryFactory(
    environment: Environment,
    mongoClient: MongoClient,
    logger: Logger
) : DatabaseManagementRepositoryFactory, DatabaseManagementCrudRepositoryFactory {
    private val databasesRepository: CrudDatabasesRepository = if (IN_MEMORY == environment) {
        InMemoryDatabasesRepository()
    } else {
        MongoDbDatabasesRepository(mongoClient)
    }

    override val readOnlyDatabasesRepository: ReadOnlyDatabasesRepository
        get() = databasesRepository
    override val crudDatabasesRepository: CrudDatabasesRepository
        get() = databasesRepository

}
