package solutions.viae.databasemanagement.mongodb.config

import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.databasemanagement.core.api.domain.ApplicationDatabasePrefixRegistry
import solutions.viae.databasemanagement.core.api.factories.DatabaseManagementUseCaseFactory
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import solutions.viae.databasemanagement.mongodb.factories.DbManagementRepositoryFactory

interface MongoDbConnectionData {
    val mongoSyncClient: MongoClient

    companion object {
        fun getConnectionDataFor(
            mongoServerAddresses: List<String>?,
            mongoUserName: String?,
            mongoPassword: String?,
            mongoSource: String?,
            connectionString: String?
        ): Response<MongoDbConnectionData> {
            return if (connectionString != null && connectionString.isNotBlank()) {
                Response.success(ConnectionStringBasedMongoDbConnectionData(connectionString))
            } else if (
                hasUserNameAndPasswordFilled(mongoUserName, mongoPassword)
                && hasMongoSourceFilled(mongoSource)
                && hasServerAddressesFilled(mongoServerAddresses)
            ) {
                Response.success(
                    ConfigBasedMongoDbConnectionData(
                        mongoUserName = mongoUserName!!,
                        mongoPassword = mongoPassword!!,
                        mongoSource = mongoSource!!,
                        mongoServerAddresses = mongoServerAddresses!!
                    )
                )
            } else {
                Response.failure(
                    ErrorMessage(
                        MongoDbConnectionData::class,
                        "when no connection string is provided, valid mongo db config parameters should be passed"
                    )
                )
            }
        }

        private fun hasServerAddressesFilled(mongoServerAddresses: List<String>?) =
            mongoServerAddresses?.isNotEmpty() == true

        private fun hasUserNameAndPasswordFilled(mongoUserName: String?, mongoPassword: String?) =
            (mongoUserName?.isNotBlank() == true && mongoPassword?.isNotBlank() == true)

        private fun hasMongoSourceFilled(mongoSource: String?) =
            mongoSource?.isNotBlank() == true
    }
}

class ConfigBasedMongoDbConnectionData(
    private val mongoServerAddresses: List<String>,
    private val mongoUserName: String,
    private val mongoPassword: String,
    private val mongoSource: String
) : MongoDbConnectionData {
    override val mongoSyncClient: MongoClient
        get() {
            val settings = MongoClientSettings.builder()
                .applyToClusterSettings { builder ->
                    builder.hosts(
                        mongoServerAddresses.map { msa -> ServerAddress(msa) }
                    )
                }
                .applyToSslSettings { builder -> builder.enabled(true) }
                .credential(
                    MongoCredential.createScramSha1Credential(
                        mongoUserName,
                        mongoSource,
                        mongoPassword.toCharArray()
                    )
                )
                .build()
            return MongoClients.create(settings)
        }
}

class ConnectionStringBasedMongoDbConnectionData(
    private val connectionString: String
) : MongoDbConnectionData {
    override val mongoSyncClient: MongoClient
        get() {
            return MongoClients.create(connectionString)
        }
}

class MongoDbBasedDatabaseManagementConfig(
    mongoDbConnectionData: MongoDbConnectionData,
    logger: Logger,
    environment: Environment,
    private val applicationDatabasePrefixRegistry: ApplicationDatabasePrefixRegistry
) {
    private val dbManagementRepositoryFactory = DbManagementRepositoryFactory(
        environment = environment,
        mongoClient = mongoDbConnectionData.mongoSyncClient,
        logger = logger
    )

    fun initDatabaseManagementUseCaseFactory(): DatabaseManagementUseCaseFactory {
        return DatabaseManagementUseCaseFactory(
            readOnlyRepositoryFactory = dbManagementRepositoryFactory,
            crudRepositoryFactory = dbManagementRepositoryFactory,
            applicationDatabasePrefixRegistry = applicationDatabasePrefixRegistry
        )
    }

    fun initReadOnlyDatabaseManagementRepository(): ReadOnlyDatabasesRepository {
        return dbManagementRepositoryFactory.readOnlyDatabasesRepository
    }

    fun initCrudDatabaseManagementRepository(): CrudDatabasesRepository {
        return dbManagementRepositoryFactory.crudDatabasesRepository
    }
}
