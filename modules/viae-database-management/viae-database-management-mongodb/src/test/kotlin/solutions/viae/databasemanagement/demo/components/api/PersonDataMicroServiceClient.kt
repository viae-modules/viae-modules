package solutions.viae.databasemanagement.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client

@Client(
    value = "/api/data/person",
    errorType = String::class
)
interface PersonDataMicroServiceClient {

    @Get("/", produces = [MediaType.APPLICATION_JSON])
    fun getAllData(): HttpResponse<List<String>>

    @Post("/", produces = [MediaType.APPLICATION_JSON], consumes = [MediaType.APPLICATION_JSON])
    fun createData(@QueryValue databaseId: String, @Body personData: String): HttpResponse<Nothing>
}
