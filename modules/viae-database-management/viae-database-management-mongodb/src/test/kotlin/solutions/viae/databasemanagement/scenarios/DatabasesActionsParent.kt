package solutions.viae.databasemanagement.scenarios

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.assertAll
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment.LOCAL_TESTING
import solutions.viae.coreutils.domain.Environment.LOCAL_TESTING_1
import solutions.viae.coreutils.domain.Environment.LOCAL_TESTING_2
import solutions.viae.coreutils.domain.Environment.LOCAL_TESTING_3
import solutions.viae.coreutils.domain.Environment.DEV
import solutions.viae.coreutils.domain.Environment.QA
import solutions.viae.coreutils.domain.Environment.PROD
import solutions.viae.coreutils.domain.Environment.INT
import solutions.viae.coreutils.domain.Environment.TEST
import solutions.viae.coreutils.domain.Environment.LOCAL
import solutions.viae.databasemanagement.demo.components.api.DatabasesControllerClient
import solutions.viae.databasemanagement.demo.components.api.ListBasedDatabasesControllerClient
import solutions.viae.databasemanagement.demo.components.api.SingleJsonFieldBasedDatabasesControllerClient
import java.util.UUID
import javax.inject.Inject

abstract class DatabasesActionsParent {

    @Inject
    private lateinit var databasesClient: DatabasesControllerClient
    @Inject
    private lateinit var singleJsonFieldBasedDatabasesControllerClient: SingleJsonFieldBasedDatabasesControllerClient
    @Inject
    private lateinit var listBasedDatabasesControllerClient: ListBasedDatabasesControllerClient

    open fun setupFreshFixture() {
        databasesClient
            .getAvailableDatabases(isActive = true, environment = LOCAL_TESTING_1.name).body()!!
            .forEach { databasesClient.markDatabaseInactive(LOCAL_TESTING_1.name, it.id) }
        databasesClient
            .getAvailableDatabases(isActive = true, environment = LOCAL_TESTING_2.name).body()!!
            .forEach { databasesClient.markDatabaseInactive(LOCAL_TESTING_2.name, it.id) }
        databasesClient
            .getAvailableDatabases(isActive = true, environment = LOCAL_TESTING_3.name).body()!!
            .forEach { databasesClient.markDatabaseInactive(LOCAL_TESTING_3.name, it.id) }
        databasesClient
            .getAvailableDatabases(isActive = true, environment = LOCAL_TESTING.name).body()!!
            .forEach { databasesClient.markDatabaseInactive(LOCAL_TESTING.name, it.id) }
    }

    open fun testGetAllActiveDatabasesForActiveVsNonActive() {
        //Given
        val databaseId1 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        val databaseId2 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        //And
        databasesClient.markDatabaseActive(databaseId = databaseId1, environment = LOCAL_TESTING_1.name)

        //When
        val activeDbs =
            databasesClient.getAvailableDatabases(isActive = true, environment = LOCAL_TESTING_1.name).body()!!
        val inactiveDbs =
            databasesClient.getAvailableDatabases(isActive = false, environment = LOCAL_TESTING_1.name).body()!!

        //Then
        assertThat(activeDbs.map { db -> db.id }).contains(databaseId1)
        assertThat(activeDbs.map { db -> db.id }).doesNotContain(databaseId2)
        assertThat(inactiveDbs.map { db -> db.id }).doesNotContain(databaseId1)
        assertThat(inactiveDbs.map { db -> db.id }).contains(databaseId2)
    }

    open fun testMarkInactiveWithoutEnvironment() {
        //When
        val response =
            databasesClient.markDatabaseInactive(databaseId = UUID.randomUUID().toString(), environment = null)
        //Then
        assertThat(response.header("error-message")).isEqualTo("solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseInactiveUseCase-could not transform environment: 'null', cause: null")
    }

    open fun testMarkInactiveWithInvalidEnvironmentAndWithInvalidDatabaseId() {
        //When
        val response =
            databasesClient.markDatabaseInactive(
                databaseId = "not-a-database-id",
                environment = "should-not-be-known"
            )
        //Then
        assertThat(response.header("error-message"))
            .contains("solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseInactiveUseCase-could not transform to database id: 'not-a-database-id', cause: Invalid UUID string: not-a-database-id")
        assertThat(response.header("error-message"))
            .contains("ErrorMessage(classSource=class solutions.viae.coreutils.domain.Environment, message=Environment 'should-not-be-known' is not supported, possible values are: ")
    }

    open fun testGetAllActiveDatabasesWithoutEnvironment() {
        //When
        val response = listBasedDatabasesControllerClient.getAvailableDatabases(isActive = false, environment = null)
        //Then
        assertThat(response.header("error-message")).isEqualTo("com.viae.propertyanalysis.configuration.controllers.DatabasesController-environment is required")
    }

    open fun testGetAllActiveDatabasesForEnvironmentNotKnown() {
        //When
        val response =
            databasesClient.getAvailableDatabases(isActive = false, environment = "should-not-be-known")
        //Then
        assertThat(response.header("error-message"))
            .contains("Environment 'should-not-be-known' is not supported, possible values are: [")
    }

    open fun testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironment() {
        //Given
        val databaseId1 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        val databaseId2 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        //And
        databasesClient.markDatabaseActive(databaseId = databaseId1, environment = LOCAL_TESTING.name)

        //When
        val response =
            databasesClient.markDatabaseActive(databaseId = databaseId2, environment = LOCAL_TESTING.name)

        //Then
        assertThat(response.header("error-message"))
            .isEqualTo("solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase-pre-validation failed, cause: can't get data: there are exceptions: ErrorMessage(classSource=class solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase, message=for the given environment (LOCAL_TESTING) there is/are already active databases: [$databaseId1], errorGroup=pre-validation, errorType=FUNCTIONAL)")
    }

    open fun testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironmentWithSwitchIfExistsParameter() {
        //Given
        val databaseId1 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        val databaseId2 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        //And
        databasesClient.markDatabaseActive(databaseId = databaseId1, environment = LOCAL_TESTING.name)
        val initialDatabases =
            databasesClient.getAvailableDatabases(isActive = true, environment = LOCAL_TESTING.name).body()!!
                .map { db -> db.id }
        assertThat(initialDatabases).isEqualTo(listOf(databaseId1))

        //When
        val response =
            databasesClient.markDatabaseActive(
                databaseId = databaseId2,
                environment = LOCAL_TESTING.name,
                switchIfExists = true
            )

        //Then
        assertThat(response.status().code).isEqualTo(204)
        val resultingDatabases =
            databasesClient.getAvailableDatabases(isActive = true, environment = LOCAL_TESTING.name).body()!!
                .map { db -> db.id }
        assertThat(resultingDatabases).isEqualTo(listOf(databaseId2))
    }

    open fun testMarkingADatabaseInactive() {
        //Given
        val databaseId = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        //And
        databasesClient.markDatabaseActive(databaseId = databaseId, environment = LOCAL_TESTING.name)
        //And
        val initialDatabases =
            databasesClient.getAvailableDatabases(isActive = true, environment = LOCAL_TESTING.name).body()

        //When
        databasesClient.markDatabaseInactive(databaseId = databaseId, environment = LOCAL_TESTING.name)

        //Then
        val database = databasesClient.getDatabase(databaseId).body()!!
        assertAll(
            { assertThat(database.name).startsWith("ms-people-") },
            {
                assertThat(database.active[LOCAL_TESTING.name])
                    .withFailMessage("${LOCAL_TESTING.name} : '${database.active[LOCAL_TESTING.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[LOCAL.name])
                    .withFailMessage("${LOCAL.name} : '${database.active[LOCAL.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[DEV.name])
                    .withFailMessage("${DEV.name} : '${database.active[DEV.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[INT.name])
                    .withFailMessage("${INT.name} : '${database.active[INT.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[TEST.name])
                    .withFailMessage("${TEST.name} : '${database.active[TEST.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[QA.name])
                    .withFailMessage("${QA.name} : '${database.active[QA.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[PROD.name])
                    .withFailMessage("${PROD.name} : '${database.active[PROD.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(
                    databasesClient.getAvailableDatabases(
                        isActive = true,
                        environment = LOCAL_TESTING.name
                    ).body()!!.size
                ).isEqualTo(initialDatabases!!.size - 1)
            }
        )
    }

    open fun testMarkingDifferentDatabasesActiveForDifferentEnvironments() {
        //Given
        val databaseId1 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second
        val databaseId2 = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second

        //When
        databasesClient.markDatabaseActive(databaseId = databaseId1, environment = LOCAL_TESTING_1.name)
        databasesClient.markDatabaseActive(databaseId = databaseId2, environment = LOCAL_TESTING_2.name)
        databasesClient.markDatabaseActive(databaseId = databaseId1, environment = LOCAL_TESTING_3.name)

        //Then
        val database1 = databasesClient.getDatabase(databaseId1).body()!!
        val database2 = databasesClient.getDatabase(databaseId2).body()!!
        assertAll(
            { assertThat(database1.name).startsWith("ms-people-") },
            {
                assertThat(database1.active[LOCAL_TESTING_1.name])
                    .withFailMessage("${LOCAL_TESTING_1.name} : '${database1.active[LOCAL_TESTING_1.name]}'")
                    .isEqualTo(true)
            },
            {
                assertThat(database1.active[LOCAL_TESTING_2.name])
                    .withFailMessage("${LOCAL_TESTING_2.name} : '${database1.active[LOCAL_TESTING_2.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[LOCAL_TESTING_3.name])
                    .withFailMessage("${LOCAL_TESTING_3.name} : '${database1.active[LOCAL_TESTING_3.name]}'")
                    .isEqualTo(true)
            },
            {
                assertThat(database1.active[LOCAL_TESTING.name])
                    .withFailMessage("${LOCAL_TESTING.name} : '${database1.active[LOCAL_TESTING.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[LOCAL.name])
                    .withFailMessage("${LOCAL.name} : '${database1.active[LOCAL.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[DEV.name])
                    .withFailMessage("${DEV.name} : '${database1.active[DEV.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[INT.name])
                    .withFailMessage("${INT.name} : '${database1.active[INT.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[TEST.name])
                    .withFailMessage("${TEST.name} : '${database1.active[TEST.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[QA.name])
                    .withFailMessage("${QA.name} : '${database1.active[QA.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database1.active[PROD.name])
                    .withFailMessage("${PROD.name} : '${database1.active[PROD.name]}'")
                    .isEqualTo(false)
            }
        )
        assertAll(
            { assertThat(database2.name).startsWith("ms-people-") },
            {
                assertThat(database2.active[LOCAL_TESTING_1.name])
                    .withFailMessage("${LOCAL_TESTING_1.name} : '${database1.active[LOCAL_TESTING_1.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[LOCAL_TESTING_2.name])
                    .withFailMessage("${LOCAL_TESTING_2.name} : '${database1.active[LOCAL_TESTING_2.name]}'")
                    .isEqualTo(true)
            },
            {
                assertThat(database2.active[LOCAL_TESTING_3.name])
                    .withFailMessage("${LOCAL_TESTING_3.name} : '${database1.active[LOCAL_TESTING_3.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[LOCAL_TESTING.name])
                    .withFailMessage("${LOCAL_TESTING.name} : '${database2.active[LOCAL_TESTING.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[LOCAL.name])
                    .withFailMessage("${LOCAL.name} : '${database2.active[LOCAL.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[DEV.name])
                    .withFailMessage("${DEV.name} : '${database2.active[DEV.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[INT.name])
                    .withFailMessage("${INT.name} : '${database2.active[INT.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[TEST.name])
                    .withFailMessage("${TEST.name} : '${database2.active[TEST.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[QA.name])
                    .withFailMessage("${QA.name} : '${database2.active[QA.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database2.active[PROD.name])
                    .withFailMessage("${PROD.name} : '${database2.active[PROD.name]}'")
                    .isEqualTo(false)
            }
        )
    }

    open fun testMarkingADatabaseActive() {
        //Given
        val initialDatabases =
            databasesClient.getAvailableDatabases(isActive = true, environment = LOCAL_TESTING.name).body()
        //And
        val databaseId = databasesClient.createNewDatabase("MS-PEOPLE").body()!!.second

        //When
        databasesClient.markDatabaseActive(databaseId = databaseId, environment = LOCAL_TESTING.name)

        //Then
        val database = databasesClient.getDatabase(databaseId).body()!!
        assertAll(
            { assertThat(database.name).startsWith("ms-people-") },
            {
                assertThat(database.active[LOCAL_TESTING.name])
                    .withFailMessage("${LOCAL_TESTING.name} : '${database.active[LOCAL_TESTING.name]}'")
                    .isEqualTo(true)
            },
            {
                assertThat(database.active[LOCAL.name])
                    .withFailMessage("${LOCAL.name} : '${database.active[LOCAL.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[DEV.name])
                    .withFailMessage("${DEV.name} : '${database.active[DEV.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[INT.name])
                    .withFailMessage("${INT.name} : '${database.active[INT.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[TEST.name])
                    .withFailMessage("${TEST.name} : '${database.active[TEST.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[QA.name])
                    .withFailMessage("${QA.name} : '${database.active[QA.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(database.active[PROD.name])
                    .withFailMessage("${PROD.name} : '${database.active[PROD.name]}'")
                    .isEqualTo(false)
            },
            {
                assertThat(
                    databasesClient.getAvailableDatabases(
                        isActive = true,
                        environment = LOCAL_TESTING.name
                    ).body()!!.size
                ).isEqualTo(initialDatabases!!.size + 1)
            }
        )
    }

    open fun testDatabaseRemoval() {
        //Given
        databasesClient.createNewDatabase("MS-PEOPLE")
        val initialDatabases = databasesClient.getAvailableDatabases().body()
        val initialDatabaseIds = initialDatabases!!
            .filter { db -> !db.name.startsWith("do-not-remove-on-") }
            .map { db -> db.id }
        assertThat(initialDatabaseIds).isNotEmpty()

        //When
        val toRemoveId = initialDatabaseIds[0]
        databasesClient.deleteDatabase(toRemoveId)

        //Then
        val databases = databasesClient.getAvailableDatabases().body()
        val databaseIds = databases!!.map { db -> db.id }
        assertThat(databaseIds).doesNotContain(toRemoveId)
    }

    open fun testCleanUpNotActiveDatabases() {
        //Given
        val initialDatabases = databasesClient.getAvailableDatabases().body()
        val initialDatabaseIds = initialDatabases!!.map { db -> db.id }
        //And
        databasesClient.createNewDatabase("MS-PEOPLE")
        databasesClient.createNewDatabase("MS-PEOPLE")
        val newDatabases = databasesClient.getAvailableDatabases().body()
        val newDatabaseIds = newDatabases!!.map { db -> db.id }.filter { id -> !initialDatabaseIds.contains(id) }
        assertThat(newDatabaseIds).isNotEmpty()
        //And
        assertThat(newDatabaseIds).contains(newDatabaseIds[0])
        assertThat(newDatabaseIds).contains(newDatabaseIds[1])

        //When
        databasesClient.cleanUpNotActiveDatabases(newDatabaseIds[0])

        //Then
        val databases = databasesClient.getAvailableDatabases().body()
        val databaseIds = databases!!.map { db -> db.id }.filter { id -> !initialDatabaseIds.contains(id) }
        assertThat(databaseIds).doesNotContain(newDatabaseIds[0])
        assertThat(databaseIds).doesNotContain(newDatabaseIds[1])
    }

    open fun testDatabaseRemovalWithWronglyFormattedDatabaseId() {
        //When
        val response = databasesClient.deleteDatabase("not-a-uuid")
        //Then
        assertThat(response.header("error-message")).isEqualTo("solutions.viae.databasemanagement.core.api.usecases.RemoveDatabaseUseCase-could not transform to database id: 'not-a-uuid', cause: Invalid UUID string: not-a-uuid")
    }

    open fun testDatabaseCreation() {
        //Given
        val initialDatabases = databasesClient.getAvailableDatabases().body()
        val initialDatabaseIds = initialDatabases!!.map { db -> db.id }

        //When
        val response = databasesClient.createNewDatabase("MS-PEOPLE")

        //Then
        val databasesV1 = databasesClient.getAvailableDatabases().body()
        val newDatabaseIds = databasesV1!!.map { db -> db.id }.filter { id: String -> !initialDatabaseIds.contains(id) }
        assertThat(newDatabaseIds.size).isEqualTo(1)
        assertThat(response.status.code).isEqualTo(200)
        assertThat(response.body()!!.second).isEqualTo(newDatabaseIds[0])
    }

    open fun testDatabaseCreationWithUnknownDatabaseType() {
        //When
        val response = singleJsonFieldBasedDatabasesControllerClient.createNewDatabase("should-not-be-known")
        //Then
        assertThat(response.header("error-message")).isEqualTo("solutions.viae.databasemanagement.core.api.usecases.CreateNewDatabaseUseCase-could not transform 'should-not-be-known' to a database prefix. Available application names are [MS-NAMES, MS-PEOPLE, MS-EMAIL], cause: prefix not found")
    }

    open fun testDatabaseCreationWithoutApplicationName() {
        //When
        val response = singleJsonFieldBasedDatabasesControllerClient.createNewDatabase(null)
        //Then
        assertThat(response.header("error-message")).isEqualTo("solutions.viae.databasemanagement.core.api.usecases.CreateNewDatabaseUseCase-application name is required")
    }

    open fun testDatabaseListing() {
        //Then
        assertThat(databasesClient.getAvailableDatabases().body()).isNotNull
    }
}
