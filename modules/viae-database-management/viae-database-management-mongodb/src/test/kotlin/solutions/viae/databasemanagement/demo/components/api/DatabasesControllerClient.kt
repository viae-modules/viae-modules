package solutions.viae.databasemanagement.demo.components.api

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.annotation.Client
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.api.usecases.GetAvailableDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.GetDatabaseByIdUseCase
import solutions.viae.databasemanagement.demo.components.config.SingleJsonField
import javax.inject.Singleton

@Client(value = "/api/databases/v0.1", configuration = Config::class, errorType = Unit::class)
interface DatabasesControllerClient {

    @Get
    fun getAvailableDatabases(
        @QueryValue("isActive") isActive: Boolean? = false,
        @QueryValue("environment") environment: String? = Environment.LOCAL_TESTING.name
    ): HttpResponse<GetAvailableDatabasesUseCase.DatabaseResources>

    @Get(value = "/{databaseId}")
    fun getDatabase(databaseId: String?): HttpResponse<GetDatabaseByIdUseCase.DatabaseResource>

    @Post
    fun createNewDatabase(@QueryValue("applicationName") applicationName: String?): HttpResponse<SingleJsonField>

    @Post(value = "/{databaseId}")
    @Header(name = "command", value = "MARK_DATABASE_ACTIVE")
    fun markDatabaseActive(
        @QueryValue("environment") environment: String?,
        databaseId: String?,
        @QueryValue("switchIfExists") switchIfExists: Boolean? = null
    ): HttpResponse<Unit>

    @Post(value = "/{databaseId}")
    @Header(name = "command", value = "MARK_DATABASE_INACTIVE")
    fun markDatabaseInactive(
        @QueryValue("environment") environment: String?,
        databaseId: String
    ): HttpResponse<Unit>

    @Post(value = "/{databaseId}")
    @Header(name = "command", value = "CLEAN_UP_NOT_ACTIVE_DATABASES")
    fun cleanUpNotActiveDatabases(
        databaseId: String?
    ): HttpResponse<Unit>


    @Delete(value = "/{databaseId}")
    fun deleteDatabase(databaseId: String?): HttpResponse<Unit>
}

@Client(value = "/api/databases/v0.1", configuration = Config::class, errorType = SingleJsonField::class)
interface SingleJsonFieldBasedDatabasesControllerClient {
    @Post
    fun createNewDatabase(@QueryValue("applicationName") applicationName: String?): HttpResponse<SingleJsonField>
}

@Client(value = "/api/databases/v0.1", configuration = Config::class, errorType = GetAvailableDatabasesUseCase.DatabaseResources::class)
interface ListBasedDatabasesControllerClient {
    @Get
    fun getAvailableDatabases(
        @QueryValue("isActive") isActive: Boolean? = false,
        @QueryValue("environment") environment: String? = Environment.LOCAL_TESTING.name
    ): HttpResponse<GetAvailableDatabasesUseCase.DatabaseResources>
}

@Singleton
open class Config : HttpClientConfiguration() {
    init {
        isExceptionOnErrorStatus = false
    }

    override fun getConnectionPoolConfiguration(): ConnectionPoolConfiguration {
        return ConnectionPoolConfiguration()
    }

}
