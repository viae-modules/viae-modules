package solutions.viae.databasemanagement.demo.components.api

import com.mongodb.client.MongoClient
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponse.noContent
import io.micronaut.http.HttpResponse.ok
import io.micronaut.http.MediaType.APPLICATION_JSON
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import org.bson.Document
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import javax.inject.Inject

@Controller("/api/data/email")
class EmailDataMicroService @Inject constructor(
    private val readOnlyDatabasesRepository: ReadOnlyDatabasesRepository,
    private val mongoClient: MongoClient
){

    @Get("/", produces = [APPLICATION_JSON])
    fun getAllData(): HttpResponse<List<String>> {
        val activeDatabaseId = readOnlyDatabasesRepository.getActiveDatabaseId(Environment.LOCAL, APP_NAME)
        if(activeDatabaseId.hasNotFoundViolation()){
            return ok(emptyList())
        }
        val dbName = readOnlyDatabasesRepository.get(activeDatabaseId.data).data.name
        return ok(
            mongoClient.getDatabase(dbName).getCollection("email-data-collection").find().toList()
                .map { it.getString("email") }
        )
    }

    @Post("/", produces = [APPLICATION_JSON], consumes = [APPLICATION_JSON])
    fun createData(@QueryValue databaseId: String, @Body body: String): HttpResponse<Nothing> {
        val dbName = readOnlyDatabasesRepository.get(DatabaseId.fromString(databaseId)).data.name
        mongoClient.getDatabase(dbName).getCollection("email-data-collection").insertOne(
            Document("email", body)
        )
        return noContent()
    }

    companion object {
        val APP_NAME = "MS-EMAIL"
    }
}
