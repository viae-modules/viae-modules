package solutions.viae.databasemanagement.demo.components.config

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.service.BadRequestResponseType
import solutions.viae.coreutils.service.HttpResponseService
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.service.OkResponseType
import solutions.viae.coreutils.service.ResponseType
import solutions.viae.coreutils.service.UnAuthorizedResponseType
import solutions.viae.coreutils.service.ViaeHttpResponse
import solutions.viae.databasemanagement.core.api.factories.DatabaseManagementUseCaseFactory
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import solutions.viae.databasemanagement.demo.components.services.DefaultLogger
import solutions.viae.databasemanagement.mongodb.config.MongoDbBasedDatabaseManagementConfig
import solutions.viae.databasemanagement.mongodb.config.MongoDbConnectionData
import javax.inject.Singleton

fun <RDT> ViaeHttpResponse<RDT>.present(logger: Logger): HttpResponse<RDT> {
    return when (type) {
        ResponseType.UNAUTHORIZED -> {
            val response = this as UnAuthorizedResponseType<RDT>
            HttpResponse.unauthorized<RDT>().header(response.errorMessageHeader.first, response.errorMessageHeader.second)
        }
        ResponseType.BAD_REQUEST -> {
            val response = this as BadRequestResponseType<RDT>
            logger.error("Bad Request: ${response.errors.joinToString(", ")}")
            val result = HttpResponse.badRequest<RDT>()
            result.header("error-message", response.errors.joinToString(";") { "${it.source}-${it.message}" })
            result
        }
        ResponseType.OK -> {
            val response = this as OkResponseType<RDT>
            if (response.data != null) {
                HttpResponse.ok(response.data)
            } else {
                HttpResponse.noContent()
            }
        }
        ResponseType.OK_NO_CONTENT -> {
            HttpResponse.noContent()
        }
    }
}

class SingleJsonField(val first: String, val second: String) {
    /**
     * Returns string representation of the [Pair] including its [first] and [second] values.
     */
    public override fun toString(): String = "($first, $second)"
}

@Factory
class Configuration(
    @Value("\${viae.mongo.server-addresses}")
    val mongoServerAddresses: List<String>?,
    @Value("\${viae.mongo.user}")
    val mongoUser: String?,
    @Value("\${viae.mongo.password}")
    val mongoPassword: String?,
    @Value("\${viae.mongo.source}")
    val mongoSource: String?,
    @Value("\${viae.mongo.connection-string}")
    val connectionString: String?
) {
    private val config: MongoDbBasedDatabaseManagementConfig =
        MongoDbBasedDatabaseManagementConfig(
            mongoDbConnectionData = MongoDbConnectionData.getConnectionDataFor(
                mongoServerAddresses = mongoServerAddresses,
                mongoUserName = mongoUser,
                mongoPassword = mongoPassword,
                mongoSource = mongoSource,
                connectionString = connectionString

            ).orIllegalState()!!,
            logger = DefaultLogger(),
            environment = Environment.LOCAL,
            applicationDatabasePrefixRegistry = mapOf(
                "MS-NAMES" to "ms-names",
                "MS-PEOPLE" to "ms-people",
                "MS-EMAIL" to "ms-email"
            )
        )

    @Singleton
    fun databaseManagementUseCaseFactory(): DatabaseManagementUseCaseFactory {
        return config.initDatabaseManagementUseCaseFactory()
    }

    @Singleton
    fun databaseManagementReadOnlyDatabasesRepository(): ReadOnlyDatabasesRepository {
        return config.initReadOnlyDatabaseManagementRepository()
    }

    @Singleton
    fun databaseManagementCurdDatabasesRepository(): CrudDatabasesRepository {
        return config.initCrudDatabaseManagementRepository()
    }

    @Singleton
    fun httpResponseService(): HttpResponseService {
        return object : HttpResponseService {}
    }

    @Singleton
    fun mongoClient(): MongoClient {
        return MongoClients.create(System.getenv("MONGODB_HOST") ?: "mongodb://localhost:27017")
    }

}
