package solutions.viae.databasemanagement.demo.components

import io.micronaut.runtime.Micronaut
import solutions.viae.databasemanagement.demo.components.api.EmailDataMicroService

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages(EmailDataMicroService::class.java.packageName)
            .mainClass(Application.javaClass)
            .start()
    }
}
