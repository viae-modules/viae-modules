package solutions.viae.databasemanagement.demo

import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import solutions.viae.coreutils.domain.Environment
import solutions.viae.coreutils.domain.Environment.LOCAL
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import solutions.viae.databasemanagement.demo.components.Application
import solutions.viae.databasemanagement.demo.components.api.DatabasesControllerClient
import solutions.viae.databasemanagement.demo.components.api.EmailDataMicroService
import solutions.viae.databasemanagement.demo.components.api.NameDataMicroService
import solutions.viae.databasemanagement.demo.components.api.PersonDataMicroService
import javax.inject.Inject

@MicronautTest(
    application = Application::class
)
class DatabaseManagementDemoTest {

    @Inject
    private lateinit var dbClient: DatabasesControllerClient

    @Inject
    private lateinit var emailDataMicroService: EmailDataMicroService

    @Inject
    private lateinit var nameDataMicroService: NameDataMicroService

    @Inject
    private lateinit var personDataMicroService: PersonDataMicroService

    @Inject
    private lateinit var crudDatabasesRepository: CrudDatabasesRepository

    @BeforeEach
    fun cleanAlreadyActiveDatabases() {
        crudDatabasesRepository.getAvailableDatabases(LOCAL, true).data.forEach {
            crudDatabasesRepository.markDatabaseInactive(it.id, LOCAL)
        }
    }

    @Test
    fun doTestWithMultipleDatabases() {
        //Given - data to query on via different microservices
        val people = listOf(
            Person("Maarten", "Vandeperre", "maarten.vandeperre@test.com"),
            Person("Pieter", "Vandeperre", "pieter.vandeperre@test.com"),
            Person("Jolien", "Vereman", "jolien.vereman@test.com")
        )
        //And - create databases
        val dbNamesId = dbClient.createNewDatabase(NameDataMicroService.APP_NAME).body()!!.second
        val dbEmailId1 = dbClient.createNewDatabase(EmailDataMicroService.APP_NAME).body()!!.second
        val dbEmailId2 = dbClient.createNewDatabase(EmailDataMicroService.APP_NAME).body()!!.second
        val dbPeopleId = dbClient.createNewDatabase(PersonDataMicroService.APP_NAME).body()!!.second
        //And - fill the databases with their data
        people.forEach { emailDataMicroService.createData(dbEmailId1, it.email) }
        people.forEach { emailDataMicroService.createData(dbEmailId2, "v2-${it.email}") }
        people.forEach { nameDataMicroService.createData(dbNamesId, "${it.firstName} ${it.lastName}") }
        people.forEach { personDataMicroService.createData(dbPeopleId, "${it.firstName} ${it.lastName}: ${it.email}") }

        //Then - getting data when none of the databases is active should return no data
        assertThat(nameDataMicroService.getAllDataOnActiveDb().body()).isEmpty()
        assertThat(nameDataMicroService.getAllData(dbNamesId).body()).isEqualTo(listOf("Maarten Vandeperre", "Pieter Vandeperre", "Jolien Vereman"))
        assertThat(personDataMicroService.getAllData().body()).isEmpty()
        assertThat(emailDataMicroService.getAllData().body()).isEmpty()

        //When marking the names data database as active on another environment than LOCAL
        dbClient.markDatabaseActive(Environment.QA.name, dbNamesId, true)

        //Then - getting data should return no data
        assertThat(nameDataMicroService.getAllDataOnActiveDb().body()).isEmpty()
        assertThat(nameDataMicroService.getAllData(dbNamesId).body()).isEqualTo(listOf("Maarten Vandeperre", "Pieter Vandeperre", "Jolien Vereman"))
        assertThat(personDataMicroService.getAllData().body()).isEmpty()
        assertThat(emailDataMicroService.getAllData().body()).isEmpty()

        //When marking the first email data database and the other databases as active on LOCAL environment
        assertThat(dbClient.markDatabaseActive(LOCAL.name, dbEmailId1, false).status().code).isEqualTo(204)
        assertThat(dbClient.markDatabaseActive(LOCAL.name, dbNamesId, false).status().code).isEqualTo(204)
        assertThat(dbClient.markDatabaseActive(LOCAL.name, dbPeopleId, false).status().code).isEqualTo(204)

        //Then - getting data should return data
        assertThat(nameDataMicroService.getAllDataOnActiveDb().body()).isEqualTo(listOf("Maarten Vandeperre", "Pieter Vandeperre", "Jolien Vereman"))
        assertThat(nameDataMicroService.getAllData(dbNamesId).body()).isEqualTo(listOf("Maarten Vandeperre", "Pieter Vandeperre", "Jolien Vereman"))
        assertThat(personDataMicroService.getAllData().body()).isEqualTo(
            listOf(
                "Maarten Vandeperre: maarten.vandeperre@test.com",
                "Pieter Vandeperre: pieter.vandeperre@test.com",
                "Jolien Vereman: jolien.vereman@test.com"
            )
        )
        assertThat(emailDataMicroService.getAllData().body()).isEqualTo(
            listOf(
                "maarten.vandeperre@test.com",
                "pieter.vandeperre@test.com",
                "jolien.vereman@test.com"
            )
        )

        //When trying to make a second database active for the same application and environment, it should give an exception
        val markDatabaseActiveResponse = dbClient.markDatabaseActive(LOCAL.name, dbEmailId2, false)
        assertThat(markDatabaseActiveResponse.status().code).isEqualTo(400)
        assertThat(markDatabaseActiveResponse.header("error-message")).isEqualTo("" +
                "solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase-pre-validation failed, " +
                "cause: can't get data: there are exceptions: " +
                "ErrorMessage(classSource=class solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase, " +
                "message=for the given environment (LOCAL) there is/are already active databases: [$dbEmailId1], " +
                "errorGroup=pre-validation, errorType=FUNCTIONAL)" +
                "")

        //When marking the second email data database as active (switch) and the others inactive on LOCAL environment
        assertThat(dbClient.markDatabaseActive(LOCAL.name, dbEmailId2, true).status().code).isEqualTo(204)
        assertThat(dbClient.markDatabaseInactive(LOCAL.name, dbNamesId).status().code).isEqualTo(204)
        assertThat(dbClient.markDatabaseInactive(LOCAL.name, dbPeopleId).status().code).isEqualTo(204)

        //Then - getting data should only return data for emails (i.e. v2)
        assertThat(nameDataMicroService.getAllDataOnActiveDb().body()).isEmpty()
        assertThat(nameDataMicroService.getAllData(dbNamesId).body()).isEqualTo(listOf("Maarten Vandeperre", "Pieter Vandeperre", "Jolien Vereman"))
        assertThat(personDataMicroService.getAllData().body()).isEmpty()
        assertThat(emailDataMicroService.getAllData().body()).isEqualTo(
            listOf(
                "v2-maarten.vandeperre@test.com",
                "v2-pieter.vandeperre@test.com",
                "v2-jolien.vereman@test.com"
            )
        )
    }
}

data class Person(
    val firstName: String,
    val lastName: String,
    val email: String
)
