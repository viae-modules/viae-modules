package solutions.viae.databasemanagement.demo.components.api

import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.HttpResponseService
import solutions.viae.databasemanagement.core.api.factories.DatabaseManagementUseCaseFactory
import solutions.viae.databasemanagement.core.api.usecases.CleanUpNotActiveDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.CreateNewDatabaseUseCase
import solutions.viae.databasemanagement.core.api.usecases.GetAvailableDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.GetDatabaseByIdUseCase
import solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseInactiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.RemoveDatabaseUseCase
import solutions.viae.databasemanagement.demo.components.config.SingleJsonField
import solutions.viae.databasemanagement.demo.components.config.present
import solutions.viae.databasemanagement.demo.components.services.DefaultLogger
import javax.inject.Inject

@Controller(
    value = "/api/databases/v0.1",
    consumes = [MediaType.APPLICATION_JSON],
    produces = [MediaType.APPLICATION_JSON]
)
class DatabasesController {
    @Inject
    private lateinit var useCaseFactory: DatabaseManagementUseCaseFactory

    @Inject
    private lateinit var httpResponseService: HttpResponseService
    private val logger = DefaultLogger()

    @Get
    fun getAvailableDatabases(
        @QueryValue("isActive") isActive: Boolean?,
        @QueryValue("environment") environment: String?
    ): HttpResponse<GetAvailableDatabasesUseCase.DatabaseResources> {
        return httpResponseService.httpify(
            useCaseFactory.getAvailableDatabasesUseCase.getActiveDatabases(
                GetAvailableDatabasesUseCase.Request(
                    logger = logger,
                    environment = environment,
                    isActive = isActive
                )
            ).map { it!!.databases }
        ).present(logger)
    }

    @Get(value = "/{databaseId}")
    fun getDetail(@PathVariable(value = "databaseId") id: String?): HttpResponse<GetDatabaseByIdUseCase.DatabaseResource> {
        return httpResponseService.httpify(
            useCaseFactory.getDatabaseByIdUseCase.getDatabase(
                GetDatabaseByIdUseCase.Request(
                    logger = logger,
                    databaseId = id
                )
            ).map { it!!.database }
        ).present(logger)
    }

    @Post
    fun createNewDatabase(
        @QueryValue(value = "applicationName") applicationName: String?
    ): HttpResponse<SingleJsonField> {
        return httpResponseService.httpify(
            useCaseFactory.createNewDatabaseUseCase.createDatabase(
                CreateNewDatabaseUseCase.Request(
                    logger = logger,
                    applicationName = applicationName
                )
            ).map { SingleJsonField("databaseId", it!!.databaseId.asString()) }
        ).present(logger)
    }

    @Post(uri = "/{databaseId}")
    fun executeCommand(
        @PathVariable("databaseId") databaseId: String?,
        @QueryValue("environment") environment: String?,
        @QueryValue("switchIfExists") switchIfExists: Boolean? = null,
        headers: HttpHeaders
    ): HttpResponse<Nothing?> {
        return httpResponseService.httpify(
            when (val command = headers.get("command")?.toUpperCase()) {
                Commands.MARK_DATABASE_ACTIVE.name ->
                    useCaseFactory.markDatabaseActiveUseCase.markDatabaseActive(
                        MarkDatabaseActiveUseCase.Request(
                            logger = logger,
                            databaseId = databaseId,
                            environment = environment,
                            switchIfExists = switchIfExists ?: false
                        )
                    )
                Commands.MARK_DATABASE_INACTIVE.name ->
                    useCaseFactory.markDatabaseInactiveUseCase.markDatabaseInactive(
                        MarkDatabaseInactiveUseCase.Request(
                            logger = logger,
                            databaseId = databaseId,
                            environment = environment
                        )
                    )
                Commands.CLEAN_UP_NOT_ACTIVE_DATABASES.name ->
                    useCaseFactory.cleanUpNotActiveDatabasesUseCase.cleanUp(
                        CleanUpNotActiveDatabasesUseCase.Request(
                            logger = logger
                        )
                    )
                else -> Response.failure(
                    DatabasesController::class,
                    "'command' $command is not supported, possible values are: ${Commands.values().map { v -> v.name }}"
                )
            }.map { null }
        ).present(logger)
    }

    @Delete(value = "/{databaseId}")
    fun deleteDatabase(@PathVariable(value = "databaseId") databaseId: String?): HttpResponse<Nothing?> {
        return httpResponseService.httpify(
            useCaseFactory.removeDatabaseUseCase.removeDatabase(
                RemoveDatabaseUseCase.Request(
                    logger = logger,
                    databaseId = databaseId
                )
            ).map { null }
        ).present(logger)
    }

    enum class Commands {
        MARK_DATABASE_ACTIVE, MARK_DATABASE_INACTIVE, CLEAN_UP_NOT_ACTIVE_DATABASES
    }
}
