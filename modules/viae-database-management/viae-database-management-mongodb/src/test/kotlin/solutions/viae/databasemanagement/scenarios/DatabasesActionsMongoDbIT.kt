package solutions.viae.databasemanagement.scenarios

import io.micronaut.retry.annotation.Retryable
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import solutions.viae.databasemanagement.demo.components.Application

@MicronautTest(
    application = Application::class
)
@Retryable
open class DatabasesActionsMongoDbIT : DatabasesActionsParent() {

    @BeforeEach
    override fun setupFreshFixture() {
        super.setupFreshFixture()
    }

    @Test
    override fun testCleanUpNotActiveDatabases() {
        super.testCleanUpNotActiveDatabases()
    }

    @Test
    override fun testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironmentWithSwitchIfExistsParameter() {
        super.testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironmentWithSwitchIfExistsParameter()
    }

    @Test
    override fun testGetAllActiveDatabasesForActiveVsNonActive() {
        super.testGetAllActiveDatabasesForActiveVsNonActive()
    }

    @Test
    override fun testMarkInactiveWithoutEnvironment() {
        super.testMarkInactiveWithoutEnvironment()
    }

    @Test
    override fun testMarkInactiveWithInvalidEnvironmentAndWithInvalidDatabaseId() {
        super.testMarkInactiveWithInvalidEnvironmentAndWithInvalidDatabaseId()
    }

    @Test
    override fun testGetAllActiveDatabasesWithoutEnvironment() {
        super.testGetAllActiveDatabasesWithoutEnvironment()
    }

    @Test
    override fun testGetAllActiveDatabasesForEnvironmentNotKnown() {
        super.testGetAllActiveDatabasesForEnvironmentNotKnown()
    }

    @Test
    override fun testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironment() {
        super.testMarkingADatabaseActiveWhileThereIsAlreadyAnActiveOneForThatEnvironment()
    }

    @Test
    override fun testMarkingADatabaseInactive() {
        super.testMarkingADatabaseInactive()
    }

    @Test
    override fun testMarkingDifferentDatabasesActiveForDifferentEnvironments() {
        super.testMarkingDifferentDatabasesActiveForDifferentEnvironments()
    }

    @Test
    override fun testMarkingADatabaseActive() {
        super.testMarkingADatabaseActive()
    }

    @Test
    override fun testDatabaseRemoval() {
        super.testDatabaseRemoval()
    }

    @Test
    override fun testDatabaseRemovalWithWronglyFormattedDatabaseId() {
        super.testDatabaseRemovalWithWronglyFormattedDatabaseId()
    }

    @Test
    override fun testDatabaseCreation() {
        super.testDatabaseCreation()
    }

    @Test
    override fun testDatabaseCreationWithUnknownDatabaseType() {
        super.testDatabaseCreationWithUnknownDatabaseType()
    }

    @Test
    override fun testDatabaseCreationWithoutApplicationName() {
        super.testDatabaseCreationWithoutApplicationName()
    }

    @Test
    override fun testDatabaseListing() {
        super.testDatabaseListing()
    }
}
