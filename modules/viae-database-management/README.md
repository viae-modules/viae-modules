#VIAE Request Log

Kotlin based library to do request logging in order to know who was doing an action, how long the action took, 
if the action was successful and, if needed, which logging was associated with this action.

Next to the in application (i.e. back-end) request logs we'll expose an enpoint 
that can be used by the front-end application to log requests, flow logs and timings as well

## Example
Micronaut example can be found in [OAuthValidationDemoTest](viae-database-management-mongodb/src/test/kotlin/solutions/viae/oauth2validation/demo/OAuthValidationDemoTest.kt).
Required controller - use case configuration can be found in the folder "test / demo / components".

**Important:** make sure that docker-compose is running: This will start up an in-memory keycloak 
instance. Command (from within the viae-oauth2.0-validator root folder): `docker-compose up`.
A keycloak instance will be runnen on port 8081. You can browse to http://localhost:8081/auth 
to connect to the admin screen. User name - password: admin - admin (please make this more secure 
when using this in a production environment).

**Usage: bean configuration**
```kotlin
    @Singleton
    fun userContextHolder(): UserContextHolder {
        return UserContextHolder(OAuth2ServerConfig(
            "http://localhost:8081/auth",
            "test"
        )) {
            //... extract JWT token from header (i.e. authorization header without bearer prefix)
        }
    }
```

**Usage: usecase**
```kotlin
class UseCaseWithSecurityForAllPermissions(
    private val userContextHolder: UserContextHolder
) {

    fun execute(request: String): String {
        if (userContextHolder.userContext.hasAllThePermissions("READ", "READ_SPECIAL")) {
            return "this is the secured content for $request"
        } else {
            throw IllegalStateException("not enough permissions")
        }
    }
}
```
