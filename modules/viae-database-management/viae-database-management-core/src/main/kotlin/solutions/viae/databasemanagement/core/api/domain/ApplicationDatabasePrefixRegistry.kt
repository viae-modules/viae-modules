package solutions.viae.databasemanagement.core.api.domain

typealias ApplicationDatabasePrefixRegistry = Map<String, String>
