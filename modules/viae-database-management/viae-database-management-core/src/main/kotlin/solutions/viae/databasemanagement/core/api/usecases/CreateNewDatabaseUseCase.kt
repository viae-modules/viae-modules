package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.utils.runValid
import solutions.viae.databasemanagement.core.api.domain.ApplicationDatabasePrefixRegistry
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.impl.api.CreateDatabaseData
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import java.lang.IllegalStateException
import java.util.*

interface CreateNewDatabaseUseCase {
    fun createDatabase(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val applicationName: String?
    )

    class ResponseData(
        val databaseId: DatabaseId
    )
}

internal class DefaultCreateNewDatabaseUseCase(
    private val crudDatabasesRepository: CrudDatabasesRepository,
    private val applicationDatabasePrefixRegistry: ApplicationDatabasePrefixRegistry
) : CreateNewDatabaseUseCase {

    override fun createDatabase(request: CreateNewDatabaseUseCase.Request): Response<CreateNewDatabaseUseCase.ResponseData> {
        return runValid {
            with(it) {
                isNotBlank(request.applicationName, error("application name is required", "null-check"))

                val prefix = and("null-check") {
                    tryTransform(
                        { applicationDatabasePrefixRegistry[request.applicationName!!.toUpperCase()]?: throw IllegalStateException("prefix not found") },
                        error("" +
                                "could not transform '${request.applicationName}' to a database prefix. " +
                                "Available application names are ${applicationDatabasePrefixRegistry.keys}", "input-validation" +
                                "")
                    )
                }

                val databaseId = DatabaseId(UUID.randomUUID())
                and("null-check", "input-validation") {
                    tryTransformFromResponse(
                        {
                            crudDatabasesRepository.createDatabase(
                                CreateDatabaseData(
                                    id = databaseId,
                                    namePrefix = prefix!!,
                                    createdTs = DateUtils.now(),
                                    isActive = noActiveEnvironments(),
                                    application = request.applicationName!!.toUpperCase()
                                )
                            )
                        },
                        error("could not create database", "do-action")
                    )
                }
                respond { CreateNewDatabaseUseCase.ResponseData(databaseId) }
            }
        }
    }

    private fun noActiveEnvironments(): Map<Environment, Boolean> {
        val result = mutableMapOf<Environment, Boolean>()
        Environment.values().forEach { v -> result[v] = false }
        return result
    }

    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(CreateNewDatabaseUseCase::class, message, errorGroup)
        }
    }
}
