package solutions.viae.databasemanagement.core.api.domain

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import java.time.ZonedDateTime

data class Database(
    val id: DatabaseId,
    val name: DatabaseName,
    val application: ApplicationName,
    val createdTs: ZonedDateTime,
    val isActive: Map<Environment, Boolean>
)
