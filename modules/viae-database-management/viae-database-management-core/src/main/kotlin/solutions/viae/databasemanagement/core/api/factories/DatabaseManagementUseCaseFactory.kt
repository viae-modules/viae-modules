package solutions.viae.databasemanagement.core.api.factories

import solutions.viae.databasemanagement.core.api.domain.ApplicationDatabasePrefixRegistry
import solutions.viae.databasemanagement.core.api.usecases.CleanUpNotActiveDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.CreateNewDatabaseUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultCleanUpNotActiveDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultCreateNewDatabaseUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultGetAvailableDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultGetDatabaseByIdUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultMarkDatabaseActiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultMarkDatabaseInactiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.DefaultRemoveDatabaseUseCase
import solutions.viae.databasemanagement.core.api.usecases.GetAvailableDatabasesUseCase
import solutions.viae.databasemanagement.core.api.usecases.GetDatabaseByIdUseCase
import solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseActiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.MarkDatabaseInactiveUseCase
import solutions.viae.databasemanagement.core.api.usecases.RemoveDatabaseUseCase
import solutions.viae.databasemanagement.core.impl.factories.DatabaseManagementCrudRepositoryFactory

class DatabaseManagementUseCaseFactory(
    private val readOnlyRepositoryFactory: DatabaseManagementRepositoryFactory,
    private val crudRepositoryFactory: DatabaseManagementCrudRepositoryFactory,
    private val applicationDatabasePrefixRegistry: ApplicationDatabasePrefixRegistry
) {
    val createNewDatabaseUseCase: CreateNewDatabaseUseCase
        get() = DefaultCreateNewDatabaseUseCase(
            crudRepositoryFactory.crudDatabasesRepository,
            applicationDatabasePrefixRegistry
        )

    val markDatabaseActiveUseCase: MarkDatabaseActiveUseCase
        get() = DefaultMarkDatabaseActiveUseCase(crudRepositoryFactory.crudDatabasesRepository)

    val markDatabaseInactiveUseCase: MarkDatabaseInactiveUseCase
        get() = DefaultMarkDatabaseInactiveUseCase(crudRepositoryFactory.crudDatabasesRepository)

    val removeDatabaseUseCase: RemoveDatabaseUseCase
        get() = DefaultRemoveDatabaseUseCase(crudRepositoryFactory.crudDatabasesRepository)

    val cleanUpNotActiveDatabasesUseCase: CleanUpNotActiveDatabasesUseCase
        get() = DefaultCleanUpNotActiveDatabasesUseCase(crudRepositoryFactory.crudDatabasesRepository)

    val getAvailableDatabasesUseCase: GetAvailableDatabasesUseCase
        get() = DefaultGetAvailableDatabasesUseCase(crudRepositoryFactory.crudDatabasesRepository)

    val getDatabaseByIdUseCase: GetDatabaseByIdUseCase
        get() = DefaultGetDatabaseByIdUseCase(crudRepositoryFactory.crudDatabasesRepository)
}
