package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.utils.runValid
import solutions.viae.databasemanagement.core.api.domain.Database
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import solutions.viae.databasemanagement.core.api.usecases.GetAvailableDatabasesUseCase.DatabaseResource
import solutions.viae.databasemanagement.core.api.usecases.GetAvailableDatabasesUseCase.ResponseData

interface GetAvailableDatabasesUseCase {


    fun getActiveDatabases(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val isActive: Boolean?,
        val environment: String?
    )

    class ResponseData(
        val databases: DatabaseResources
    )

    interface DatabaseResources : List<DatabaseResource>

    class DatabaseResource(
        val id: String,
        val name: String,
        val created: String
    ) {
        companion object {
            fun fromDb(db: Database): DatabaseResource {
                return DatabaseResource(
                    id = db.id.asString(),
                    name = db.name,
                    created = DateUtils.toString(db.createdTs)
                )
            }
        }
    }
}

internal class DefaultGetAvailableDatabasesUseCase(
    private val readOnlyDatabasesRepository: ReadOnlyDatabasesRepository
) : GetAvailableDatabasesUseCase {
    override fun getActiveDatabases(request: GetAvailableDatabasesUseCase.Request): Response<ResponseData> {
        return runValid {
            with(it) {
                isNotBlank(request.environment, error("environment is required", "input-validation"))
                isNotNull(request.isActive, error("is active is required", "input-validation"))

                val environmentValue = and("input-validation") {
                    tryTransform(
                        { Environment.fromString(request.environment!!).data }, error("could not tranform enviroment '${request.environment}'", "transform")
                    )
                }
                val dbs = and("input-validation", "transform") {
                    tryTransform(
                        {
                            readOnlyDatabasesRepository.getAvailableDatabases(
                                environmentValue!!,
                                request.isActive!!
                            ).data.map { DatabaseResource.fromDb(it) } as GetAvailableDatabasesUseCase.DatabaseResources
                        },
                        error("could not get available databases", "do-action")
                    )
                }
                respond { ResponseData(dbs!!) }
            }
        }
    }


    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(GetAvailableDatabasesUseCase::class, message, errorGroup)
        }
    }
}
