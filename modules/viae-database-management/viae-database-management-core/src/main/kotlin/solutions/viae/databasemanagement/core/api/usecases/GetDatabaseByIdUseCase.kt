package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.utils.runValid
import solutions.viae.databasemanagement.core.api.domain.Database
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository
import solutions.viae.databasemanagement.core.api.usecases.GetDatabaseByIdUseCase.DatabaseResource
import solutions.viae.databasemanagement.core.api.usecases.GetDatabaseByIdUseCase.ResponseData
import java.util.*

interface GetDatabaseByIdUseCase {
    fun getDatabase(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val databaseId: String?
    )

    class ResponseData(
        val database: DatabaseResource
    )

    class DatabaseResource(
        val id: String,
        val name: String,
        val created: String,
        val active: Map<String, Boolean>
    ) {
        companion object {
            fun fromDb(db: Database): DatabaseResource {
                return DatabaseResource(
                    id = db.id.asString(),
                    name = db.name,
                    created = DateUtils.toString(db.createdTs),
                    active = db.isActive.map { it.key.name to it.value }.toMap()
                )
            }
        }
    }
}

internal class DefaultGetDatabaseByIdUseCase(
    private val readOnlyDatabasesRepository: ReadOnlyDatabasesRepository
) : GetDatabaseByIdUseCase {
    override fun getDatabase(request: GetDatabaseByIdUseCase.Request): Response<ResponseData> {
        return runValid {
            with(it) {
                val databaseId = tryTransform(
                    { DatabaseId(UUID.fromString(request.databaseId)) },
                    error("could not transform to database id: '${request.databaseId}'", "fetch-database-id")
                )
                val result = and("fetch-database-id") {
                    tryTransform(
                        { DatabaseResource.fromDb(readOnlyDatabasesRepository.get(databaseId!!).data) },
                        error("could not get read only database '${request.databaseId}'", "do-action")
                    )
                }
                respond { ResponseData(result!!) }
            }
        }
    }


    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(GetDatabaseByIdUseCase::class, message, errorGroup)
        }
    }
}
