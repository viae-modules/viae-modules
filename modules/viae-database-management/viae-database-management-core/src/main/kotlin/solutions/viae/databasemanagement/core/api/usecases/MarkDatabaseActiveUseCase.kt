package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.runValid
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import java.util.*

interface MarkDatabaseActiveUseCase {

    fun markDatabaseActive(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val databaseId: String?,
        val environment: String?,
        val switchIfExists: Boolean = false
    )

    class ResponseData
}

internal class DefaultMarkDatabaseActiveUseCase(private val crudDatabasesRepository: CrudDatabasesRepository) :
    MarkDatabaseActiveUseCase {

    override fun markDatabaseActive(request: MarkDatabaseActiveUseCase.Request): Response<MarkDatabaseActiveUseCase.ResponseData> {
        return runValid {
            with(it) {
                val databaseId = tryTransform(
                    { DatabaseId(UUID.fromString(request.databaseId)) },
                    error("could not transform to database id: '${request.databaseId}'", "transform-input")
                )

                val environment = tryTransform(
                    { Environment.fromString(request.environment!!).data },
                    error("could not transform environment: '${request.environment}'", "transform-input")
                )

                and("transform-input") {
                    tryTransform(
                        { request.switchIfExists || hasNotYetAnActiveDatabaseFor(databaseId!!, environment!!).data },
                        error("pre-validation failed", "pre-validation")
                    )
                }

                and("transform-input", "pre-validation") {
                    tryTransform(
                        {
                            if (request.switchIfExists == true) {
                                crudDatabasesRepository.getAvailableDatabases(environment!!, true)
                                    .map { dbs ->
                                        dbs!!.forEach { db ->
                                            crudDatabasesRepository.markDatabaseInactive(db.id, environment)
                                        }
                                    }
                            }
                            crudDatabasesRepository.markDatabaseActive(databaseId!!, environment!!)
                        },
                        error("", "do-action")
                    )
                }
                respond { MarkDatabaseActiveUseCase.ResponseData() }
            }
        }
    }

    private fun hasNotYetAnActiveDatabaseFor(databaseId: DatabaseId, environment: Environment): Response<Boolean> {
        val db = crudDatabasesRepository.get(databaseId)
        return if(db.hasNoErrors()){
            val activeDatabases = crudDatabasesRepository.getAvailableDatabases(environment, true).data
                .filter { it.id != databaseId && it.isActive.getValue(environment) && it.application == db.data.application }
                .map { it.id.asString() }

            if (activeDatabases.isEmpty()) {
                Response.success(true)
            } else {
                Response.failure(
                    error(
                        "for the given environment (${environment.name}) there is/are already active databases: [${activeDatabases.joinToString(",")}]",
                        "pre-validation"
                    )
                )
            }
        } else {
            db.map { false }
        }
    }

    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(MarkDatabaseActiveUseCase::class, message, errorGroup)
        }
    }
}
