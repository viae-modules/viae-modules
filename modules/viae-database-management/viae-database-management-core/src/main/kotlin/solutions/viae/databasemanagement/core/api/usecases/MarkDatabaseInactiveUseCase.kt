package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.runValid
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import java.util.*

interface MarkDatabaseInactiveUseCase {

    fun markDatabaseInactive(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val databaseId: String?,
        val environment: String?
    )

    class ResponseData
}

internal class DefaultMarkDatabaseInactiveUseCase(private val crudDatabasesRepository: CrudDatabasesRepository) :
    MarkDatabaseInactiveUseCase {

    override fun markDatabaseInactive(request: MarkDatabaseInactiveUseCase.Request): Response<MarkDatabaseInactiveUseCase.ResponseData> {
        return runValid {
            with(it) {
                val databaseId = tryTransform(
                    { DatabaseId(UUID.fromString(request.databaseId)) },
                    error("could not transform to database id: '${request.databaseId}'", "transform-input")
                )

                val environment = tryTransform(
                    { Environment.fromString(request.environment!!).data },
                    error("could not transform environment: '${request.environment}'", "transform-input")
                )

                and("transform-input") {
                    tryTransform(
                        { crudDatabasesRepository.markDatabaseInactive(databaseId!!, environment!!) },
                        error("could not mark database inactive", "do-action")
                    )
                }

                respond { MarkDatabaseInactiveUseCase.ResponseData() }
            }
        }
    }

    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(MarkDatabaseInactiveUseCase::class, message, errorGroup)
        }
    }
}
