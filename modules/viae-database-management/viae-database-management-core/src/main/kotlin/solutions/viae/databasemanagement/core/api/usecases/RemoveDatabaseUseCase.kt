package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.coreutils.utils.runValid
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository
import java.util.*

interface RemoveDatabaseUseCase {

    fun removeDatabase(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger,
        val databaseId: String?
    )

    class ResponseData
}

internal class DefaultRemoveDatabaseUseCase(private val crudDatabasesRepository: CrudDatabasesRepository) :
    RemoveDatabaseUseCase {

    override fun removeDatabase(request: RemoveDatabaseUseCase.Request): Response<RemoveDatabaseUseCase.ResponseData> {
        return runValid {
            with(it) {
                val databaseId = tryTransform(
                    { DatabaseId(UUID.fromString(request.databaseId)) },
                    error("could not transform to database id: '${request.databaseId}'", "get-db")
                )
                and("get-db") {
                    tryTransform(
                        { crudDatabasesRepository.removeDatabase(databaseId!!) },
                        error("could not remove database", "transform")
                    )
                }
                respond { RemoveDatabaseUseCase.ResponseData() }
            }
        }
    }

    companion object {
        fun error(message: String, errorGroup: String): ErrorMessage {
            return ErrorMessage(RemoveDatabaseUseCase::class, message, errorGroup)
        }
    }
}
