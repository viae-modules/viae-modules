package solutions.viae.databasemanagement.core.impl.factories

import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository

interface DatabaseManagementCrudRepositoryFactory {
    val crudDatabasesRepository: CrudDatabasesRepository
}
