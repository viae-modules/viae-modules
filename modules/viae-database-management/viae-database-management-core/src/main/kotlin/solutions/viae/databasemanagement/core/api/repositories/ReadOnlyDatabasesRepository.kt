package solutions.viae.databasemanagement.core.api.repositories

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.domain.ErrorMessage
import solutions.viae.coreutils.domain.ErrorType
import solutions.viae.coreutils.domain.ErrorType.FUNCTIONAL
import solutions.viae.coreutils.domain.ErrorType.NOT_FOUND
import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.utils.runValid
import solutions.viae.databasemanagement.core.api.domain.Database
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment

interface ReadOnlyDatabasesRepository {
    fun getActiveDatabaseId(environment: Environment, applicationName: ApplicationName): Response<DatabaseId> {
        return runValid {
            with(it) {
                val rawDatabaseId = tryTransform(
                    {
                        val response = getAvailableDatabases(environment, true, applicationName)
                        if (response.hasNoErrors() && response.data.size != 1) {
                            val dbNames = response.data.map { db -> db.name }.joinToString(",")
                            throw IllegalStateException("expected 1 active database for ${environment.name}, but got dbs '$dbNames'")
                        }
                        val result = response.map<String?> { dbs -> dbs!![0].id.asString() }
                        result.data ?: throw IllegalStateException("no active db in result")
                    },
                    error("could not get database id", "get-raw-db", NOT_FOUND)
                )
                val databaseId = and("get-raw-db") {
                    tryTransform(
                        { DatabaseId.fromString(rawDatabaseId!!) },
                        error("database id '$rawDatabaseId' is not a valid format", "get-db")
                    )
                }
                respond { databaseId!! }
            }
        }
    }

    fun getAvailableDatabases(environment: Environment, active: Boolean, applicationName: ApplicationName? = null): Response<List<Database>>
    fun get(databaseId: DatabaseId): Response<Database>

    companion object {
        fun error(message: String, errorGroup: String, errorType: ErrorType = FUNCTIONAL): ErrorMessage {
            return ErrorMessage(ReadOnlyDatabasesRepository::class, message, errorGroup, errorType)
        }
    }
}
