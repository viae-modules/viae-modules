package solutions.viae.databasemanagement.core.impl.repositories

import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import solutions.viae.databasemanagement.core.impl.api.CreateDatabaseData
import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository

interface CrudDatabasesRepository : ReadOnlyDatabasesRepository {
    fun createDatabase(database: CreateDatabaseData): Response<Unit>
    fun removeDatabase(databaseId: DatabaseId): Response<Unit>
    fun markDatabaseActive(databaseId: DatabaseId, environment: Environment): Response<Unit>
    fun markDatabaseInactive(databaseId: DatabaseId, environment: Environment): Response<Unit>
    fun cleanUpAllNoneActiveDatabases(): Response<Unit>
    //Indices
    fun ensureGeoLocationIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean>
    fun ensureAscendingIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean>
    fun ensureDescendingIndex(databaseId: DatabaseId, collectionName: String, indexName: String, vararg fieldNames: String): Response<Boolean>
}
