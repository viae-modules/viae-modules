package solutions.viae.databasemanagement.core.impl.api

import solutions.viae.coreutils.domain.ApplicationName
import solutions.viae.coreutils.utils.DateUtils
import solutions.viae.coreutils.domain.DatabaseId
import solutions.viae.coreutils.domain.Environment
import java.time.ZonedDateTime

data class CreateDatabaseData(
    val id: DatabaseId = DatabaseId(),
    val application: ApplicationName,
    private val namePrefix: String,
    val createdTs: ZonedDateTime = DateUtils.now(),
    val isActive: Map<Environment, Boolean> = Environment.values().map { it to false }.toMap()
) {
    val name: String
        get() = "$namePrefix-${DateUtils.toStringAndRemoveSpecialCharactersAndDotsFromDate(createdTs)}"
}
