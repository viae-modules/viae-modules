package solutions.viae.databasemanagement.core.api.usecases

import solutions.viae.coreutils.domain.Response
import solutions.viae.coreutils.service.Logger
import solutions.viae.databasemanagement.core.impl.repositories.CrudDatabasesRepository

interface CleanUpNotActiveDatabasesUseCase {

    fun cleanUp(request: Request): Response<ResponseData>

    class Request(
        val logger: Logger
    )

    class ResponseData
}

internal class DefaultCleanUpNotActiveDatabasesUseCase(private val crudDatabasesRepository: CrudDatabasesRepository) :
    CleanUpNotActiveDatabasesUseCase {

    override fun cleanUp(request: CleanUpNotActiveDatabasesUseCase.Request): Response<CleanUpNotActiveDatabasesUseCase.ResponseData> {
        return crudDatabasesRepository.cleanUpAllNoneActiveDatabases().map { CleanUpNotActiveDatabasesUseCase.ResponseData() }
    }
}
