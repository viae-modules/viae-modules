package solutions.viae.databasemanagement.core.api.factories

import solutions.viae.databasemanagement.core.api.repositories.ReadOnlyDatabasesRepository

interface DatabaseManagementRepositoryFactory {
    val readOnlyDatabasesRepository: ReadOnlyDatabasesRepository
}
