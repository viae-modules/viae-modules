import org.gradle.api.JavaVersion


object MainConfig {
    const val kotlinVersion = "1.4.10"
    const val kotlinxVersion = "0.20.0-1.3.70-eap-274-2"
    const val shadowPluginVersion = "5.1.0"
    const val detektVersion = "1.9.0"
    const val dependencyVersionsPluginVersion = "1.0.199"
    const val researchGateGradleReleasePluginVersion = "2.8.1"
    const val jvmTarget = "11"

    val javaSourceVersionCompatibility = JavaVersion.VERSION_11
    val javaTargetVersionCompatibility = JavaVersion.VERSION_11
}

object Libraries {
    private object Versions {
        //micronaut
        const val micronautVersion = "2.2.0"
        const val micronautConfigurationVersion = "1.5.3"
        const val micronautTestVersion = "2.3.0"
        //others
        const val swaggerAnnotations = "2.1.1"
        const val keycloak = "10.0.2"
        const val slf4jSimple = "1.7.30"
        const val kotlinJacksonVersion = "2.11.1"
        const val graphQlJavaVersion = "14.1"
        //common testing
        const val assertjVersion = "3.16.1"
        const val junitVersion = "5.6.2"
        const val junitPlatformVersion = "1.6.2"
        const val mockitoJunitJupiterVersion = "3.3.3"
        //database
        const val mongodbVersion = "4.0.4"
    }

    object Kotlinx {
        const val serializationRuntime = "org.jetbrains.kotlinx:kotlinx-serialization-runtime:${MainConfig.kotlinxVersion}"
    }

    object Micronaut {
        const val injectJava = "io.micronaut:micronaut-inject-java:${Versions.micronautVersion}"
        const val core = "io.micronaut:micronaut-core:${Versions.micronautVersion}"
        const val graphql = "io.micronaut.graphql:micronaut-graphql:${Versions.micronautVersion}"
        const val http = "io.micronaut:micronaut-http:${Versions.micronautVersion}"
        const val openApi = "io.micronaut.configuration:micronaut-openapi:${Versions.micronautConfigurationVersion}"
        const val swaggerAnnotation = "io.swagger.core.v3:swagger-annotations:${Versions.swaggerAnnotations}"
        const val httpClient = "io.micronaut:micronaut-http-client:${Versions.micronautVersion}"
        const val httpServerNetty = "io.micronaut:micronaut-http-server-netty:${Versions.micronautVersion}"
        const val runtime = "io.micronaut:micronaut-runtime:${Versions.micronautVersion}"
        const val kotlinRuntime = "io.micronaut.kotlin:micronaut-kotlin-runtime:${Versions.micronautVersion}"
        const val graphQl = "io.micronaut.graphql:micronaut-graphql:${Versions.micronautVersion}"
        const val testCore = "io.micronaut.test:micronaut-test-core:${Versions.micronautTestVersion}"
        const val testJunit5 = "io.micronaut.test:micronaut-test-junit5:${Versions.micronautTestVersion}"
    }

    object Test {
        const val mockitoJunitJupiter = "org.mockito:mockito-junit-jupiter:${Versions.mockitoJunitJupiterVersion}"
        const val junitJupiterApi = "org.junit.jupiter:junit-jupiter-api:${Versions.junitVersion}"
        const val junitJupiterParams = "org.junit.jupiter:junit-jupiter-params:${Versions.junitVersion}"

        const val junitJupiterEngine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junitVersion}"
        const val junitPlatformLauncher = "org.junit.platform:junit-platform-launcher:${Versions.junitPlatformVersion}"
        const val junitPlatformRunner = "org.junit.platform:junit-platform-runner:${Versions.junitPlatformVersion}"

        const val assertjCore = "org.assertj:assertj-core:${Versions.assertjVersion}"
    }

    object Database {
        const val mongodbDriverCore = "org.mongodb:mongodb-driver-core:${Versions.mongodbVersion}"
        const val mongodbDriverSync = "org.mongodb:mongodb-driver-sync:${Versions.mongodbVersion}"
    }

    object Keycloak {
        const val admin = "org.keycloak:keycloak-admin-client:${Versions.keycloak}"
    }

    object Logging {
        const val slf4jSimple = "org.slf4j:slf4j-simple:${Versions.slf4jSimple}"
    }

    object Jackson {
        const val kotlin = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.kotlinJacksonVersion}"
    }

    object GraphQl{
        const val graphQlJava = "com.graphql-java:graphql-java:${Versions.graphQlJavaVersion}"
    }
}
