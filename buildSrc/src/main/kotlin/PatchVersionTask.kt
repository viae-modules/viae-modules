import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.register
import java.io.File

open class PatchVersionTask : DefaultTask() {
    @TaskAction
    fun run() {
        println("patchVersion start")
        val oldVersion = File(".version").readText(Charsets.UTF_8)
        println("old version: $oldVersion")
        val groups = Regex("([0-9]+)\\.([0-9]+)\\.([0-9]+)").find(oldVersion)
        val major = groups!!.groups[1]!!.value
        val minor = groups.groups[2]!!.value
        val patch = Integer.parseInt(groups.groups[3]!!.value)  + 1
        val newVersion = "${major}.${minor}.${patch}-SNAPSHOT"
        println("new version: $newVersion")

        File(".version").writeBytes(newVersion.toByteArray(Charsets.UTF_8))
        println("patchVersion done")
    }
}

fun Project.withPatchVersionTask() =
    tasks.register("patchVersion", PatchVersionTask::class)
