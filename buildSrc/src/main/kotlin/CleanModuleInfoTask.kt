import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.register
import java.io.File
import java.nio.file.Files

open class CleanModuleInfoTask : DefaultTask() {
    @TaskAction
    fun run() {
        val projectDir = project.projectDir
        val javaModuleInfo = File("$projectDir/src/main/java/module-info.java")
        val backupFolder = File("$projectDir/backup")
        val backupModuleInfo = File("$backupFolder/module-info.java")
        println("clean $javaModuleInfo?")
        if (javaModuleInfo.exists()) {
            println("do clean $javaModuleInfo")
            if (backupFolder.exists()) {
                deleteDirectory(backupFolder)
            }
            Files.createDirectories(backupFolder.toPath())
            Files.copy(javaModuleInfo.toPath(), backupModuleInfo.toPath())
            javaModuleInfo.delete()
        }
    }

    fun deleteDirectory(dir: File): Boolean {
        if (dir.isDirectory) {
            val children = dir.listFiles()
            for (i in children!!.indices) {
                val success = deleteDirectory(children[i])
                if (!success) {
                    return false
                }
            }
        }
        return dir.delete()
    }
}

fun Project.withCleanModuleInfoTask() =
    tasks.register("cleanModuleInfo", CleanModuleInfoTask::class)
