import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.register
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


open class RenameProjectTask : DefaultTask() {
    @Input lateinit var basePackageName: String

    @TaskAction
    fun run() {
        println("Start rename project task")
        for (childLevel1: MutableMap.MutableEntry<String, Project> in project.childProjects.entries) {
            println("Handle level 1 child ${childLevel1.value.name}")
            rename("${childLevel1.value.name}/src/main/java/com/viae/competitorsapi")
            rename("${childLevel1.value.name}/src/main/kotlin/com/viae/competitorsapi")
            rename("${childLevel1.value.name}/src/test/java/com/viae/competitorsapi")
            rename("${childLevel1.value.name}/src/test/kotlin/com/viae/competitorsapi")
            for (childLevel2: MutableMap.MutableEntry<String, Project> in childLevel1.value.childProjects.entries) {
                println("Handle level 2 child ${childLevel1.value.name}/${childLevel2.value.name}")
                rename("${childLevel1.value.name}/${childLevel2.value.name}/src/main/java/com/viae/competitorsapi")
                rename("${childLevel1.value.name}/${childLevel2.value.name}/src/main/kotlin/com/viae/competitorsapi")
                rename("${childLevel1.value.name}/${childLevel2.value.name}/src/test/java/com/viae/competitorsapi")
                rename("${childLevel1.value.name}/${childLevel2.value.name}/src/test/kotlin/com/viae/competitorsapi")
                for (childLevel3: MutableMap.MutableEntry<String, Project> in childLevel2.value.childProjects.entries) {
                    println("Handle level 3 child ${childLevel1.value.name}/${childLevel2.value.name}/${childLevel3.value.name}")
                    rename("${childLevel1.value.name}/${childLevel2.value.name}/${childLevel3.value.name}/src/main/java/com/viae/competitorsapi")
                    rename("${childLevel1.value.name}/${childLevel2.value.name}/${childLevel3.value.name}/src/main/kotlin/com/viae/competitorsapi")
                    rename("${childLevel1.value.name}/${childLevel2.value.name}/${childLevel3.value.name}/src/test/java/com/viae/competitorsapi")
                    rename("${childLevel1.value.name}/${childLevel2.value.name}/${childLevel3.value.name}/src/test/kotlin/com/viae/competitorsapi")
                }
            }
        }
        println("End rename project task")

        listf("${project.rootDir.absolutePath}/application").map { f -> renameImports(f) }
        renameImports(Paths.get("${project.rootDir}/build.gradle.kts").toFile())
    }

    private fun renameImports(file: File) {
        if(!file.isDirectory){
            println("renaming imports for ${file.absolutePath}")
            val path = file.toPath()
            val charset = StandardCharsets.UTF_8

            var content = String(Files.readAllBytes(path), charset)
            content = content.replace(".competitorsapi.", ".${basePackageName}.")
            Files.write(path, content.toByteArray(charset))
        }
    }

    private fun listf(directoryName: String): List<File> {
        val directory = File(directoryName)

        val resultList = ArrayList<File>()

        // get all the files from a directory
        val fList = directory.listFiles()
        val files: List<File> = fList!!.toList()
        resultList.addAll(files)
        for (file in fList) {
            if (file.isFile) {
                println(file.absolutePath)
            } else if (file.isDirectory) {
                resultList.addAll(listf(file.absolutePath))
            }
        }
        //System.out.println(fList);
        return resultList
    }

    private fun rename(oldDirectory: String) {
        val path = Paths.get(project.rootDir.absolutePath, oldDirectory)
        if (path.toFile().exists() && path.toFile().isDirectory) {
            val newPath = path.toFile().absolutePath.replace("competitorsapi", basePackageName)
            path.toFile().renameTo(File(newPath))
        }
    }
}

/**
 * Declares a [RenameProjectTask] named `renameProject`.
 */
fun Project.withRenameProjectTask() =
    tasks.register("renameProject", RenameProjectTask::class) {
        basePackageName = checkNotNull(rootProject.properties.get("name"), { "rootProject.name not set" }).toString()
            .toLowerCase()
            .replace("-", "")
    }
