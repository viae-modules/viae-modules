import java.nio.file.Files
import java.nio.file.Paths

plugins {
    base
    kotlin("jvm") version MainConfig.kotlinVersion apply true
    kotlin("kapt") version MainConfig.kotlinVersion apply true
    kotlin("plugin.serialization") version MainConfig.kotlinVersion apply true
    jacoco
    id("io.gitlab.arturbosch.detekt").version(MainConfig.detektVersion)
    id("net.researchgate.release").version(MainConfig.researchGateGradleReleasePluginVersion)
    `maven-publish`
    signing
    id("name.remal.check-dependency-updates") version MainConfig.dependencyVersionsPluginVersion
}

buildscript {
    val kotlinVersion by extra(MainConfig.kotlinVersion)
    val shadowPluginVersion by extra(MainConfig.shadowPluginVersion)
    repositories {
        mavenCentral()
        maven(url = "https://plugins.gradle.org/m2/")
        jcenter()
    }
    dependencies {
        classpath(kotlin("gradle-plugin", version = kotlinVersion))
        classpath(kotlin("serialization", version = kotlinVersion))
        classpath("com.github.jengelman.gradle.plugins:shadow:$shadowPluginVersion")
        classpath("net.ltgt.gradle:gradle-apt-plugin:0.15")
    }
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}

allprojects {
    group = "solutions.viae"
    version = Files.readAllLines(Paths.get("${project.rootDir.absolutePath}/.version")).first().trim().replace("version=", "")

    repositories {
        mavenCentral()
        maven(url = "https://plugins.gradle.org/m2/")
        jcenter()
    }

    project.tasks.withType<Test>().configureEach {
        reports.junitXml.destination = file("$rootDir/test-reports/xml/${project.name}")
        reports.html.destination = file("$rootDir/test-reports/html/${project.name}")
        useJUnitPlatform()
        if (System.getProperty("isWithStdOutput") == "true") {
            testLogging {
                events = org.gradle.api.tasks.testing.logging.TestLogEvent.values().toSet()
            }
        }
    }

    project.apply(plugin = "idea")
    project.apply(plugin = "org.jetbrains.kotlin.jvm")
    project.apply(plugin = "java-library")
    project.apply(plugin = "kotlin-kapt")
    project.apply(plugin = "jacoco")
    project.apply(plugin = "checkstyle")
    project.apply(plugin = "io.gitlab.arturbosch.detekt")
    project.apply(plugin = "java")
//    project.apply(plugin = "maven")
    project.apply(plugin = "maven-publish")
    project.apply(plugin = "signing")

    project.dependencies {
//        add("api", Libraries.Kotlin.stdlibJdk8)

        add("testImplementation", Libraries.Test.mockitoJunitJupiter)
        add("testImplementation", Libraries.Test.junitJupiterApi)
        add("testImplementation", Libraries.Test.junitJupiterParams)
        add("testImplementation", Libraries.Test.assertjCore)
        add("testImplementation", Libraries.Logging.slf4jSimple)

        add("testImplementation", Libraries.Test.mockitoJunitJupiter)
        add("testImplementation", Libraries.Test.junitJupiterApi)
        add("testImplementation", Libraries.Test.junitJupiterParams)
        add("testImplementation", Libraries.Test.assertjCore)
        add("testImplementation", Libraries.Micronaut.injectJava)
        add("testImplementation", Libraries.Micronaut.runtime)
        add("testImplementation", Libraries.Micronaut.testCore)
        add("testImplementation", Libraries.Micronaut.testJunit5)
        add("testImplementation", Libraries.Micronaut.httpClient)
        add("testImplementation", Libraries.Micronaut.httpServerNetty)
        add("testImplementation", Libraries.Micronaut.http)
        add("testImplementation", Libraries.Jackson.kotlin)

        add("testRuntimeOnly", Libraries.Test.junitJupiterEngine)
        add("testRuntimeOnly", Libraries.Test.junitPlatformLauncher)
        add("testRuntimeOnly", Libraries.Test.junitPlatformRunner)

        add("kaptTest", Libraries.Micronaut.injectJava)
    }

    project.tasks.withType(JacocoReport::class) {
        dependsOn(project.tasks.getByName("test"))
        reports {
            xml.isEnabled = false
            csv.isEnabled = false
            html.destination = file("${rootDir}/test-reports/coverage/jacocoHtml")
        }
    }

    project.tasks.withType(JacocoCoverageVerification::class) {
        violationRules {
            rule {
                limit {
                    minimum = "0.8".toBigDecimal()
                }
            }

            rule {
                enabled = true
                element = "CLASS"
                includes = listOf("solutions.viae.*")

                limit {
                    counter = "LINE"
                    value = "TOTALCOUNT"
                    maximum = "250".toBigDecimal()
                }
            }
        }
    }

    project.configure<org.gradle.plugins.ide.idea.model.IdeaModel> {
        module {
            inheritOutputDirs = true
        }
    }

    project.configure<JavaPluginConvention> {
        sourceCompatibility = MainConfig.javaSourceVersionCompatibility
        targetCompatibility = MainConfig.javaTargetVersionCompatibility
    }

    project.tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")
        kotlinOptions.jvmTarget = MainConfig.jvmTarget
    }

    project.tasks.withType<io.gitlab.arturbosch.detekt.Detekt>() {
        jvmTarget = MainConfig.javaTargetVersionCompatibility.majorVersion
        include("**/*.kt")
        include("**/*.kts")
        exclude("resources/")
        exclude("build/")
    }

    project.configure<io.gitlab.arturbosch.detekt.extensions.DetektExtension> {
        //https://arturbosch.github.io/detekt/kotlindsl.html
        toolVersion = "1.9.1"
        input = files(
            "src/main/java",
            "src/main/kotlin"
        )     // The directories where detekt looks for source files. Defaults to `files("src/main/java", "src/main/kotlin")`.
        parallel =
            false                                      // Builds the AST in parallel. Rules are always executed in parallel. Can lead to speedups in larger projects. `false` by default.
        config =
            files("${rootDir}/config/detekt/detekt-config.yml")                  // Define the detekt configuration(s) you want to use. Defaults to the default detekt configuration.
        buildUponDefaultConfig = false                        // Interpret config files as updates to the default config. `false` by default.
//        baseline = file("${rootDir}/config/detekt/detekt-baseline.xml")               // Specifying a baseline file. All findings stored in this file in subsequent runs of detekt.
        disableDefaultRuleSets =
            false                        // Disables all default detekt rulesets and will only run detekt with custom rules defined in plugins passed in with `detektPlugins` configuration. `false` by default.
        debug = false                                         // Adds debug output during task execution. `false` by default.
        ignoreFailures =
            false                                // If set to `true` the build does not fail when the maxIssues count was reached. Defaults to `false`.
        reports {
            xml {
                enabled = true                                // Enable/Disable XML report (default: true)
                destination =
                    file("${rootDir}/test-reports/detekt/detekt.xml")  // Path where XML report will be stored (default: `build/reports/detekt/detekt.xml`)
            }
            html {
                enabled = true                                // Enable/Disable HTML report (default: true)
                destination =
                    file("${rootDir}/test-reports/detekt/detekt.html") // Path where HTML report will be stored (default: `build/reports/detekt/detekt.html`)
            }
            txt {
                enabled = true                                // Enable/Disable TXT report (default: true)
                destination =
                    file("${rootDir}/test-reports/detekt/detekt.txt") // Path where TXT report will be stored (default: `build/reports/detekt/detekt.txt`)
            }
            custom {
                reportId = "CustomJsonReport"                   // The simple class name of your custom report.
                destination = file("${rootDir}/test-reports/detekt/detekt.json") // Path where report will be stored
            }
        }
        idea {
            path = "$rootDir/.idea"
            codeStyleScheme = "$rootDir/.idea/idea-code-style.xml"
            inspectionsProfile = "$rootDir/.idea/inspect.xml"
            report = "$project.projectDir/reports"
            mask = "*.kt,"
        }
    }

    //For sub projects
    if (
        (project.parent?.parent?.name == "viae-modules" && project.parent?.name == "modules")
        || (project.parent?.parent?.parent?.name == "viae-modules" && project.parent?.parent?.name == "modules")
    ) {
        println("configure sub project ${project.name}")

        project.apply(plugin = "net.researchgate.release")
        project.apply(plugin = "name.remal.check-dependency-updates")

        project.configure<net.researchgate.release.ReleaseExtension> {
            versionPropertyFile = "${rootDir}/.version"
            failOnUnversionedFiles = false
            failOnCommitNeeded = false
        }

        project.tasks.withType<name.remal.gradle_plugins.plugins.check_updates.CheckDependencyUpdates>{
            failIfUpdatesFound = true
            notCheckedDependencies = mutableSetOf<String>()
        }

        project.tasks.create("sourcesJar", org.gradle.jvm.tasks.Jar::class) {
            dependsOn(JavaPlugin.CLASSES_TASK_NAME)
            archiveClassifier.set("sources")
            from(sourceSets.main.get().allSource)
        }

        project.tasks.create("javadocJar", org.gradle.jvm.tasks.Jar::class) {
            dependsOn(JavaPlugin.JAVADOC_TASK_NAME)
            archiveClassifier.set("javadoc")
            from(project.tasks["javadoc"])
        }

        project.configure<SigningExtension> {
            "configuring signing for publications ${publishing.publications.names}"
            //if you get "Could not read PGP secret key" error
//            gpg --armor --export-secret-keys info@viae.solutions \
//            | awk 'NR == 1 { print "GPG_SIGNING_KEY=" } 1' ORS='\\n' \
//            >> gradle.properties
            //extract public key:
//            gpg --armor --export info@viae.solutions
            //https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/
            val signingKeyId: String= System.getenv("signingKeyId") ?: project.properties["signingKeyId"].toString()
            val signingKey: String= System.getenv("signingKey") ?: project.properties["signingKey"].toString()
            val signingPassword: String= System.getenv("signingPassword") ?: project.properties["signingPassword"].toString()
            useInMemoryPgpKeys(signingKeyId, signingKey, signingPassword)
            sign(project.publishing.publications)
        }

        publishing {
            repositories {
                repositories {
                    maven {
                        val nexusUsername: String = System.getenv("nexusUsername") ?: project.properties["nexusUsername"].toString()
                        val nexusPassword: String = System.getenv("nexusPassword") ?: project.properties["nexusPassword"].toString()
                        credentials {
                            username = nexusUsername
                            password = nexusPassword
                        }
                        val releasesRepoUrl = "https://oss.sonatype.org/service/local/staging/deploy/maven2"
                        val snapshotsRepoUrl = "https://oss.sonatype.org/content/repositories/snapshots"
                        url = uri(if (version.toString().endsWith("-SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl)
                    }
                }
                publications {
                    register("mavenJava", MavenPublication::class) {
                        from(components["java"])
                        artifact(project.tasks.findByName("sourcesJar"))
                        artifact(project.tasks.findByName("javadocJar"))

                        pom {
                            name.set(project.name)
                            description.set("VIAE modules")
                            url.set("https://viae.solutions")

                            licenses {
                                license {
                                    name.set("The Apache License, Version 2.0")
                                    url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                                }
                            }
                            developers {
                                developer {
                                    id.set("VIAE")
                                    name.set("VIAE")
                                    email.set("info@viae.solutions")
                                }
                            }
                            scm {
                                connection.set("scm:git:git://gitlab.com:viae-modules/viae-modules.git")
                                developerConnection.set("scm:git:ssh://gitlab.com:viae-modules/viae-modules.git")
                                url.set("https://gitlab.com/viae-modules/viae-modules")
                            }
                        }
                    }
                }
            }
        }
        tasks.getByName("afterReleaseBuild").dependsOn(tasks.getByName("publish"))
    }

}

withPatchVersionTask()
withRenameProjectTask()
